﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GestionDigitalCore.CoreObjects;
using GestionDigitalCore.CoreBussines;

namespace GestiónDigital.Log
{
    public partial class frmFTPLogList : Form
    {
        public int IdConfiguracionFTP;
        public DateTime desde;
        public DateTime hasta;

        public frmFTPLogList()
        {
            desde = DateTime.Now.AddDays(-5);
            hasta = DateTime.Now;
            IdConfiguracionFTP = 0;
            InitializeComponent();
        }

        private void frmFTPLogList_Load(object sender, EventArgs e)
        {
            BindFiltros();
            if (IdConfiguracionFTP > 0)
            {
                cboConfs.SelectedValue = IdConfiguracionFTP;
                DataBind();
            }
        }

        private void BindFiltros()
        {
            BBConfiguracionFTP BBC = new BBConfiguracionFTP();
            List<ConfiguracionFTP> l = BBC.GetAll();
            cboConfs.DataSource = l;
            cboConfs.DisplayMember = "Nombre";
            cboConfs.ValueMember = "ID";

            rngDesdeHasta.Desde = desde;
            rngDesdeHasta.Hasta = hasta;
        }

        private void cmdBuscar_Click(object sender, EventArgs e)
        {
            DataBind();
        }
        private void DataBind()
        {
            BBLogFTP BBL = new BBLogFTP();
            BBConfiguracionFTP bbconf = new BBConfiguracionFTP();
            List<LogFTP>datos =  BBL.GetAll(Convert.ToInt32(cboConfs.SelectedValue), rngDesdeHasta.Desde, rngDesdeHasta.Hasta);
            dgDatos.AutoGenerateColumns = false;
            dgDatos.Rows.Clear();
            foreach (LogFTP c in datos)
            {
                dgDatos.Rows.Add(c.ID, c.FechaInicio.ToString("dd/MM/yyyy hh:mm"), c.FechaFin.ToString("dd/MM/yyyy hh:mm"), bbconf.getById(c.IdConfiguracionFTP).Nombre, c.ArchivosProcesados.ToString(), c.Errores);
            }
            
        }

        private void dgDatos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            frmLogDetalleList f = new frmLogDetalleList();
            f.IdLog = Convert.ToInt32(dgDatos.SelectedRows[0].Cells[0].Value);
            f.ShowDialog(this);
            DataBind();
        }

        private void cmdDelete_Click(object sender, EventArgs e)
        {


            Cursor.Current = Cursors.WaitCursor;
            try
            {
                BBLogFTP BBF = new BBLogFTP();
                if (dgDatos.SelectedRows.Count > 0)
                {
                    foreach (DataGridViewRow r in dgDatos.SelectedRows)
                    {
                        int IdLog = (int)r.Cells[0].Value;
                        BBF.Eliminar(IdLog);
                    }
                }
                DataBind();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
			
        }


    }
}