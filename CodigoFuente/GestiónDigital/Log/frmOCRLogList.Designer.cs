﻿namespace GestiónDigital.Log
{
    partial class frmOCRLogList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgDatos = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColDocumento = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNombreOriginal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNombreModif = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFechaProc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColResolucion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColError = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColFullName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.rngDesdeHasta = new Controles.dtRange();
            this.cmdBuscar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cboDocumentos = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmdEliminar = new System.Windows.Forms.Button();
            this.chkConError = new System.Windows.Forms.CheckBox();
            this.chkModoEdicion = new System.Windows.Forms.CheckBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.cboOCRConf = new System.Windows.Forms.ComboBox();
            this.cmdCambiarNombre = new System.Windows.Forms.Button();
            this.pic = new System.Windows.Forms.PictureBox();
            this.chkManuales = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgDatos)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic)).BeginInit();
            this.SuspendLayout();
            // 
            // dgDatos
            // 
            this.dgDatos.AllowUserToAddRows = false;
            this.dgDatos.AllowUserToDeleteRows = false;
            this.dgDatos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgDatos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDatos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.ColDocumento,
            this.ColNombreOriginal,
            this.ColNombreModif,
            this.ColFechaProc,
            this.ColResolucion,
            this.ColError,
            this.ColFullName});
            this.dgDatos.Location = new System.Drawing.Point(3, 3);
            this.dgDatos.Name = "dgDatos";
            this.dgDatos.ReadOnly = true;
            this.dgDatos.RowHeadersVisible = false;
            this.dgDatos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDatos.Size = new System.Drawing.Size(815, 471);
            this.dgDatos.TabIndex = 7;
            this.dgDatos.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDatos_CellDoubleClick);
            this.dgDatos.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDatos_CellEndEdit);
            this.dgDatos.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDatos_CellEnter);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "IdLog";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // ColDocumento
            // 
            this.ColDocumento.HeaderText = "Documento";
            this.ColDocumento.Name = "ColDocumento";
            this.ColDocumento.ReadOnly = true;
            this.ColDocumento.Width = 250;
            // 
            // ColNombreOriginal
            // 
            this.ColNombreOriginal.HeaderText = "Nombre Original";
            this.ColNombreOriginal.Name = "ColNombreOriginal";
            this.ColNombreOriginal.ReadOnly = true;
            this.ColNombreOriginal.Width = 150;
            // 
            // ColNombreModif
            // 
            this.ColNombreModif.HeaderText = "Nombre Modificado";
            this.ColNombreModif.Name = "ColNombreModif";
            this.ColNombreModif.ReadOnly = true;
            this.ColNombreModif.Width = 150;
            // 
            // ColFechaProc
            // 
            this.ColFechaProc.HeaderText = "Fecha de Proceso";
            this.ColFechaProc.Name = "ColFechaProc";
            this.ColFechaProc.ReadOnly = true;
            this.ColFechaProc.Width = 150;
            // 
            // ColResolucion
            // 
            this.ColResolucion.HeaderText = "Resolución";
            this.ColResolucion.Name = "ColResolucion";
            this.ColResolucion.ReadOnly = true;
            // 
            // ColError
            // 
            this.ColError.HeaderText = "Error";
            this.ColError.Name = "ColError";
            this.ColError.ReadOnly = true;
            this.ColError.Width = 250;
            // 
            // ColFullName
            // 
            this.ColFullName.HeaderText = "ColFullName";
            this.ColFullName.Name = "ColFullName";
            this.ColFullName.ReadOnly = true;
            this.ColFullName.Visible = false;
            // 
            // rngDesdeHasta
            // 
            this.rngDesdeHasta.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.rngDesdeHasta.Desde = new System.DateTime(2008, 4, 10, 0, 0, 0, 0);
            this.rngDesdeHasta.Hasta = new System.DateTime(2008, 4, 10, 23, 59, 59, 0);
            this.rngDesdeHasta.Location = new System.Drawing.Point(142, 38);
            this.rngDesdeHasta.Name = "rngDesdeHasta";
            this.rngDesdeHasta.RequiereDesde = false;
            this.rngDesdeHasta.RequiereHasta = false;
            this.rngDesdeHasta.Size = new System.Drawing.Size(348, 20);
            this.rngDesdeHasta.TabIndex = 6;
            // 
            // cmdBuscar
            // 
            this.cmdBuscar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdBuscar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdBuscar.Location = new System.Drawing.Point(629, 37);
            this.cmdBuscar.Name = "cmdBuscar";
            this.cmdBuscar.Size = new System.Drawing.Size(75, 23);
            this.cmdBuscar.TabIndex = 5;
            this.cmdBuscar.Text = "Buscar";
            this.cmdBuscar.UseVisualStyleBackColor = true;
            this.cmdBuscar.Click += new System.EventHandler(this.cmdBuscar_Click);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Documentos";
            // 
            // cboDocumentos
            // 
            this.cboDocumentos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboDocumentos.FormattingEnabled = true;
            this.cboDocumentos.Location = new System.Drawing.Point(142, 11);
            this.cboDocumentos.Name = "cboDocumentos";
            this.cboDocumentos.Size = new System.Drawing.Size(348, 21);
            this.cboDocumentos.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Fecha de Proc.";
            // 
            // cmdEliminar
            // 
            this.cmdEliminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdEliminar.Location = new System.Drawing.Point(505, 37);
            this.cmdEliminar.Name = "cmdEliminar";
            this.cmdEliminar.Size = new System.Drawing.Size(117, 23);
            this.cmdEliminar.TabIndex = 13;
            this.cmdEliminar.Text = "Borrar Selección";
            this.cmdEliminar.UseVisualStyleBackColor = true;
            this.cmdEliminar.Click += new System.EventHandler(this.cmdEliminar_Click);
            // 
            // chkConError
            // 
            this.chkConError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkConError.AutoSize = true;
            this.chkConError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkConError.Location = new System.Drawing.Point(505, 13);
            this.chkConError.Name = "chkConError";
            this.chkConError.Size = new System.Drawing.Size(199, 17);
            this.chkConError.TabIndex = 12;
            this.chkConError.Text = "Mostrar Documentos Con Error";
            this.chkConError.UseVisualStyleBackColor = true;
            this.chkConError.CheckedChanged += new System.EventHandler(this.chkConError_CheckedChanged);
            // 
            // chkModoEdicion
            // 
            this.chkModoEdicion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkModoEdicion.AutoSize = true;
            this.chkModoEdicion.Enabled = false;
            this.chkModoEdicion.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkModoEdicion.Location = new System.Drawing.Point(895, 13);
            this.chkModoEdicion.Name = "chkModoEdicion";
            this.chkModoEdicion.Size = new System.Drawing.Size(103, 17);
            this.chkModoEdicion.TabIndex = 14;
            this.chkModoEdicion.Text = "Editar Errores";
            this.chkModoEdicion.UseVisualStyleBackColor = true;
            this.chkModoEdicion.CheckedChanged += new System.EventHandler(this.chkModoEdicion_CheckedChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(2, 66);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.cboOCRConf);
            this.splitContainer1.Panel1.Controls.Add(this.cmdCambiarNombre);
            this.splitContainer1.Panel1.Controls.Add(this.dgDatos);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.pic);
            this.splitContainer1.Size = new System.Drawing.Size(1009, 532);
            this.splitContainer1.SplitterDistance = 821;
            this.splitContainer1.TabIndex = 16;
            // 
            // cboOCRConf
            // 
            this.cboOCRConf.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cboOCRConf.FormattingEnabled = true;
            this.cboOCRConf.Location = new System.Drawing.Point(3, 480);
            this.cboOCRConf.Name = "cboOCRConf";
            this.cboOCRConf.Size = new System.Drawing.Size(804, 21);
            this.cboOCRConf.TabIndex = 18;
            this.cboOCRConf.Visible = false;
            // 
            // cmdCambiarNombre
            // 
            this.cmdCambiarNombre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCambiarNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCambiarNombre.Location = new System.Drawing.Point(3, 507);
            this.cmdCambiarNombre.Name = "cmdCambiarNombre";
            this.cmdCambiarNombre.Size = new System.Drawing.Size(804, 22);
            this.cmdCambiarNombre.TabIndex = 17;
            this.cmdCambiarNombre.Text = "Cambiar Ahora";
            this.cmdCambiarNombre.UseVisualStyleBackColor = true;
            this.cmdCambiarNombre.Visible = false;
            this.cmdCambiarNombre.Click += new System.EventHandler(this.cmdCambiarNombre_Click);
            // 
            // pic
            // 
            this.pic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic.Location = new System.Drawing.Point(0, 0);
            this.pic.Name = "pic";
            this.pic.Size = new System.Drawing.Size(184, 532);
            this.pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic.TabIndex = 16;
            this.pic.TabStop = false;
            // 
            // chkManuales
            // 
            this.chkManuales.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkManuales.AutoSize = true;
            this.chkManuales.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkManuales.Location = new System.Drawing.Point(715, 13);
            this.chkManuales.Name = "chkManuales";
            this.chkManuales.Size = new System.Drawing.Size(109, 17);
            this.chkManuales.TabIndex = 17;
            this.chkManuales.Text = "Solo Manuales";
            this.chkManuales.UseVisualStyleBackColor = true;
            // 
            // frmOCRLogList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1010, 599);
            this.Controls.Add(this.chkManuales);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.chkModoEdicion);
            this.Controls.Add(this.cmdEliminar);
            this.Controls.Add(this.chkConError);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboDocumentos);
            this.Controls.Add(this.rngDesdeHasta);
            this.Controls.Add(this.cmdBuscar);
            this.Name = "frmOCRLogList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Listado de Documentos Procesados";
            this.Load += new System.EventHandler(this.frmOCRLogList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgDatos)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgDatos;
        private Controles.dtRange rngDesdeHasta;
        private System.Windows.Forms.Button cmdBuscar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboDocumentos;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button cmdEliminar;
        private System.Windows.Forms.CheckBox chkConError;
        private System.Windows.Forms.CheckBox chkModoEdicion;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PictureBox pic;
        private System.Windows.Forms.Button cmdCambiarNombre;
        private System.Windows.Forms.ComboBox cboOCRConf;
        private System.Windows.Forms.CheckBox chkManuales;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColDocumento;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNombreOriginal;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNombreModif;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFechaProc;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColResolucion;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColError;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColFullName;
    }
}