﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GestionDigitalCore.CoreObjects;
using GestionDigitalCore.CoreBussines;

namespace GestiónDigital.Log
{
    public partial class frmLogDetalleList : Form
    {
        public int IdLog;
        private LogFTP Obj;
        public frmLogDetalleList()
        {
            InitializeComponent();
        }

        private void frmLogDetalleList_Load(object sender, EventArgs e)
        {
            if (IdLog > 0)
            {
                BBLogFTP BBL = new BBLogFTP();
                Obj = BBL.getById(IdLog);
                BindearDatosLog();
                BindearDetallesLog();
            }
        }

        private void BindearDetallesLog()
        {
            BBLogDetalleFTP BBL = new BBLogDetalleFTP();
            List<LogDetalleFTP> datos = BBL.GetByLogId(IdLog);
            dgDatos.AutoGenerateColumns = false;
            dgDatos.Rows.Clear();
            foreach (LogDetalleFTP log in datos)
            {
                dgDatos.Rows.Add(log.ID,log.FileName, log.GetSizeFormateado(), log.FechaInicio.ToString("dd/MM/yyyy hh:mm:ss"), log.FechaFin.ToString("dd/MM/yyyy hh:mm:ss"), log.GetTiempoTotal(), log.Error);
            }
        }

        private void BindearDatosLog()
        {
            BBConfiguracionFTP BBC = new BBConfiguracionFTP();
            txtFechaInicio.Text = Obj.FechaInicio.ToString("dd/MM/yyyy hh:mm:ss");
            TxtFechaFin.Text = Obj.FechaFin.ToString("dd/MM/yyyy hh:mm:ss");
            txtErrores.Text = Obj.Errores.ToString() ;
            txtArchivos.Text = Obj.ArchivosProcesados.ToString();
            txtConfiguracion.Text = BBC.getById(Obj.IdConfiguracionFTP).Nombre;
        }

        private void cmdEliminarTodo_Click(object sender, EventArgs e)
        {
            BBLogFTP BB = new BBLogFTP();
            BB.Eliminar(Obj);
            this.Close();
        }

        private void dgDatos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            CargarRegistroOCR();
        }

        private void CargarRegistroOCR()
        {
            if (dgDatos.SelectedRows.Count > 0)
            {
                string fileName = (string)dgDatos.SelectedRows[0].Cells[1].Value;
                int i = fileName.LastIndexOf('\\');
                string path = fileName.Substring(0, i);
                string NombreArchivo = fileName.Substring(i + 1, fileName.Length - path.Length - 1);
                BBArchivo BBA = new BBArchivo();
                
                List<Archivo> ListadoArchivo = BBA.getByFileNameModificado(path, NombreArchivo);
                BindearGrilaOCR(ListadoArchivo);
            }
        }

        private void BindearGrilaOCR(List<Archivo> Listado)
        {
            dgDatosOCR.AutoGenerateColumns = false;
            dgDatosOCR.Rows.Clear();
            BBDocumento BBD = new BBDocumento();
            Documento Doc;
            foreach (Archivo c in Listado)
            {
                Doc = BBD.getById(c.IdDocumento);
                dgDatosOCR.Rows.Add(c.ID, Doc.Nombre, c.NombreOriginal, c.NombreModificado, c.FechaProceso.ToString("dd/MM/yyyy hh:mm"), c.Error);
            }
        }

        private void dgDatosOCR_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            frmLogOCRDetalle f = new frmLogOCRDetalle();
            f.IdArchivo = Convert.ToInt32(dgDatosOCR.SelectedRows[0].Cells[0].Value);
            f.ShowDialog(this);
        }

    }
}