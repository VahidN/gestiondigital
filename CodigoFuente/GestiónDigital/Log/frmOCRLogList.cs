﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GestionDigitalCore.CoreBussines;
using GestionDigitalCore.CoreObjects;
using System.IO;

namespace GestiónDigital.Log
{
    public partial class frmOCRLogList : Form
    {
        public frmOCRLogList()
        {
            InitializeComponent();
        }

        private void frmOCRLogList_Load(object sender, EventArgs e)
        {
            CargarDocumentos();
        }

        private void CargarDocumentos()
        {
            BBDocumento BBC = new BBDocumento();
            List<Documento> l = BBC.GetAll();
            cboDocumentos.DataSource = l;
            cboDocumentos.DisplayMember = "Nombre";
            cboDocumentos.ValueMember = "ID";
            cboDocumentos.SelectedIndex = -1;
            //--
            BBConfiguracionOCR BBO = new BBConfiguracionOCR();
            List<ConfiguracionOCR> locr = BBO.GetAll();
            cboOCRConf.DataSource = locr;
            cboOCRConf.DisplayMember = "Nombre";
            cboOCRConf.ValueMember = "ID";
            cboOCRConf.SelectedValue = BBO.GetDefaultConfiguration().ID;

            //--
            rngDesdeHasta.Hasta = DateTime.Today;
            rngDesdeHasta.Desde = DateTime.Today.AddDays(-30);
            BuscarDatos();
        }

        private void cmdBuscar_Click(object sender, EventArgs e)
        {
            BuscarDatos();
        }

        private void BuscarDatos()
        {
            int IdDoc =-1;
            if(cboDocumentos.SelectedValue!=null)
                IdDoc = (int)cboDocumentos.SelectedValue;

            DateTime? Desde = rngDesdeHasta.Desde;
            DateTime? Hasta = rngDesdeHasta.Hasta;
            BBArchivo BBA = new BBArchivo();
            List<Archivo> Listado = BBA.getFiltered(IdDoc, Desde, Hasta, chkConError.Checked, chkManuales.Checked);
            BindearGrila(Listado);
        }

        private void BindearGrila(List<Archivo> Listado)
        {
            dgDatos.AutoGenerateColumns = false;
            dgDatos.Rows.Clear();
            BBDocumento BBD = new BBDocumento();
            Documento Doc;
            foreach (Archivo c in Listado)
            {
                Doc = BBD.getById(c.IdDocumento);
                dgDatos.Rows.Add(c.ID,Doc.Nombre, c.NombreOriginal, c.NombreModificado, c.FechaProceso.ToString("dd/MM/yyyy hh:mm"),c.ResolucionHorizontal , c.Error, c.GetModificadoFullName());
            }
        }

        private void cmdEliminar_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                BBArchivo BBA = new BBArchivo();
                if (dgDatos.SelectedRows.Count > 0)
                {
                    foreach (DataGridViewRow row in dgDatos.SelectedRows)
                    {
                        int Id = (int)row.Cells[0].Value;
                        BBA.Eliminar(Id);
                    }
                    BuscarDatos();
                }
                else
                {
                    MessageBox.Show("Debe Seleccionar al menos un registro");
                }
            }
            catch (Exception Ex)
            {

                MessageBox.Show(Ex.Message);
            }
            finally {
                Cursor.Current = Cursors.Default;
            }
        }

        private void dgDatos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            frmLogOCRDetalle f = new frmLogOCRDetalle();
            f.IdArchivo = Convert.ToInt32(dgDatos.SelectedRows[0].Cells[0].Value);
            f.ShowDialog(this);
            
        }

        private void chkConError_CheckedChanged(object sender, EventArgs e)
        {
            chkModoEdicion.Enabled = chkConError.Checked;
            if (!chkModoEdicion.Checked)
            {
                chkModoEdicion.Checked = false;
            }
            BuscarDatos();
        }

        private void chkModoEdicion_CheckedChanged(object sender, EventArgs e)
        {
            ColDocumento.Visible = !chkModoEdicion.Checked;
            ColNombreOriginal.Visible = !chkModoEdicion.Checked;
            ColFechaProc.Visible = !chkModoEdicion.Checked;
            ColError.Visible = chkModoEdicion.Checked;
            
            dgDatos.ReadOnly = !chkModoEdicion.Checked;
            ColNombreModif.ReadOnly = !chkModoEdicion.Checked;
            cboOCRConf.Visible = chkModoEdicion.Checked;
            cmdCambiarNombre.Visible = chkModoEdicion.Checked;
            splitContainer1.SplitterDistance = ColNombreModif.Width + 10 + ColError.Width;
        }

        private void dgDatos_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            string Modificado = dgDatos.Rows[e.RowIndex].Cells[7].Value.ToString();
            pic.ImageLocation = Modificado;
        }

        private void dgDatos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            string valor = dgDatos.Rows[e.RowIndex].Cells[3].Value.ToString();
            if(ValidarString(valor))
                dgDatos.Rows[e.RowIndex].Cells[3].Style.ForeColor = Color.Green;//revisar cmdCambiarNombre_Click si deseo cambiar este color
            else
                dgDatos.Rows[e.RowIndex].Cells[3].Style.ForeColor = Color.Red;
            if (cboOCRConf.SelectedValue != null)
            {
                BBConfiguracionOCR BBC = new BBConfiguracionOCR();
                BBArchivo BBA = new BBArchivo();
                ConfiguracionOCR conf = BBC.getById((int)cboOCRConf.SelectedValue);
                List<Archivo> Duplicados = BBA.getByFileNameModificado(dgDatos.Rows[e.RowIndex].Cells[3].Value + ".jpg");
                if (Duplicados.Count > 0 || File.Exists(conf.DirectorioDestino + "\\" + dgDatos.Rows[e.RowIndex].Cells[3].Value + ".jpg") || YaExisteEnLista(dgDatos.Rows[e.RowIndex]))
                {
                    dgDatos.Rows[e.RowIndex].Cells[3].Style.BackColor = Color.Yellow;
                    dgDatos.Rows[e.RowIndex].Cells[3].Style.ForeColor = Color.Red;
                }
                else {
                    dgDatos.Rows[e.RowIndex].Cells[3].Style.BackColor = Color.White;
                }
            }
        }

        private bool YaExisteEnLista(DataGridViewRow r)
        {
            foreach (DataGridViewRow MyRow in dgDatos.Rows)
            {
                if (MyRow.Cells[0].Value.ToString() != r.Cells[0].Value.ToString())
                {
                    if (MyRow.Cells[3].Value.ToString() == r.Cells[3].Value.ToString())
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        private bool ValidarString(string valor)
        {
            if (valor.Length != 10)
            {
                return false;
            }
            else { 
                try{
                    long.Parse(valor);
                    return true;

                }catch{
                    return false;
                }
            }
        }

        private void cmdCambiarNombre_Click(object sender, EventArgs e)
        {
            if (cboOCRConf.SelectedValue != null)
            {
                BBConfiguracionOCR BBC = new BBConfiguracionOCR();
                ConfiguracionOCR conf = BBC.getById((int)cboOCRConf.SelectedValue);
                BBArchivo BBA = new BBArchivo();
                foreach (DataGridViewRow r in dgDatos.Rows)
                { 
                    if(r.Cells[3].Style.ForeColor==Color.Green)
                    {

                            Archivo Arch = BBA.getById((int)r.Cells[0].Value);
                            File.Move(Arch.GetModificadoFullName(), conf.DirectorioDestino + "\\" + r.Cells[3].Value + ".jpg");
                            Arch.PathDestino = conf.DirectorioDestino;
                            Arch.NombreModificado = r.Cells[3].Value + ".jpg";
                            Arch.Error = "";
                            Arch.CambioManual = true;
                            BBA.Guardar(Arch);

                    }
                    
                }
                BuscarDatos();
            }
            else {
                MessageBox.Show("Debe seleccionar una configuración.");
            }
        }


    }
}