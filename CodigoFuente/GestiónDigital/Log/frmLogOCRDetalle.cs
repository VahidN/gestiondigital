﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GestionDigitalCore.CoreObjects;
using GestionDigitalCore.CoreBussines;

namespace GestiónDigital.Log
{
    public partial class frmLogOCRDetalle : Form
    {
        public int IdArchivo;
        Archivo Obj;
        public frmLogOCRDetalle()
        {
            InitializeComponent();
        }

        private void frmLogOCRDetalle_Load(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;

            try
            {
                BindearPantalla();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ": " + ex.InnerException.Message;
                }
                MessageBox.Show(msg);
            }

            Cursor.Current = Cursors.Default;
			
        }

        private void BindearPantalla()
        {
            BBArchivo BBA = new BBArchivo();
            BBDocumento BBD = new BBDocumento();
            Documento doc = new Documento();
            BBA.LazyLoad = false;
            Obj = BBA.getById(IdArchivo);
            if (Obj != null)
            {
                txtError.Text = Obj.Error;
                doc = BBD.getById(Obj.IdDocumento);
                txtNombreDoc.Text = doc.Nombre;
                txtNombreModificado.Text = Obj.NombreModificado;
                txtNombreOriginal.Text = Obj.NombreOriginal;
                txtPathDestino.Text = Obj.PathDestino;
                txtPathOrigen.Text = Obj.PathOrigen;
                txtsizeModi.Text = Obj.getTamañoModificadoFormateado();
                txtSizeOri.Text = Obj.getTamañoOriginalFormateado();
            }
            dgDatos.AutoGenerateColumns = false;
            dgDatos.Rows.Clear();
            Campo c;
            foreach (ValorArchivo v in Obj.MisValores)
            {                
                c = new BBCampo().getById( v.IdCampo);
                dgDatos.Rows.Add(v.ID,c.Nombre, c.TipoCampoDescripcion, v.Valor,v.Error);
            }
            
        }

        private void cmdCerrar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}