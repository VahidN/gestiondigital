﻿namespace GestiónDigital.Procesadores
{
    partial class ProcesamientoArchivos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmdLog = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblTipoCampo = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblCampo = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblOrigen = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblTamaño = new System.Windows.Forms.Label();
            this.lblArchivo = new System.Windows.Forms.Label();
            this.cmdCancelar = new System.Windows.Forms.Button();
            this.cmdProcesar = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtZoneHeight = new System.Windows.Forms.TextBox();
            this.txtZoneWidth = new System.Windows.Forms.TextBox();
            this.txtZoneY = new System.Windows.Forms.TextBox();
            this.txtZoneX = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.pboxsample = new System.Windows.Forms.PictureBox();
            this.txtTextoObtenido = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.listViewBar = new System.Windows.Forms.ListView();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.imgResultado = new System.Windows.Forms.PictureBox();
            this.cmdCargarImagen = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.chkMostrarImagen = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pnlDocument = new System.Windows.Forms.Panel();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxsample)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgResultado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnlDocument.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmdLog);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.cmdCancelar);
            this.groupBox1.Controls.Add(this.cmdProcesar);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Location = new System.Drawing.Point(1, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(371, 732);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Procesando Archivo";
            // 
            // cmdLog
            // 
            this.cmdLog.Location = new System.Drawing.Point(6, 703);
            this.cmdLog.Name = "cmdLog";
            this.cmdLog.Size = new System.Drawing.Size(96, 23);
            this.cmdLog.TabIndex = 108;
            this.cmdLog.Text = "Ver Log";
            this.cmdLog.UseVisualStyleBackColor = true;
            this.cmdLog.Click += new System.EventHandler(this.cmdLog_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Controls.Add(this.lblTipoCampo);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.lblCampo);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.lblOrigen);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Controls.Add(this.lblTamaño);
            this.groupBox4.Controls.Add(this.lblArchivo);
            this.groupBox4.Location = new System.Drawing.Point(6, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(357, 190);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Archivo";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(200, 162);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Tipo:";
            // 
            // lblTipoCampo
            // 
            this.lblTipoCampo.AutoSize = true;
            this.lblTipoCampo.Location = new System.Drawing.Point(255, 162);
            this.lblTipoCampo.Name = "lblTipoCampo";
            this.lblTipoCampo.Size = new System.Drawing.Size(0, 13);
            this.lblTipoCampo.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 162);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(49, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Campo:";
            // 
            // lblCampo
            // 
            this.lblCampo.AutoSize = true;
            this.lblCampo.Location = new System.Drawing.Point(61, 162);
            this.lblCampo.Name = "lblCampo";
            this.lblCampo.Size = new System.Drawing.Size(0, 13);
            this.lblCampo.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Origen:";
            // 
            // lblOrigen
            // 
            this.lblOrigen.ForeColor = System.Drawing.Color.Red;
            this.lblOrigen.Location = new System.Drawing.Point(6, 29);
            this.lblOrigen.Name = "lblOrigen";
            this.lblOrigen.Size = new System.Drawing.Size(336, 87);
            this.lblOrigen.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 138);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Tamaño:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nombre:";
            // 
            // lblTamaño
            // 
            this.lblTamaño.AutoSize = true;
            this.lblTamaño.Location = new System.Drawing.Point(61, 138);
            this.lblTamaño.Name = "lblTamaño";
            this.lblTamaño.Size = new System.Drawing.Size(0, 13);
            this.lblTamaño.TabIndex = 1;
            // 
            // lblArchivo
            // 
            this.lblArchivo.AutoSize = true;
            this.lblArchivo.Location = new System.Drawing.Point(61, 116);
            this.lblArchivo.Name = "lblArchivo";
            this.lblArchivo.Size = new System.Drawing.Size(0, 13);
            this.lblArchivo.TabIndex = 0;
            // 
            // cmdCancelar
            // 
            this.cmdCancelar.Enabled = false;
            this.cmdCancelar.Location = new System.Drawing.Point(288, 703);
            this.cmdCancelar.Name = "cmdCancelar";
            this.cmdCancelar.Size = new System.Drawing.Size(75, 23);
            this.cmdCancelar.TabIndex = 0;
            this.cmdCancelar.Text = "Cancelar";
            this.cmdCancelar.UseVisualStyleBackColor = true;
            this.cmdCancelar.Click += new System.EventHandler(this.cmdCancelar_Click);
            // 
            // cmdProcesar
            // 
            this.cmdProcesar.Location = new System.Drawing.Point(209, 703);
            this.cmdProcesar.Name = "cmdProcesar";
            this.cmdProcesar.Size = new System.Drawing.Size(75, 23);
            this.cmdProcesar.TabIndex = 1;
            this.cmdProcesar.Text = "Procesar";
            this.cmdProcesar.UseVisualStyleBackColor = true;
            this.cmdProcesar.Click += new System.EventHandler(this.cmdProcesar_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtZoneHeight);
            this.groupBox2.Controls.Add(this.txtZoneWidth);
            this.groupBox2.Controls.Add(this.txtZoneY);
            this.groupBox2.Controls.Add(this.txtZoneX);
            this.groupBox2.Location = new System.Drawing.Point(8, 215);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(357, 65);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Área Analizada";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(255, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Alto:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(79, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(17, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Y:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "X:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(156, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Ancho:";
            // 
            // txtZoneHeight
            // 
            this.txtZoneHeight.Location = new System.Drawing.Point(286, 27);
            this.txtZoneHeight.Name = "txtZoneHeight";
            this.txtZoneHeight.Size = new System.Drawing.Size(46, 20);
            this.txtZoneHeight.TabIndex = 3;
            // 
            // txtZoneWidth
            // 
            this.txtZoneWidth.Location = new System.Drawing.Point(203, 27);
            this.txtZoneWidth.Name = "txtZoneWidth";
            this.txtZoneWidth.Size = new System.Drawing.Size(46, 20);
            this.txtZoneWidth.TabIndex = 2;
            // 
            // txtZoneY
            // 
            this.txtZoneY.Location = new System.Drawing.Point(102, 27);
            this.txtZoneY.Name = "txtZoneY";
            this.txtZoneY.Size = new System.Drawing.Size(46, 20);
            this.txtZoneY.TabIndex = 1;
            // 
            // txtZoneX
            // 
            this.txtZoneX.Location = new System.Drawing.Point(26, 27);
            this.txtZoneX.Name = "txtZoneX";
            this.txtZoneX.Size = new System.Drawing.Size(46, 20);
            this.txtZoneX.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.pboxsample);
            this.groupBox3.Controls.Add(this.txtTextoObtenido);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.listViewBar);
            this.groupBox3.Controls.Add(this.progressBar1);
            this.groupBox3.Controls.Add(this.imgResultado);
            this.groupBox3.Location = new System.Drawing.Point(6, 286);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(357, 411);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Resultado Análisis";
            // 
            // pboxsample
            // 
            this.pboxsample.Location = new System.Drawing.Point(9, 298);
            this.pboxsample.Name = "pboxsample";
            this.pboxsample.Size = new System.Drawing.Size(342, 75);
            this.pboxsample.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pboxsample.TabIndex = 105;
            this.pboxsample.TabStop = false;
            // 
            // txtTextoObtenido
            // 
            this.txtTextoObtenido.Location = new System.Drawing.Point(9, 272);
            this.txtTextoObtenido.Name = "txtTextoObtenido";
            this.txtTextoObtenido.ReadOnly = true;
            this.txtTextoObtenido.Size = new System.Drawing.Size(342, 20);
            this.txtTextoObtenido.TabIndex = 104;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(6, 256);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(102, 13);
            this.label11.TabIndex = 103;
            this.label11.Text = "Texto Obtenido: ";
            // 
            // listViewBar
            // 
            this.listViewBar.Location = new System.Drawing.Point(6, 68);
            this.listViewBar.Name = "listViewBar";
            this.listViewBar.Size = new System.Drawing.Size(345, 180);
            this.listViewBar.TabIndex = 102;
            this.listViewBar.UseCompatibleStateImageBehavior = false;
            this.listViewBar.Visible = false;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(6, 379);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(345, 23);
            this.progressBar1.TabIndex = 18;
            // 
            // imgResultado
            // 
            this.imgResultado.Location = new System.Drawing.Point(9, 19);
            this.imgResultado.Name = "imgResultado";
            this.imgResultado.Size = new System.Drawing.Size(345, 43);
            this.imgResultado.TabIndex = 1;
            this.imgResultado.TabStop = false;
            // 
            // cmdCargarImagen
            // 
            this.cmdCargarImagen.Location = new System.Drawing.Point(935, 711);
            this.cmdCargarImagen.Name = "cmdCargarImagen";
            this.cmdCargarImagen.Size = new System.Drawing.Size(132, 23);
            this.cmdCargarImagen.TabIndex = 107;
            this.cmdCargarImagen.Text = "Cargar Imagen";
            this.cmdCargarImagen.UseVisualStyleBackColor = true;
            this.cmdCargarImagen.Click += new System.EventHandler(this.cmdCargarImagen_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // chkMostrarImagen
            // 
            this.chkMostrarImagen.AutoSize = true;
            this.chkMostrarImagen.Location = new System.Drawing.Point(387, 711);
            this.chkMostrarImagen.Name = "chkMostrarImagen";
            this.chkMostrarImagen.Size = new System.Drawing.Size(96, 17);
            this.chkMostrarImagen.TabIndex = 106;
            this.chkMostrarImagen.Text = "MostrarImagen";
            this.chkMostrarImagen.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(4, 5);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(246, 20);
            this.label6.TabIndex = 5;
            this.label6.Text = "Imagen de Muestra";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(10, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(71, 58);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // pnlDocument
            // 
            this.pnlDocument.Controls.Add(this.pictureBox1);
            this.pnlDocument.Controls.Add(this.label6);
            this.pnlDocument.Location = new System.Drawing.Point(387, 12);
            this.pnlDocument.Name = "pnlDocument";
            this.pnlDocument.Size = new System.Drawing.Size(680, 675);
            this.pnlDocument.TabIndex = 5;
            // 
            // ProcesamientoArchivos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1084, 741);
            this.Controls.Add(this.cmdCargarImagen);
            this.Controls.Add(this.chkMostrarImagen);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pnlDocument);
            this.Name = "ProcesamientoArchivos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Procesamiento de Archivos";
            this.Load += new System.EventHandler(this.ProcesamientoArchivos_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProcesamientoArchivos_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pboxsample)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgResultado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnlDocument.ResumeLayout(false);
            this.pnlDocument.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button cmdCancelar;
        private System.Windows.Forms.Button cmdProcesar;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblTamaño;
        private System.Windows.Forms.Label lblArchivo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtZoneHeight;
        private System.Windows.Forms.TextBox txtZoneWidth;
        private System.Windows.Forms.TextBox txtZoneY;
        private System.Windows.Forms.TextBox txtZoneX;
        private System.Windows.Forms.PictureBox imgResultado;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblOrigen;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lblCampo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblTipoCampo;
        private System.Windows.Forms.ListView listViewBar;
        private System.Windows.Forms.TextBox txtTextoObtenido;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pboxsample;
        private System.Windows.Forms.Button cmdCargarImagen;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button cmdLog;
        private System.Windows.Forms.CheckBox chkMostrarImagen;
        internal System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel pnlDocument;
    }
}