﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GestionDigitalCore.CoreObjects;
using System.Threading;
using GestionDigitalCore.CoreBussines;
using System.IO;
using AForge.Imaging.Filters;
using System.Drawing.Imaging;
using GestiónDigital.Log;

namespace GestiónDigital.Procesadores
{
    public partial class ProcesamientoArchivos : Form
    {
        public ConfiguracionOCR cfg;
        public List<string> filelist;
        private Thread myThread;
        private bool myAbort = false;
        private bool CancelarProceso = false;
        OCRTools.OCR ocrdoc;
        MODI.Document md;
        private int iGradoRotacion = 0;

        #region Inicializar Formulario
        public ProcesamientoArchivos()
        {
            InitializeComponent();

        }

        private void ProcesamientoArchivos_Load(object sender, EventArgs e)
        {
            md = new MODI.Document();
            ocrdoc = new OCRTools.OCR();
            ocrdoc.CustomerName = "Version5";
            ocrdoc.OrderID = "5108";
            ocrdoc.ProductName = "StandardBar";
            ocrdoc.RegistrationCodes = "4081-0097-0082-4945";
            ocrdoc.ActivationKey = "2750-8441-7000-8746";
        }
        #endregion

        private void AtenderEventosDeWindows()
        {
            for (int i = 0; i < 50; i++)
                Application.DoEvents();
        }

        private void cmdProcesar_Click(object sender, EventArgs e)
        {
            progressBar1.Maximum = filelist.Count;
            progressBar1.Value = 0;
            cmdProcesar.Enabled = false;
            CancelarProceso = false;
            foreach (string archivo in filelist)
            {
                if (CancelarProceso)
                    break;
                ProcesarArchivo(archivo);
                progressBar1.Value++;
                AtenderEventosDeWindows();
            }
            cmdProcesar.Enabled = true;
            progressBar1.Value = 0;
        }

        #region Limpiar OCR
        private void LimpiarOCR()
        {
            try
            {
                ocrdoc.BitmapImage.Dispose();
            }
            catch { }
            try
            {
                ocrdoc.BitmapImageProcessed.Dispose();
            }
            catch { }
            try
            {
                ocrdoc.Dispose();
            }
            catch { }
            try
            {
                pictureBox1.Dispose();

            }
            catch { }
        }
        #endregion 

        #region Cargar Imagen
        private void CargarImagenArchivo(string archivo)
        {

            //limpio las imagenes que tengo en en el OCR
            LimpiarOCR();

            ocrdoc.BitmapImageFile = archivo;
            

            //Si esta seleccionado ver las imagenes en pantalla las muestro
            if (chkMostrarImagen.Checked)
            {
                pictureBox1.ImageLocation = archivo;
                pictureBox1.Visible = true;
                ocrdoc.SetPictureBox(pictureBox1);  
                ocrdoc.LoadPictureBox(archivo);
                pictureBox1.Refresh();              
                ResizeForm();
                AtenderEventosDeWindows();

            }
        }


        private void ResizeForm()
        {
            if (ocrdoc.PictureBox != null)
            {
                ocrdoc.PictureBox.ScaleImage(pnlDocument.Width, pnlDocument.Height);
            }
        }
        #endregion

        #region Procesar Archivo
        protected void ProcesarArchivo(string Archivo)
        {
            BBArchivo bbarchivo = new BBArchivo();
            Archivo archivo = new Archivo();
            try
            {
                //Inicializo los valores del objeto archivo
                archivo = InicializarLog(Archivo);
                //Cargo la imagen del archivo a procesar
                //CargarImagenArchivo(Archivo);

                BBDocumento bbdoc = new BBDocumento();
                Documento tipodoc = new Documento();

                //muestro datos del archivo que se esta procesando en pantalla
                lblOrigen.Text = archivo.PathOrigen;
                lblArchivo.Text = archivo.NombreOriginal;
                lblTamaño.Text = archivo.TamañoOriginal.ToString();

                //obtengo el tipo de documento al que pertenece el archivo
                if (cfg.IdDocumento > 0)
                    tipodoc = bbdoc.getById(cfg.IdDocumento);
                else
                    tipodoc = ObtenerTipoDocumento(archivo);

                if (iGradoRotacion > 0)
                    RotarArchivo(Archivo, iGradoRotacion);

                //guardo en la BD los primeros datos del archivo
                archivo = bbarchivo.Guardar(archivo);
                bool Exito = false;
                double angulo = 0;
                //do
                //{

                    archivo.Error = "";
                    if (tipodoc.ID > 0)//Si se encontro un tipo de documento para el archivo
                    {
                        archivo.IdDocumento = tipodoc.ID;
                        //Proceso los campos del tipo de documento al que pertenece el archivo
                        Exito = ProcesarCamposSeleccionados(Archivo, tipodoc.MisCampos, archivo);
                        //if (!Exito)
                        //{
                        //    Exito = BuscarConCamposDeOtrosDocumentos(Archivo, tipodoc, archivo);
                        //}
                    }
                    else
                    {

                        //Puedo Buscar Probando con todos los campos activos definidos, si tengo suerte, puedo identificarlo.
                        //si le paso un tipo de documento nuevo me va a analizar todos los documentos nuevamente
                        //Exito = BuscarConCamposDeOtrosDocumentos(Archivo, new Documento(), archivo);
                        //if (!Exito)
                        //{
                        Exito = false;
                            archivo.Error = "No se encontro un tipo de documento válido para el archivo.";
                        //}
                    }
                    if (!Exito)
                    {
                        CambiarNombreArchivoError(archivo, archivo.Error);
                    }
                   // angulo -= 90;
                   // Archivo = Rotar(archivo, -90);

               // } while (!Exito && angulo > -360);
                archivo = bbarchivo.Guardar(archivo);

            }
            catch (Exception ex)
            {
                try
                {
                    archivo.Error = ex.Message;
                    archivo = bbarchivo.Guardar(archivo);
                }
                catch { }
            }

        } 
        #endregion
        
        private void RotarArchivo(string archivo, int grado)
        {
            CargarImagenArchivo(archivo);
            ocrdoc.SetRegion(0, 0, ocrdoc.BitmapImage.Width, ocrdoc.BitmapImage.Height, grado);
            ocrdoc.BitmapImage.Save(archivo);
        
        }

        private string Rotar(Archivo archivo, double angulo)
        {
            // Ver la rotacion hace que el archivo quede muy grande y se vaya al carajo la memoria.

            //http://www.codeproject.com/KB/GDI-plus/BitonalImageConverter.aspx
            //http://social.msdn.microsoft.com/Forums/es-ES/dotnetes/thread/119109fd-3540-4b11-82dd-4445f3f84c5c
            ImageCodecInfo imageCodecInfo = GetEncoderInfo("image/tiff");
                System.Drawing.Imaging.Encoder encoder = 
                       System.Drawing.Imaging.Encoder.Compression;
                EncoderParameters encoderParameters = new EncoderParameters(1);

                // Save the bitmap as a TIFF file with group IV compression.

                EncoderParameter encoderParameter = new EncoderParameter(encoder,
                                                    (long)EncoderValue.CompressionLZW);

            AForge.Imaging.Filters.RotateNearestNeighbor R = new RotateNearestNeighbor(angulo);
            Bitmap B = new Bitmap(archivo.PathOrigen+"\\"+archivo.NombreOriginal);
            B = R.Apply(B);
            string NuevoNombre=archivo.NombreOriginal+"_r_"+angulo.ToString()+".tif";
            B.Save(archivo.PathOrigen + "\\" + NuevoNombre, imageCodecInfo, encoderParameters);
            archivo.NombreOriginal = NuevoNombre;
            return archivo.PathOrigen + "\\" + NuevoNombre;
            B.Dispose();
        }


        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }
        private Archivo InicializarLog(string Archivo)
        {
            //creo e inicializo el objeto archivo
            Archivo archivo = new Archivo();
            FileInfo file = new FileInfo(Archivo);
            archivo.NombreOriginal = file.Name;
            archivo.PathOrigen = file.DirectoryName;
            archivo.TamañoOriginal = file.Length;
            archivo.FechaProceso = DateTime.Now;
            archivo.UsuarioProceso = Environment.UserName;

            //muestro datos del archivo que se esta procesando en pantalla
            lblOrigen.Text = archivo.PathOrigen;
            lblArchivo.Text = archivo.NombreOriginal;
            lblTamaño.Text = archivo.TamañoOriginal.ToString();


            return archivo;
        }
        private bool BuscarConCamposDeOtrosDocumentos(string archivo, Documento tipodoc, Archivo arch)
        {
         
            BBCampo BBC = new BBCampo();
            
            List<Campo> listado = BBC.EjecutarConsulta("Select c.* from Campo c join documento d on c.iddocumento = d.iddocumento and d.iddocumento <> "+ tipodoc.ID.ToString() + " and d.activo = 1 and c.esidentificador = 0 order by c.orden");

            return ProcesarCamposSeleccionados(archivo, listado, arch);
        }

        private bool ProcesarCamposSeleccionados(string archivo, List<Campo> listado, Archivo arch)
        {
            foreach (Campo campo in listado)
            {
                //En el caso que venga por defecto el tipo de documento ¿No deberíamos leer el campo tipo identificador?
                if (!campo.EsIdentificador)
                {
                    lblCampo.Text = campo.Nombre;

                    if (campo.TipoCampo == eTipoCampo.CodigoBarra)
                        lblTipoCampo.Text = "Código de Barra";
                    else
                        lblTipoCampo.Text = "Texto";

                    ValorArchivo valor = new ValorArchivo();
                    valor.idArchivo = arch.ID;
                    valor.IdCampo = campo.ID;
                    valor.Valor = LeerValorCampo(archivo, campo);

                    new BBValorArchivo().Guardar(valor);
                    if (valor.Valor.Length == 10)
                    {
                        CambiarNombreArchivo(arch, valor.Valor);
                        return true;
                    }
                }
            }
            return false;
        }

        private void CambiarNombreArchivo(Archivo Arch, string valor)
        {
            Bitmap BM = new Bitmap(Arch.PathOrigen+"\\"+Arch.NombreOriginal);
            string NuevoNombre; int i = 0;
            do
            {
                if (i == 0)
                    NuevoNombre = cfg.DirectorioDestino + "\\" + valor + ".jpg";
                else
                    NuevoNombre = cfg.DirectorioDestino + "\\" + valor + "_" + i.ToString() + ".jpg";

                i++;
            } while (File.Exists(NuevoNombre));

            AForge.Imaging.Filters.ResizeNearestNeighbor R = new ResizeNearestNeighbor((int)(BM.Width * .2), (int)(BM.Height * .2));
            BM = R.Apply(BM);
            BM.Save(NuevoNombre, ImageFormat.Jpeg);
            bool borrado = false;
            do
            {
                try
                {
                    File.Delete(Arch.PathOrigen + "\\" + Arch.NombreOriginal);
                    borrado = true;
                }
                catch { }
            } while (!borrado);
            Arch.NombreModificado = NuevoNombre.Substring(cfg.DirectorioDestino.Length + 1, NuevoNombre.Length - cfg.DirectorioDestino.Length - 1);
            Arch.TamañoModificado = File.ReadAllBytes(NuevoNombre).Length;
            Arch.PathDestino = cfg.DirectorioDestino;


        }
        private void CambiarNombreArchivoError(Archivo Arch, string Error)
        {
            Bitmap BM = new Bitmap(Arch.PathOrigen + "\\" + Arch.NombreOriginal);
            string NuevoNombre; int i = 0;
            do
            {
                if (i == 0)
                    NuevoNombre = cfg.DirectorioErrores + "\\" + DateTime.Now.Ticks.ToString() + ".tif";
                else
                    NuevoNombre = cfg.DirectorioErrores + "\\" + DateTime.Now.Ticks.ToString() + "_" + i.ToString() + ".tif";

                i++;
            } while (File.Exists(NuevoNombre));

            File.Copy(Arch.PathOrigen + "\\" + Arch.NombreOriginal, NuevoNombre);
            bool borrado = false; int intentos = 0;
            do
            {
                try
                {
                    intentos++;
                    File.Delete(Arch.PathOrigen + "\\" + Arch.NombreOriginal);
                    borrado = true;
                    
                }
                catch { }
            } while (!borrado && intentos<10);
            Arch.NombreModificado = NuevoNombre.Substring(cfg.DirectorioErrores.Length + 1, NuevoNombre.Length - cfg.DirectorioErrores.Length - 1);
            Arch.TamañoModificado = File.ReadAllBytes(NuevoNombre).Length;
            Arch.PathDestino = cfg.DirectorioErrores;
            Arch.Error = Error;

        }
        private string ProcesarValorLeido(string valor, Campo CampoActual)
        {
            if (!CampoActual.EsIdentificador)
            {
                valor = valor.Trim();
                string result = "";
                char[] caracteres = new char[valor.Length];
                valor.CopyTo(0, caracteres, 0, valor.Length);
                foreach (char c in caracteres)
                {
                    try
                    {
                        int.Parse(c.ToString());
                        result += c.ToString();
                    }
                    catch
                    {
                    }
                }
                if (result.Length > 11)
                    return "";
                else
                    if (result.Length == 11)
                        return result.Substring(0, 10);
                    else if (result.Length == 10)
                        return result;
                    else
                        return "";
            }
            else
            {
                return valor;
            }
        }

        //Buscar a que tipo de documento pertenece el archivo
        protected Documento ObtenerTipoDocumento(Archivo archivo)
        {
            BBDocumento bbtipoDoc = new BBDocumento();
            BBCampo bbcampo = new BBCampo();
            Documento tipodoc = new Documento();

            List<Documento> listdoc = bbtipoDoc.GetAll();

            foreach (Documento documento in listdoc)
            {
                iGradoRotacion = 0;
                foreach (Campo campo in documento.MisCampos)
                {

                    if (campo.EsIdentificador)
                    {
                        if (LeerValorCampo(archivo.PathOrigen + "\\" + archivo.NombreOriginal, campo).ToLower().Contains(campo.ValorIdentificacion.ToLower()))
                        {                         
                            iGradoRotacion = campo.GradoRotacion;
                            tipodoc = documento;
                            return tipodoc;
                        }
                    }
                }
            }
            return tipodoc;
        }        
        protected string LeerValorCampo(string archivo, Campo campo)
        {

            myAbort = false;
            cmdCancelar.Enabled = true;
            string filename = DateTime.Now.Ticks.ToString() + ".tiff";
            try
            {
               
                string strText = "";

                #region Setear Area

                txtZoneX.Text = campo.XInicial.ToString();
                txtZoneY.Text = campo.YInicial.ToString();
                txtZoneWidth.Text = campo.Ancho.ToString();
                txtZoneHeight.Text = campo.Alto.ToString();
                AtenderEventosDeWindows();


                CargarImagenArchivo(archivo);

                //Verifico si hay que scannear un area o todo el documento
                if (txtZoneHeight.Text == "0")
                {
                    bool lResult = ocrdoc.SetRegion(0,
                              0,
                              ocrdoc.BitmapImage.Width,
                              ocrdoc.BitmapImage.Height,
                              campo.GradoRotacion);
                }
                else
                {
                    bool lResult = ocrdoc.SetRegion(campo.XInicial,
                              campo.YInicial,
                              campo.Ancho,
                              campo.Alto,
                              campo.GradoRotacion);
                }
 
                pboxsample.Image = (Image)ocrdoc.BitmapImage;
                //ProcesarSubImagen(campo, filename);

                #endregion              

                //Verifico si el campo a leer es codigo de barra o un texto
                if (campo.TipoCampo == eTipoCampo.CodigoBarra)
                {
                    #region CODIGO DE BARRA
                            #region Setear Area
                            //Verifico si hay que scannear un area o todo el documento

                            if (txtZoneHeight.Text == "0")
                                listViewBar.Visible = true;
                            else
                                listViewBar.Visible = false;

                            #endregion

                            #region Procesar Imagen
                            //ocrdoc.BitmapImageFile = filename;
                            ocrdoc.OCRType = OCRTools.OCRType.Barcode;
                            ocrdoc.ErrorCorrection = OCRTools.ErrorCorrectionTypes.IterateBrightness;
                            myThread = ocrdoc.Process_Start();

                            while (myThread.IsAlive)
                            {
                                Application.DoEvents();

                                this.Cursor =System.Windows.Forms.Cursors.WaitCursor;
                            }
                            #endregion


                            #region Obtener Resultados
                            if (ocrdoc.ThreadError)
                                strText = "Error: " + ocrdoc.ThreadErrorMessage;

                            else
                            {
                                if (txtZoneHeight.Text == "0")
                                    ocrdoc.SetListViewBarcode(listViewBar);
                                else//Para poner en producción mostrar el resultado en texto y no en imagen
                                    imgResultado.Image = ocrdoc.DemoResults;

                                strText = ocrdoc.Text;
                            }
                            #endregion 
                    #endregion
                }
                else
                {
                    #region OCR TEXTO
                    strText = "";
                    listViewBar.Visible = false;
                    //File.Copy(archivo, filename);                    
                    //ocrdoc.SetRegion(campo.XInicial,
                    //     campo.YInicial,
                    //     campo.Ancho,
                    //     campo.Alto);

                    ocrdoc.BitmapImage.Save(filename,System.Drawing.Imaging.ImageFormat.Tiff);

                    //ProcesarSubImagen(campo, filename);
                    md = new MODI.Document();
                    md.Create(filename);

                    // The Create method grabs the picture from
                    //   disk snd prepares for OCR.
                    // Do the OCR.
                    
                    md.OCR(MODI.MiLANGUAGES.miLANG_SPANISH, true, true);
                    md.Save();

                    // Get the first (and only image)
                    MODI.Image image = (MODI.Image)md.Images[0];

                    // Get the layout.
                    MODI.Layout layout = image.Layout;

                    // Loop through the words.
                    for (int j = 0; j < layout.Words.Count; j++)
                    {
                        // Get this word.
                        MODI.Word word = (MODI.Word)layout.Words[j];
                        // Add a blank space to separate words.
                        if (strText.Length > 0)
                        {
                            strText += " ";
                        }
                        // Add the word.
                        strText += ProcesarValorLeido(word.Text, campo);
                        if (strText.Length == 10)
                            break;
                    }
                    // Close the MODI.Document object.
                    md.Close(false);
                }

                strText = ProcesarValorLeido(strText,campo);
                txtTextoObtenido.Text = strText;
                return strText; 
                    #endregion
            }
            catch (Exception ex)
            {
                return "Error: " + ex.Message;
            }
            finally
            {
                try
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(md);
                    md = null;
                    // Force garbage collection so image file is closed properly
                    GC.Collect();
                    GC.WaitForPendingFinalizers();               
                    File.Delete(filename);
                }catch{}
            }



        }

        private void ProcesarSubImagen(Campo campo, string filename)
        {
            Bitmap BM;
            BM = (Bitmap)ocrdoc.BitmapImage.Clone();


            //if (campo.TipoCampo == eTipoCampo.CodigoBarra)
            //{
            //    AForge.Imaging.Filters.ResizeNearestNeighbor R = new ResizeNearestNeighbor((int)(BM.Width * .7), (int)(BM.Height * .7));
            //    BM = R.Apply(BM);

            //}
            if (campo.MejorarImagen)
            {
                BM = PreProcesarTexto(BM, campo);
            }

            BM.Save(filename, ImageFormat.Tiff);
            BM.Dispose();
            BM = null;
            string imagenpequeña = DateTime.Now.Ticks.ToString() + ".tiff";
            File.Copy(filename, imagenpequeña);
            pboxsample.ImageLocation = imagenpequeña; AtenderEventosDeWindows();
        }
        private Bitmap PreProcesarTexto(Bitmap image, Campo campo)
        {

            
            
            // set wait cursor
            this.Cursor = Cursors.WaitCursor;
            Bitmap Mejorada = (Bitmap)image.Clone();
            if (!campo.EsIdentificador)
            {
                AForge.Imaging.Filters.ResizeNearestNeighbor R = new ResizeNearestNeighbor((int)(image.Width * .3), (int)(image.Height * .3));
                AForge.Imaging.Filters.GaussianBlur G = new GaussianBlur(1.4, 9);
                Mejorada = G.Apply(R.Apply(image));
            }

            
            Mejorada.Save("b.tiff", ImageFormat.Tiff);
            Cursor.Current = Cursors.Default;
            return Mejorada;

        }


        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (ocrdoc.PictureBox != null)
            {
                txtZoneX.Text = ocrdoc.PictureBox.X.ToString();
                txtZoneY.Text = ocrdoc.PictureBox.Y.ToString();
                txtZoneWidth.Text = ocrdoc.PictureBox.Width.ToString();
                txtZoneHeight.Text = ocrdoc.PictureBox.Height.ToString();
            }
        }

        private void cmdCancelar_Click(object sender, EventArgs e)
        {
            myThread.Abort();
            cmdProcesar.Enabled = true;
            progressBar1.Value = 0;
            CancelarProceso = true;
        }


        private void cmdCargarImagen_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            chkMostrarImagen.Checked = true;
            if (File.Exists(openFileDialog1.FileName))
            {
                CargarImagenArchivo(openFileDialog1.FileName);
            }
        }

        private void ProcesamientoArchivos_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (myThread != null)
            {
                myThread.Abort();
            }
        }

        private void cmdLog_Click(object sender, EventArgs e)
        {
            frmOCRLogList f = new frmOCRLogList();
            f.Show();
        }




    }
}