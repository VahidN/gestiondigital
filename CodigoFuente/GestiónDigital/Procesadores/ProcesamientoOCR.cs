﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GestionDigitalCore.CoreObjects;
using CoreEngine;

namespace GestiónDigital.Procesadores
{
    public partial class ProcesamientoOCR : Form
    {
        private int ArchivosProcesados = 0;
        private Bitmap bmp; 
        public ConfiguracionOCR cfgocr;
        public List<string> filelist;
        public ProcesamientoOCR()
        {
            InitializeComponent();
        }

        private void ProcesamientoOCR_Load(object sender, EventArgs e)
        {
            UbicarPantalla();
            AdministrarTransparencia();
            GestionarAnimacion();
            ProcesadorOCR ocrp = new ProcesadorOCR(Application.StartupPath);
            ocrp.ProcesoIniciado += new EventoBase(ocrp_ProcesoIniciado);
            ocrp.ProcesoFinalizado += new EventoBase(ocrp_ProcesoFinalizado);
            ocrp.ProcesandoArchivo += new _EventoDatosArchivoEnProceso(ocrp_ProcesandoArchivo);
            ocrp.PreArchivoProcesadoOk += new _EventoFinProcesoArchivoOk(ocrp_PreArchivoProcesadoOk);
            ocrp.PreArchivoProcesadoError += new _EventoFinProcesoArchivoError(ocrp_PreArchivoProcesadoError);
            ocrp.ArchivoProcesadoOk += new _EventoFinProcesoArchivoOk(ocrp_ArchivoProcesadoOk);
            ocrp.ArchivoProcesadoError += new _EventoFinProcesoArchivoError(ocrp_ArchivoProcesadoError);
            //ocrp.WaitingSending += new _WaitingSending(ocrp_WaitingSending);
            //ocrp.WaitPeriodoFinished += new _WaitPeriodoFinished(ocrp_WaitPeriodoFinished);
            ocrp.Working += new EventoBase(ocrp_Working);
            AtenderEventosDeWindows();
            ocrp.ProcesarArchivos(cfgocr, filelist);
            
        }
        private void UbicarPantalla()
        {
            this.Top = Screen.PrimaryScreen.WorkingArea.Height - this.Height;
            this.Left = Screen.PrimaryScreen.WorkingArea.Width - this.Width;

        }
        private void AdministrarTransparencia()
        {
            T1.Enabled = true;
        }
        private void GestionarAnimacion()
        {
            bmp = new Bitmap(global::GestiónDigital.Properties.Resources.file_transfer_between_computers_md_wht);
            ImageAnimator.Animate(
            bmp,
            new EventHandler(this.OnFrameChanged));
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ProcesamientoOCR_Paint);
        }
        private void ProcesamientoOCR_Paint(object sender, PaintEventArgs e)
        {
            ImageAnimator.UpdateFrames();
            e.Graphics.DrawImage(this.bmp, new Point(0, 0));
        } 
        private void OnFrameChanged(object o, EventArgs e)
        {
            this.Invalidate();
        }
        void ocrp_ProcesoIniciado()
        {
            //AbrirLogFTP();
            //AtenderEventosDeWindows();

        }

        void ocrp_ProcesoFinalizado()
        {
            AtenderEventosDeWindows();
            Application.ExitThread();
            // CerrarLogFTP();
        }
        private void AtenderEventosDeWindows()
        {
            for (int i = 0; i < 50; i++)
                Application.DoEvents();
        }
        long bytesEnviados = 0;
        long ArchivosEnviados = 0;
        void ocrp_ProcesandoArchivo(string FileName, long ByteSize, decimal PorcentajeAvance, ref bool Cancelar)
        {
            //InicializarFileLog();
            ArchivosProcesados++;
            txtActualFile.Text = FileName + ": " + GetByteSize(ByteSize);
            AtenderEventosDeWindows();

        }
        void ocrp_PreArchivoProcesadoOk(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal)
        {
            bytesEnviados += ByteSize;
            ArchivosEnviados++;
            txtBytesSend.Text = GetByteSize(bytesEnviados);


            txtFilesSends.Text = ArchivosEnviados.ToString();
            progressBar1.Value = Convert.ToInt32(PorcentajeAvance * 100);

            AtenderEventosDeWindows();

        }
        void ocrp_PreArchivoProcesadoError(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal, string Error)
        {
            AtenderEventosDeWindows();

        }
        void ocrp_ArchivoProcesadoOk(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal)
        {/*
            bytesEnviados += ByteSize;
            ArchivosEnviados++;
            if (bytesEnviados > 1024)
            {
                if (bytesEnviados / 1024 > 1024)
                {
                    txtBytesSend.Text = (bytesEnviados / (1024 * 1024)) + " MBytes";
                }
                else
                {
                    txtBytesSend.Text = (bytesEnviados / 1024) + " KBytes";
                }
            }
            else
            {
                txtBytesSend.Text = bytesEnviados.ToString() + " Bytes";
            }
            long velocidadenbytes = (bytesEnviados / TiempoTotal.Seconds);
            lblVelocidad.Text = GetByteSize(velocidadenbytes) + "/Seg.";           
            txtFilesSends.Text = ArchivosEnviados.ToString();
            progressBar1.Value = Convert.ToInt32(PorcentajeAvance * 100);

            RegistrarLog(FileName, ByteSize, TiempoTotal, "");
            AtenderEventosDeWindows();
         */   
        }
        void ocrp_ArchivoProcesadoError(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal, string Error)
        {
            //  RegistrarLog(FileName, ByteSize, TiempoTotal, Error);
            AtenderEventosDeWindows();

        }
        void ocrp_WaitingSending(int waittime)
        {
            txtActualFile.Text = "Proceso Auto pausado por: " + waittime.ToString() + " Segundos";
            AtenderEventosDeWindows();
        }
        void ocrp_WaitPeriodoFinished(string Archivo)
        {
            txtActualFile.Text = Archivo;
            AtenderEventosDeWindows();
        }
        void ocrp_Working()
        {
            AtenderEventosDeWindows();
        }
        private string GetByteSize(long Bytes)
        {
            string r = "";
            if (Bytes > 1024)
            {
                if (Bytes / 1024 > 1024)
                {
                    r = (Bytes / (1024 * 1024)) + " MBytes";
                }
                else
                {
                    r = (Bytes / 1024) + " KBytes";
                }
            }
            else
            {
                r = Bytes.ToString() + " Bytes";
            }
            return r;
        }


        private void T1_Tick(object sender, EventArgs e)
        {
            AtenderEventosDeWindows();
            if (this.Opacity > 0.2)
            {
                this.Opacity = this.Opacity - 0.01;
            }
            else
            {
                this.Opacity = 0;
                this.WindowState = FormWindowState.Minimized;
                T1.Enabled = false;
            }
        }
    }
}