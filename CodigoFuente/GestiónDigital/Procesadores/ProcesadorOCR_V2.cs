﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GestionDigitalCore.CoreObjects;
using System.Threading;
using GestionDigitalCore.CoreBussines;
using System.IO;
using AForge.Imaging.Filters;
using System.Drawing.Imaging;
using GestiónDigital.Log;
using ToolBox;
using CoreEngine;

namespace GestiónDigital.Procesadores
{
    public partial class ProcesadorOCR_V2 : Form
    {
        public ConfiguracionOCR cfg;
        private Procesador px;
        private long ArchivosOk;
        private long ArchivosError;
        public bool AutoRun = false;
        private DateTime FechaHoraInicioProceso;
        private bool bCancelar = false;
        private string CarpetaTemporal;

        public ProcesadorOCR_V2()
        {            
           
     
            InitializeComponent();

        }


        #region MAnejadores de PRocesador OCR
        private void IniciarProcesador()
        {
            px = new ProcesadorOCR(Application.StartupPath);
            ((ProcesadorOCR)px).ArchivoProcesadoOCROk += new _EventoFinProcesoOCRArchivoOk(ProcesadorOCR_V2_ArchivoProcesadoOCROk);
            ((ProcesadorOCR)px).ArchivoProcesadoOCRError += new _EventoFinProcesoOCRArchivoError(ProcesadorOCR_V2_ArchivoProcesadoOCRError);
            px.ArchivoProcesadoError += new _EventoFinProcesoArchivoError(px_ArchivoProcesadoError);
            px.ProcesoIniciado += new EventoBase(px_ProcesoIniciado);
            px.ProcesandoArchivo += new _EventoDatosArchivoEnProceso(px_ProcesandoArchivo);
            px.Working += new EventoBase(px_Working);
            px.ProcesoFinalizado += new EventoBase(px_ProcesoFinalizado);
            ((ProcesadorOCR)px).ProcesandoOCR += new EventoBaseOCR(ProcesadorOCR_V2_ProcesandoOCR);

        }

        void px_ArchivoProcesadoError(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal, string Error)
        {
            ArchivosError++;
            txtProcesados.Text = ((int)(ArchivosOk + ArchivosError)).ToString();
            txtError.Text = ArchivosError.ToString();
            progressBar1.Value = (int)PorcentajeAvance;

            CalcularTiempoPromedioActual();


            AtenderEventosDeWindows();
        }

        private void LiberarProcesador()
        {
            ((ProcesadorOCR)px).ArchivoProcesadoOCROk -= ProcesadorOCR_V2_ArchivoProcesadoOCROk;
            ((ProcesadorOCR)px).ArchivoProcesadoOCRError -= ProcesadorOCR_V2_ArchivoProcesadoOCRError;
            px.ProcesoIniciado -= px_ProcesoIniciado;
            px.ProcesandoArchivo -= px_ProcesandoArchivo;
            px.Working -= px_Working;
            px.ProcesoFinalizado -= px_ProcesoFinalizado;
            ((ProcesadorOCR)px).ProcesandoOCR -= ProcesadorOCR_V2_ProcesandoOCR;
            px.ArchivoProcesadoError -= px_ArchivoProcesadoError;
            px = null;
        }
        void px_ProcesoIniciado()
        {
            FechaHoraInicioProceso = DateTime.Now;
        }

        void ProcesadorOCR_V2_ArchivoProcesadoOCRError(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal, string Error)
        {
            ArchivosError++;
            txtProcesados.Text = ((int)(ArchivosOk + ArchivosError)).ToString();
            txtError.Text = ArchivosError.ToString();
            progressBar1.Value = (int)PorcentajeAvance;

            CalcularTiempoPromedioActual();


            AtenderEventosDeWindows();
        }

        private void CalcularTiempoPromedioActual()
        {
            try
            {
                TimeSpan DiferenciaTicks = DateTime.Now.Subtract(FechaHoraInicioProceso);
                double PromTicks = DiferenciaTicks.Ticks / (ArchivosOk + ArchivosError);
                TimeSpan TS = new TimeSpan((long)PromTicks);
                txtTiempoProm.Text = TS.Hours + ":" + TS.Minutes + ":" + TS.Seconds + "." + TS.Milliseconds;
            }
            catch (Exception ex)
            {
                FSO_NH.log4Net.FSOLog4Net.LogDebug("Error en CalcularTiempoPromedioActual: " + ex.Message);
            }
        }

        void ProcesadorOCR_V2_ArchivoProcesadoOCROk(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal)
        {
            ArchivosOk++;
            txtProcesados.Text = ((int)(ArchivosOk + ArchivosError)).ToString();
            txtIdentificados.Text = ArchivosOk.ToString();
            progressBar1.Value = (int)PorcentajeAvance;
            CalcularTiempoPromedioActual();
            AtenderEventosDeWindows();

        }

        void ProcesadorOCR_V2_ProcesandoOCR(string Accion)
        {
            txtEstado.Text = Accion;
            AtenderEventosDeWindows();
        }

        void px_ProcesoFinalizado()
        {
            txtEstado.Text = "Finalizado";
            AtenderEventosDeWindows();
        }

        void px_Working()
        {
            AtenderEventosDeWindows();
        }

        void px_ProcesandoArchivo(string FileName, long ByteSize, decimal PorcentajeAvance, ref bool Cancelar)
        {
            txtEstado.Text = "Procesando Archivo...";
            lblOrigen.Text = FileName;
            lblTamaño.Text = ToolBox.FileTools.GetFileSizeFormateado(ByteSize);
            AtenderEventosDeWindows();
            Cancelar = bCancelar;
        }

        
        #endregion

        #region Eventos de Windows

            private void rocesadorOCR_V2_Load(object sender, EventArgs e)
            {
              
                if (AutoRun) {
                    T1.Enabled = true;
                }
            }

            private void ProcesadorOCR_V2_FormClosing(object sender, FormClosingEventArgs e)
            {
                cfg.DirectorioTemp = CarpetaTemporal;
            }

            private void cmdProcesar_Click(object sender, EventArgs e)
            {
                try
                {
                    this.Cursor =System.Windows.Forms.Cursors.WaitCursor;
                    IniciarProceso();
                }
                catch (Exception)
                {
                    this.Cursor =System.Windows.Forms.Cursors.Default;
                    throw;
                }
                finally {
                    this.Cursor =System.Windows.Forms.Cursors.Default;
                }
            }
            private void cmdCancelar_Click(object sender, EventArgs e)
            {
                bCancelar = true;
                EliminarArchivos();
                this.Cursor =System.Windows.Forms.Cursors.Default;

            }
            private void T1_Tick(object sender, EventArgs e)
            {
                T1.Enabled = false;
                IniciarProceso();
                Application.Exit();

            }
            private void T2_Tick(object sender, EventArgs e)
            {
                T2.Enabled = false;
                IniciarProceso();

            }
            //private void cmdLog_Click(object sender, EventArgs e)
            //{
            //    frmOCRLogList f = new frmOCRLogList();
            //    f.Show();
            //}
        #endregion
        #region Metodos Genericos
            private void AtenderEventosDeWindows()
                {
                    for (int i = 0; i < 50; i++)
                        Application.DoEvents();
                }

            private void CrearCarpetaTemporal() {
                    if (cfg.DirectorioTemp == "")
                        cfg.DirectorioTemp = Application.StartupPath.ToString() + "\\Temp";
                    CarpetaTemporal = cfg.DirectorioTemp;      
                    if (!Directory.Exists(cfg.DirectorioTemp))
                        Directory.CreateDirectory(cfg.DirectorioTemp);

                    FSO_NH.log4Net.FSOLog4Net.LogDebug("Carpeta Temporal: " + cfg.DirectorioTemp);
                }

            private void InicializarFormulario() {
                txtAProcesar.Text = "0";
                txtError.Text = "0";
                txtIdentificados.Text = "0";
                txtProcesados.Text = "0";
                txtTiempoProm.Text = "0";
                txtTiempoTotal.Text = "0";
                txtHoraFin.Text = "";
                txtHoraInicio.Text = "";
            
            }

            private void VerificarCarpetas() 
            {
                try
                {
                    if (Directory.Exists(cfg.DirectorioDestino))
                        Directory.CreateDirectory(cfg.DirectorioDestino);

                    if (Directory.Exists(cfg.DirectorioDuplicados))
                        Directory.CreateDirectory(cfg.DirectorioDuplicados);

                    if (Directory.Exists(cfg.DirectorioErrores))
                        Directory.CreateDirectory(cfg.DirectorioErrores);

                    if (Directory.Exists(cfg.DirectorioOrigen))
                        Directory.CreateDirectory(cfg.DirectorioOrigen);
                }
                catch { }

            }

            private void IniciarProceso()
                {
                    try
                    {
                        decimal TamanoLote = numMaxLote.Value;
                        FSO_NH.log4Net.FSOLog4Net.LogDebug("====================== INICIO ============================");
                        IniciarProcesador();
                        T2.Enabled = false;
                        CrearCarpetaTemporal();
                        VerificarCarpetas();
                        InicializarFormulario();
                        ArchivosOk = 0;
                        ArchivosError = 0;
                        cmdCancelar.Enabled = true;
                        txtEstado.Text = "Buscando Archivos...";
                        List<string> filelist = FileTools.GetFilesRecursive(cfg.DirectorioOrigen, cfg.TipoArchivo, Convert.ToInt32(TamanoLote));
                        if (filelist.Count == 0)
                        {                   
                            txtEstado.Text = "No se encontraron Archivos, se reintentará en unos minutos.";
                            T2.Enabled = true;
                            return;                        
                        }
                        List<string> Lote = new List<string>();
                        FSO_NH.log4Net.FSOLog4Net.LogDebug("Archivos Encontrados: " + filelist.Count.ToString());
                        FSO_NH.log4Net.FSOLog4Net.LogDebug("********************* A PROCESAR: ******************");
                        if (filelist.Count > TamanoLote)
                        {
                            for (int i = 0; i < TamanoLote; i++)
                            {
                                FSO_NH.log4Net.FSOLog4Net.LogDebug("Arch: \t\t" + filelist[i]);
                                Lote.Add(filelist[i]);
                            }
                        }
                        else {
                            Lote = filelist;
                        }
                        FSO_NH.log4Net.FSOLog4Net.LogDebug("****************************************************");
                        txtEstado.Text = "Iniciando Proceso";
                        DateTime FI = DateTime.Now;
                        txtHoraInicio.Text = FI.ToString("dd/MM/yyyy hh:mm:ss");
                        progressBar1.Maximum = Lote.Count;
                        txtAProcesar.Text = Lote.Count.ToString();
                        progressBar1.Value = 0;                    
                        cmdProcesar.Enabled = false;
                        ((ProcesadorOCR)px).IniciarConCampos = chkIniciarConCampos.Checked;
                        cfg.RotarImagenes = chkRotar.Checked;
                        cfg.MaxKBytesToProcess = Convert.ToInt32(NumMaxKB.Value);
                        px.ProcesarArchivos(cfg, Lote);

                        cmdProcesar.Enabled = true;
                        progressBar1.Value = 0;
                        DateTime FF = DateTime.Now;
                        txtHoraFin.Text = FF.ToString("dd/MM/yyyy hh:mm:ss");
                        TimeSpan tt = FF.Subtract(FI);
                        txtTiempoTotal.Text = tt.Hours + ":" + tt.Minutes + ":" + tt.Seconds+"."+tt.Milliseconds;
                        EliminarArchivos();
                        cmdCancelar.Enabled = false;
                        
                    }
                    catch (Exception ex)
                    {
                        FSO_NH.log4Net.FSOLog4Net.LogDebug("Error: " + ex.Message);
                    
                    }
                    finally {
                        T2.Enabled = true;
                        FSO_NH.log4Net.FSOLog4Net.LogDebug("====================== FIN ============================");
                        this.Cursor =System.Windows.Forms.Cursors.Default;
                        LiberarProcesador();
                    }
                }


       
             private void EliminarArchivos()
                {
                    //No es necesario eliminar los archivos ya que son movidos a la carpeta de OK's
                    #region Limpieza de Carpetas Vacias del directorio origen
                    FSO_NH.log4Net.FSOLog4Net.LogDebug("Limpiando Carpeta Scanner");
                    string[] directorios = Directory.GetDirectories(cfg.DirectorioOrigen);
                    foreach (string dir in directorios)
                    {
                        List<string> Archivos = ToolBox.FileTools.GetFilesRecursive(dir, cfg.TipoArchivo);
                        if (Archivos.Count == 0)
                        {
                            #region Eliminacion de Archivos Temporales de Windows
                            List<string> ArchivosTemp = ToolBox.FileTools.GetFilesRecursive(dir, "*.db");
                            foreach (string archTemp in ArchivosTemp)
                            {
                                    try
                                    {
                                        File.Delete(archTemp);
                                        FSO_NH.log4Net.FSOLog4Net.LogDebug("Archivo Eliminado: " + archTemp);
                                    }
                                    catch{}
                            } 
                            #endregion
                            try
                            {
                                Directory.Delete(dir);
                                FSO_NH.log4Net.FSOLog4Net.LogDebug("Carpeta Eliminada: " + dir);
                            }
                            catch {
                                FSO_NH.log4Net.FSOLog4Net.LogDebug("Error Eliminando: " + dir);
                            }
                        }
                    }
                    
                    #endregion
                    #region Limpieza de Carpetas Temporal de este proceso
                    FSO_NH.log4Net.FSOLog4Net.LogDebug("Limpiando Carpeta Temporales");
                    List<String>Listado = ToolBox.FileTools.GetFilesRecursive(cfg.DirectorioTemp);
                    foreach (string archTemp in Listado)
                    {
                        try
                        {
                            File.Delete(archTemp);
                        }
                        catch{}             
                    } 
                    #endregion

                    txtEstado.Text = "Inactivo";

                }
        #endregion

  


    }
}