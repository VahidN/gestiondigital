﻿namespace GestiónDigital.Procesadores
{
    partial class ProcesadorOCR_V2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProcesadorOCR_V2));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtAProcesar = new System.Windows.Forms.TextBox();
            this.cmdProcesar = new System.Windows.Forms.Button();
            this.cmdCancelar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtEstado = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtError = new System.Windows.Forms.TextBox();
            this.txtIdentificados = new System.Windows.Forms.TextBox();
            this.txtProcesados = new System.Windows.Forms.TextBox();
            this.txtTiempoTotal = new System.Windows.Forms.TextBox();
            this.txtTiempoProm = new System.Windows.Forms.TextBox();
            this.txtHoraFin = new System.Windows.Forms.TextBox();
            this.txtHoraInicio = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.numMaxLote = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.NumMaxKB = new System.Windows.Forms.NumericUpDown();
            this.chkRotar = new System.Windows.Forms.CheckBox();
            this.chkIniciarConCampos = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.lblOrigen = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblTamaño = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.T1 = new System.Windows.Forms.Timer(this.components);
            this.T2 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxLote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumMaxKB)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.progressBar1);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Location = new System.Drawing.Point(0, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(733, 415);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "OCR";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.txtAProcesar);
            this.panel1.Controls.Add(this.cmdProcesar);
            this.panel1.Controls.Add(this.cmdCancelar);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtEstado);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.txtError);
            this.panel1.Controls.Add(this.txtIdentificados);
            this.panel1.Controls.Add(this.txtProcesados);
            this.panel1.Controls.Add(this.txtTiempoTotal);
            this.panel1.Controls.Add(this.txtTiempoProm);
            this.panel1.Controls.Add(this.txtHoraFin);
            this.panel1.Controls.Add(this.txtHoraInicio);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Location = new System.Drawing.Point(10, 240);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(720, 170);
            this.panel1.TabIndex = 127;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(385, 85);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(91, 13);
            this.label17.TabIndex = 146;
            this.label17.Text = "Archivos Error:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(385, 62);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(137, 13);
            this.label16.TabIndex = 145;
            this.label16.Text = "Archivos Identificados:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(385, 40);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(130, 13);
            this.label15.TabIndex = 144;
            this.label15.Text = "Archivos Procesados:";
            // 
            // txtAProcesar
            // 
            this.txtAProcesar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAProcesar.Location = new System.Drawing.Point(533, 17);
            this.txtAProcesar.Name = "txtAProcesar";
            this.txtAProcesar.Size = new System.Drawing.Size(173, 20);
            this.txtAProcesar.TabIndex = 143;
            this.txtAProcesar.Text = "0";
            this.txtAProcesar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cmdProcesar
            // 
            this.cmdProcesar.Location = new System.Drawing.Point(552, 134);
            this.cmdProcesar.Name = "cmdProcesar";
            this.cmdProcesar.Size = new System.Drawing.Size(75, 23);
            this.cmdProcesar.TabIndex = 128;
            this.cmdProcesar.Text = "Procesar";
            this.cmdProcesar.UseVisualStyleBackColor = true;
            this.cmdProcesar.Click += new System.EventHandler(this.cmdProcesar_Click);
            // 
            // cmdCancelar
            // 
            this.cmdCancelar.Enabled = false;
            this.cmdCancelar.Location = new System.Drawing.Point(632, 134);
            this.cmdCancelar.Name = "cmdCancelar";
            this.cmdCancelar.Size = new System.Drawing.Size(75, 23);
            this.cmdCancelar.TabIndex = 127;
            this.cmdCancelar.Text = "Cancelar";
            this.cmdCancelar.UseVisualStyleBackColor = true;
            this.cmdCancelar.Click += new System.EventHandler(this.cmdCancelar_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(384, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 13);
            this.label3.TabIndex = 142;
            this.label3.Text = "Archivos a Procesar:";
            // 
            // txtEstado
            // 
            this.txtEstado.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEstado.Location = new System.Drawing.Point(201, 109);
            this.txtEstado.Name = "txtEstado";
            this.txtEstado.Size = new System.Drawing.Size(505, 20);
            this.txtEstado.TabIndex = 141;
            this.txtEstado.Text = "Inactivo";
            this.txtEstado.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(6, 111);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(86, 13);
            this.label18.TabIndex = 140;
            this.label18.Text = "Estado Actual";
            // 
            // txtError
            // 
            this.txtError.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtError.Location = new System.Drawing.Point(533, 84);
            this.txtError.Name = "txtError";
            this.txtError.Size = new System.Drawing.Size(174, 20);
            this.txtError.TabIndex = 139;
            this.txtError.Text = "0";
            this.txtError.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtIdentificados
            // 
            this.txtIdentificados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtIdentificados.Location = new System.Drawing.Point(533, 61);
            this.txtIdentificados.Name = "txtIdentificados";
            this.txtIdentificados.Size = new System.Drawing.Size(173, 20);
            this.txtIdentificados.TabIndex = 138;
            this.txtIdentificados.Text = "0";
            this.txtIdentificados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtProcesados
            // 
            this.txtProcesados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtProcesados.Location = new System.Drawing.Point(533, 39);
            this.txtProcesados.Name = "txtProcesados";
            this.txtProcesados.Size = new System.Drawing.Size(173, 20);
            this.txtProcesados.TabIndex = 137;
            this.txtProcesados.Text = "0";
            this.txtProcesados.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTiempoTotal
            // 
            this.txtTiempoTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTiempoTotal.Location = new System.Drawing.Point(201, 84);
            this.txtTiempoTotal.Name = "txtTiempoTotal";
            this.txtTiempoTotal.Size = new System.Drawing.Size(174, 20);
            this.txtTiempoTotal.TabIndex = 136;
            this.txtTiempoTotal.Text = "0";
            this.txtTiempoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTiempoProm
            // 
            this.txtTiempoProm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTiempoProm.Location = new System.Drawing.Point(201, 62);
            this.txtTiempoProm.Name = "txtTiempoProm";
            this.txtTiempoProm.Size = new System.Drawing.Size(174, 20);
            this.txtTiempoProm.TabIndex = 135;
            this.txtTiempoProm.Text = "0";
            this.txtTiempoProm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtHoraFin
            // 
            this.txtHoraFin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHoraFin.Location = new System.Drawing.Point(201, 40);
            this.txtHoraFin.Name = "txtHoraFin";
            this.txtHoraFin.Size = new System.Drawing.Size(174, 20);
            this.txtHoraFin.TabIndex = 134;
            this.txtHoraFin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtHoraInicio
            // 
            this.txtHoraInicio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtHoraInicio.Location = new System.Drawing.Point(202, 17);
            this.txtHoraInicio.Name = "txtHoraInicio";
            this.txtHoraInicio.Size = new System.Drawing.Size(173, 20);
            this.txtHoraInicio.TabIndex = 133;
            this.txtHoraInicio.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(4, 86);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(85, 13);
            this.label14.TabIndex = 132;
            this.label14.Text = "Tiempo Total:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(4, 64);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(177, 13);
            this.label13.TabIndex = 131;
            this.label13.Text = "Tiempo Promedio por Archivo:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(4, 42);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(149, 13);
            this.label12.TabIndex = 130;
            this.label12.Text = "Proceso Finalizado a las:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(4, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(137, 13);
            this.label6.TabIndex = 129;
            this.label6.Text = "Proceso Iniciado a las:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.numMaxLote);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.NumMaxKB);
            this.groupBox4.Controls.Add(this.chkRotar);
            this.groupBox4.Controls.Add(this.chkIniciarConCampos);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.lblOrigen);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.lblTamaño);
            this.groupBox4.Location = new System.Drawing.Point(6, 19);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(357, 216);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Archivo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(211, 13);
            this.label5.TabIndex = 110;
            this.label5.Text = "Tamaño del Lote de Procesamiento:";
            // 
            // numMaxLote
            // 
            this.numMaxLote.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numMaxLote.Location = new System.Drawing.Point(250, 105);
            this.numMaxLote.Maximum = new decimal(new int[] {
            5120,
            0,
            0,
            0});
            this.numMaxLote.Name = "numMaxLote";
            this.numMaxLote.Size = new System.Drawing.Size(75, 20);
            this.numMaxLote.TabIndex = 109;
            this.numMaxLote.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numMaxLote.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 85);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(162, 13);
            this.label4.TabIndex = 108;
            this.label4.Text = "Tamaño Máximo a procesar";
            // 
            // NumMaxKB
            // 
            this.NumMaxKB.Increment = new decimal(new int[] {
            128,
            0,
            0,
            0});
            this.NumMaxKB.Location = new System.Drawing.Point(250, 80);
            this.NumMaxKB.Maximum = new decimal(new int[] {
            5120,
            0,
            0,
            0});
            this.NumMaxKB.Name = "NumMaxKB";
            this.NumMaxKB.Size = new System.Drawing.Size(75, 20);
            this.NumMaxKB.TabIndex = 107;
            this.NumMaxKB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.NumMaxKB.Value = new decimal(new int[] {
            512,
            0,
            0,
            0});
            // 
            // chkRotar
            // 
            this.chkRotar.AutoSize = true;
            this.chkRotar.Checked = true;
            this.chkRotar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkRotar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRotar.Location = new System.Drawing.Point(10, 170);
            this.chkRotar.Name = "chkRotar";
            this.chkRotar.Size = new System.Drawing.Size(115, 17);
            this.chkRotar.TabIndex = 106;
            this.chkRotar.Text = "Rotar Imagenes";
            this.chkRotar.UseVisualStyleBackColor = true;
            // 
            // chkIniciarConCampos
            // 
            this.chkIniciarConCampos.AutoSize = true;
            this.chkIniciarConCampos.Checked = true;
            this.chkIniciarConCampos.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkIniciarConCampos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkIniciarConCampos.Location = new System.Drawing.Point(10, 195);
            this.chkIniciarConCampos.Name = "chkIniciarConCampos";
            this.chkIniciarConCampos.Size = new System.Drawing.Size(325, 17);
            this.chkIniciarConCampos.TabIndex = 105;
            this.chkIniciarConCampos.Text = "Intentar Identificación Automática por Campos No ID";
            this.chkIniciarConCampos.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(48, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "Origen:";
            // 
            // lblOrigen
            // 
            this.lblOrigen.ForeColor = System.Drawing.Color.Red;
            this.lblOrigen.Location = new System.Drawing.Point(8, 29);
            this.lblOrigen.Name = "lblOrigen";
            this.lblOrigen.Size = new System.Drawing.Size(336, 35);
            this.lblOrigen.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 67);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Tamaño:";
            // 
            // lblTamaño
            // 
            this.lblTamaño.AutoSize = true;
            this.lblTamaño.Location = new System.Drawing.Point(68, 67);
            this.lblTamaño.Name = "lblTamaño";
            this.lblTamaño.Size = new System.Drawing.Size(0, 13);
            this.lblTamaño.TabIndex = 1;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(375, 170);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(334, 23);
            this.progressBar1.TabIndex = 18;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.pictureBox1);
            this.groupBox3.Location = new System.Drawing.Point(367, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(357, 152);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(176, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(171, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "G E S T I O N   D I G I T A L";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(160, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(184, 76);
            this.label1.TabIndex = 6;
            this.label1.Text = "OCR";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = global::GestiónDigital.Properties.Resources.Black_Apple_System_Icon_16;
            this.pictureBox1.Location = new System.Drawing.Point(3, 16);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(153, 133);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // T1
            // 
            this.T1.Interval = 1000;
            this.T1.Tick += new System.EventHandler(this.T1_Tick);
            // 
            // T2
            // 
            this.T2.Interval = 60000;
            this.T2.Tick += new System.EventHandler(this.T2_Tick);
            // 
            // ProcesadorOCR_V2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 426);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ProcesadorOCR_V2";
            this.Text = "Procesador OCR";
            this.Load += new System.EventHandler(this.rocesadorOCR_V2_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ProcesadorOCR_V2_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxLote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumMaxKB)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblOrigen;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblTamaño;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.CheckBox chkIniciarConCampos;
        private System.Windows.Forms.Timer T1;
        private System.Windows.Forms.Timer T2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox chkRotar;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown NumMaxKB;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numMaxLote;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtAProcesar;
        private System.Windows.Forms.Button cmdProcesar;
        private System.Windows.Forms.Button cmdCancelar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEstado;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtError;
        private System.Windows.Forms.TextBox txtIdentificados;
        private System.Windows.Forms.TextBox txtProcesados;
        private System.Windows.Forms.TextBox txtTiempoTotal;
        private System.Windows.Forms.TextBox txtTiempoProm;
        private System.Windows.Forms.TextBox txtHoraFin;
        private System.Windows.Forms.TextBox txtHoraInicio;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
    }
}