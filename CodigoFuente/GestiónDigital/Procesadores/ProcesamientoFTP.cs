﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using CoreEngine;
using GestionDigitalCore.CoreObjects;
using GestionDigitalCore.CoreBussines;
using FSO.NH.WEB;

namespace GestiónDigital.Procesadores
{
    public partial class ProcesamientoFTP : Form
    {
        private int ArchivosProcesados = 0;
        private Bitmap bmp; 
        public ConfiguracionFTP cfgftp;
        public LogFTP MyLog;
        public List<string> filelist;
        List<LogDetalleFTP> MisLogs = new List<LogDetalleFTP>();
        public ProcesamientoFTP()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            UbicarPantalla();
            AdministrarTransparencia();
            GestionarAnimacion();
            ProcesadorFTP ftpp = new ProcesadorFTP();
            ftpp.ProcesoIniciado += new EventoBase(ftpp_ProcesoIniciado);
            ftpp.ProcesoFinalizado += new EventoBase(ftpp_ProcesoFinalizado);
            ftpp.ProcesandoArchivo += new _EventoDatosArchivoEnProceso(ftpp_ProcesandoArchivo);
            ftpp.PreArchivoProcesadoOk += new _EventoFinProcesoArchivoOk(ftpp_PreArchivoProcesadoOk);
            ftpp.PreArchivoProcesadoError += new _EventoFinProcesoArchivoError(ftpp_PreArchivoProcesadoError);
            ftpp.ArchivoProcesadoOk += new _EventoFinProcesoArchivoOk(ftpp_ArchivoProcesadoOk);
            ftpp.ArchivoProcesadoError += new _EventoFinProcesoArchivoError(ftpp_ArchivoProcesadoError);
            ftpp.WaitingSending += new _WaitingSending(ftpp_WaitingSending);
            ftpp.WaitPeriodoFinished += new _WaitPeriodoFinished(ftpp_WaitPeriodoFinished);
            ftpp.Working += new EventoBase(ftpp_Working);
            AtenderEventosDeWindows();
            ftpp.ProcesarArchivos(cfgftp, filelist);
            
        }

        void ftpp_WaitPeriodoFinished(string Archivo)
        {
            txtActualFile.Text = Archivo;
            AtenderEventosDeWindows();
        }

        void ftpp_WaitingSending(int waittime)
        {
            txtActualFile.Text = "Proceso Auto pausado por: " + waittime.ToString() + " Segundos";
            AtenderEventosDeWindows();
        }

        private void GestionarAnimacion()
        {
            bmp = new Bitmap(global::GestiónDigital.Properties.Resources.file_transfer_between_computers_md_wht);
            ImageAnimator.Animate(
            bmp,
            new EventHandler(this.OnFrameChanged));
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint); 
        }
        private void OnFrameChanged(object o, EventArgs e)
        {
            this.Invalidate();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            ImageAnimator.UpdateFrames();
            e.Graphics.DrawImage(this.bmp, new Point(0, 0));
        } 
        void ftpp_Working()
        {
            AtenderEventosDeWindows();
        }

        private void AdministrarTransparencia()
        {
            T1.Enabled = true;
        }

        private void UbicarPantalla()
        {
            this.Top = Screen.PrimaryScreen.WorkingArea.Height - this.Height;
            this.Left = Screen.PrimaryScreen.WorkingArea.Width - this.Width;
           
        }
        private void AtenderEventosDeWindows()
        { 
            for(int i=0;i<50;i++)
                Application.DoEvents();
        }
        void ftpp_ArchivoProcesadoError(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal, string Error)
        {
           
            RegistrarLog(FileName, ByteSize, TiempoTotal, Error);
            AtenderEventosDeWindows();
            
        }

        void ftpp_ArchivoProcesadoOk(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal)
        {
            bytesEnviados += ByteSize;
            ArchivosEnviados++;
            if (bytesEnviados > 1024)
            {
                if (bytesEnviados / 1024 > 1024)
                {
                    txtBytesSend.Text = (bytesEnviados / (1024 * 1024)) + " MBytes";
                }
                else
                {
                    txtBytesSend.Text = (bytesEnviados / 1024) + " KBytes";
                }
            }
            else
            {
                txtBytesSend.Text = bytesEnviados.ToString() + " Bytes";
            }
            long velocidadenbytes = (bytesEnviados / TiempoTotal.Seconds);
            lblVelocidad.Text = GetByteSize(velocidadenbytes) + "/Seg.";           
            txtFilesSends.Text = ArchivosEnviados.ToString();
            progressBar1.Value = Convert.ToInt32(PorcentajeAvance * 100);

            RegistrarLog(FileName, ByteSize, TiempoTotal, "");
            AtenderEventosDeWindows();
            
        }
        #region Log Region
        private void CerrarLogFTP()
        {
            BBLogFTP BBLog = new BBLogFTP();
            MyLog.FechaFin = DateTime.Now;
            MyLog.ArchivosProcesados = ArchivosProcesados;
            MyLog = BBLog.Guardar(MyLog);
            if (MyLog.Errores > 0)
            {
                // Ver Error de Autenticación.
                ClienteSMTP mailer = new ClienteSMTP();                
                mailer.MisParametros = cfgftp.SMTPConfig;
                mailer.MisParametros.UseSSL = true;
                mailer.MisParametros.SMTPPort = "587";
           
                mailer.MensajeMail = "Gestión Digital\n\rError en Envio por FTP. Revisar Log Nro: " + MyLog.ID + "\n\rErrores: " + MyLog.Errores.ToString() + "\n\r";
                foreach(LogDetalleFTP det in MisLogs)
                {
                    mailer.MensajeMail += det.FileName + ": " + det.Error + "\n\r";
                }

                mailer.SendAsHTML = true;
                mailer.Subject = "Gestión Digital: Error en Envio por FTP";
                mailer.To = cfgftp.EmailAlerta;
                mailer.From = cfgftp.SMTPConfig.SMTPUser;
                mailer.FromAlias = "Gestión Digital: Alertas";
                

                mailer.SendMail();
            }
        }
        private void AbrirLogFTP()
        {
            MyLog = new LogFTP();
            MyLog.IdConfiguracionFTP = cfgftp.ID;
            MyLog.FechaInicio = DateTime.Now;
            MyLog.FechaFin = DateTime.Now;
            MyLog.ArchivosProcesados = 0;
            BBLogFTP BBLog = new BBLogFTP();
            MyLog = BBLog.Guardar(MyLog);
        }
        LogDetalleFTP Filelog;
        private void RegistrarLog(string FileName, long ByteSize, TimeSpan TiempoTotal, string Error)
        {
            BBLogDetalleFTP BB = new BBLogDetalleFTP();           
            Filelog.FechaFin = DateTime.Now;
            Filelog.FileName = FileName;
            Filelog.IdLogFTP = MyLog.ID;
            Filelog.TiempoSubida = Convert.ToUInt32(TiempoTotal.Ticks);
            Filelog.Error = Error;
            Filelog.ByteSize = ByteSize;
            if (Error != "")
            {
                MyLog.Errores++;
            }
            BB.Guardar(Filelog);
            MisLogs.Add(Filelog);
        }
        private void InicializarFileLog()
        {            
            Filelog = new LogDetalleFTP();
            Filelog.FechaInicio = DateTime.Now;
        }
        #endregion
        void ftpp_PreArchivoProcesadoError(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal, string Error)
        {
            AtenderEventosDeWindows();
            
        }
        
        void ftpp_PreArchivoProcesadoOk(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal)
        {
            bytesEnviados += ByteSize;            
            ArchivosEnviados++;
           txtBytesSend.Text = GetByteSize(bytesEnviados);
            
            
            txtFilesSends.Text = ArchivosEnviados.ToString();
            progressBar1.Value = Convert.ToInt32(PorcentajeAvance * 100);

            AtenderEventosDeWindows();
            
        }
        long bytesEnviados=0;
        long ArchivosEnviados = 0;
        void ftpp_ProcesandoArchivo(string FileName, long ByteSize, decimal PorcentajeAvance, ref bool Cancelar)
        {
            InicializarFileLog();
            ArchivosProcesados++;
            txtActualFile.Text = FileName + ": " + GetByteSize(ByteSize);
            AtenderEventosDeWindows();
            
        }

        void ftpp_ProcesoFinalizado()
        {
            AtenderEventosDeWindows();
            Application.ExitThread();
            CerrarLogFTP();
        }


        void ftpp_ProcesoIniciado()
        {
            AbrirLogFTP();
            AtenderEventosDeWindows();
            
        }



        private void NI_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.Opacity = 1;
            this.WindowState = FormWindowState.Normal;
            AtenderEventosDeWindows();
            AdministrarTransparencia();
        }

        private void cancelarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AtenderEventosDeWindows();
            Application.ExitThread();
        }

        private string GetByteSize(long Bytes)
        {
            string r = "";
            if (Bytes > 1024)
            {
                if (Bytes / 1024 > 1024)
                {
                    r = (Bytes / (1024 * 1024)) + " MBytes";
                }
                else
                {
                    r = (Bytes / 1024) + " KBytes";
                }
            }
            else
            {
                r = Bytes.ToString() + " Bytes";
            }
            return r;
        }


        private void T1_Tick(object sender, EventArgs e)
        {
            AtenderEventosDeWindows();
            if (this.Opacity > 0.2)
            {
                this.Opacity = this.Opacity - 0.01;
            }
            else
            {
                this.Opacity = 0;
                this.WindowState = FormWindowState.Minimized;
                T1.Enabled = false;
            }
        }

        private void ProcesamientoFTP_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            AdministrarTransparencia();
        }

        private void ProcesamientoFTP_Activated(object sender, EventArgs e)
        {
            this.Opacity = 1;
            this.WindowState = FormWindowState.Normal;
            T1.Enabled = false;
            AtenderEventosDeWindows();
        }


    }
}