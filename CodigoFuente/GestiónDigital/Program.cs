﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using GestionDigitalCore.CoreObjects;
using GestionDigitalCore.CoreBussines;
using GestiónDigital.Procesadores;
using ToolBox;
using ZoomCar.SecurityCode;
using FSO.NH.CodigoDeSeguridad;

namespace GestiónDigital
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                FSO.NH.CodigoDeSeguridad.ValidadorCodigoSeguridad validator = new FSO.NH.CodigoDeSeguridad.ValidadorCodigoSeguridad("78945389745324896451579843216765132");

                validator = new ValidadorCodigoSeguridad("xLFWinGD");
                validator.RegisterExecutionTime();
                if (validator.PuedoEjecutar())
                {
                    GestionarInicio();
                }
                else
                {
                    Application.Run(new frmActivacion());
                    //Vuelvo a preguntar por si lo active.
                    if (validator.PuedoEjecutar())
                    {
                        GestionarInicio();
                    }
                } 
            }
            catch (Exception ex)
            {
                string MasDatos = "";
                if (ex.InnerException != null)
                {
                    MasDatos = ": " + ex.InnerException.Message;
                    if (ex.InnerException.InnerException != null)
                    {
                        MasDatos = ": " + ex.InnerException.InnerException.Message;

                    }
                }
                MessageBox.Show("ERROR: " + ex.Message + MasDatos);
                Application.Exit();
            }
			
        }

        private static void GestionarInicio()
        {
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length == 1)
                NormalStart();
            else
                ParametrizedStart(args);
        }

        private static void ParametrizedStart(string[] args)
        {
            try
            {
                bool procesaFTP = false;
                bool procesaOCR = false;
                List<string> filelistFTP = new List<string>();
                List<string> filelistOCR = new List<string>();
                string ConfToLoad = "DEFAULT";
                ConfiguracionOCR cfgocr = new ConfiguracionOCR();
                ConfiguracionFTP cfgftp = new ConfiguracionFTP();
                foreach (string arg in args)
                {
                    string[] p = arg.Split(':');
                    if (p.Length == 2)
                    {
                        string command = p[0];
                        string Paramenter = p[1];
                        switch (command.ToUpper())
                        {
                            case "-FTP":
                                
                                BBConfiguracionFTP BBFTP = new BBConfiguracionFTP();
                                if (Paramenter.ToUpper() != "DEFAULT")
                                {
                                    ConfToLoad = Paramenter;
                                }
                                if (ConfToLoad == "DEFAULT")
                                {
                                    cfgftp = BBFTP.GetDefaultConfiguration();
                                }
                                else
                                {
                                    cfgftp = BBFTP.GetConfigurationByName(ConfToLoad);
                                }
                                filelistFTP = GetFileListFromConfig(cfgftp.DirectorioOrigen, "*.jpg");

                                procesaFTP = true;
                                break;

                            case "-OCR"://"C":
                               
                                BBConfiguracionOCR BBOCR = new BBConfiguracionOCR();
                                if (Paramenter.ToUpper() != "DEFAULT")
                                {
                                    ConfToLoad = Paramenter;
                                }
                                if (ConfToLoad == "DEFAULT")
                                {
                                    cfgocr = BBOCR.GetDefaultConfiguration();
                                }
                                else
                                {
                                    cfgocr = BBOCR.GetConfigurationByName(ConfToLoad);
                                }                                
                                procesaOCR = true;
                                break;
                        }
                    }
                }
                if (procesaFTP)
                {
                    ProcesamientoFTP f = new ProcesamientoFTP();
                    f.cfgftp = cfgftp;
                    f.filelist = filelistFTP;
                    Application.Run(f);
                }
                if (procesaOCR)
                {
                    ProcesadorOCR_V2 aplocr = new ProcesadorOCR_V2();
                    aplocr.cfg = cfgocr;
                    aplocr.AutoRun = true;                    
                    Application.Run(aplocr);
                }


            }
            catch (Exception ex)
            { MessageBox.Show("ERROR: " + ex.Message ); }
        }
        private static void NormalStart()
        {
          
            Application.Run(new frmSysTrayManager());
           //Application.Run(new frmTest());
        }
        private static List<string> GetFileListFromConfig(string DirectorioOrigen)
        {
            return FileTools.GetFilesRecursive(DirectorioOrigen);        
        }
        private static List<string> GetFileListFromConfig(string DirectorioOrigen, string filtro)
        {
            return FileTools.GetFilesRecursive(DirectorioOrigen,filtro);
        }
    }
}
