﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using TaskScheduler;
using GestionDigitalCore.CoreBussines;
using GestionDigitalCore.CoreObjects;
using System.Security.Principal;

namespace GestiónDigital.Scheduller
{
    public partial class frmScheduller : Form
    {
        ScheduledTasks st;
        public frmScheduller()
        {
 
            st = new ScheduledTasks();
            InitializeComponent();
            SetearCadenaEjecutable();
        }

        private void cmdGuardar_Click(object sender, EventArgs e)
        {

            try
            {
                if (txtPassword.Text == txtPassword2.Text)
                {

                    TaskScheduler.Task Tarea;

                    try
                    {
                        Tarea = st.CreateTask(txtNombre.Text);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("La Tarea ya Existe"); return;
                    }

                    Tarea.ApplicationName = txtTask.Text;
                    Tarea.Comment = txtComentario.Text;
                    if (txtUsuario.Text == "")
                    {
                        Tarea.SetAccountInformation(txtUsuario.Text, (string)null);
                    }
                    else if (txtUsuario.Text == Environment.UserName)
                    {
                        Tarea.Flags = TaskFlags.RunOnlyIfLoggedOn;
                        Tarea.SetAccountInformation(txtUsuario.Text, (string)null);
                    }
                    else
                    {
                        Tarea.SetAccountInformation(txtUsuario.Text, txtPassword.Text);
                    }

                    Tarea.IdleWaitDeadlineMinutes = 20;
                    Tarea.IdleWaitMinutes = 10;
                    Tarea.MaxRunTime = new TimeSpan(1, 0, 0);
                    Tarea.Priority = System.Diagnostics.ProcessPriorityClass.High;
                    Tarea.Triggers.Add(new DailyTrigger(22, 30, 1));
                    Tarea.Creator = "Gestion Digital";
                    Tarea.WorkingDirectory = Application.StartupPath;
                    Tarea.Save(txtNombre.Text);


                }
                else
                {
                    MessageBox.Show("Verifique el password ingresado");
                } 
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
            }
			

        }
        private void SetearCadenaEjecutable()
        {
            txtTask.Text = Application.StartupPath + "\\GestionDigital.exe";
            if (chkDefFTP.Checked || cboFTP.SelectedIndex > -1)
            {
                if (chkDefFTP.Checked)
                {
                    txtTask.Text += " -FTP:Default";
                }
                else
                {
                    txtTask.Text += " -FTP:" + cboFTP.SelectedItem.ToString();
                }
            }
            if (chkDefOCR.Checked || cboOCR.SelectedIndex > -1)
            {
                if (chkDefOCR.Checked)
                {
                    txtTask.Text += " -OCR:Default";
                }
                else
                {
                    txtTask.Text += " -OCR:" + cboOCR.SelectedItem.ToString();
                }
            }
        }

        private void frmScheduller_Load(object sender, EventArgs e)
        {
            BindearComboFTP(); BindearComboOCR(); BindearTareas();
        }

        private void BindearComboOCR()
        {
            List<ConfiguracionOCR> ListaOCR = new BBConfiguracionOCR().GetAll();
            foreach (ConfiguracionOCR c in ListaOCR)
            {
                cboOCR.Items.Add(c.Nombre);
            }
        }

        private void BindearComboFTP()
        {
            List<ConfiguracionFTP> ListaFTP = new BBConfiguracionFTP().GetAll();
            foreach (ConfiguracionFTP c in ListaFTP)
            {
                cboFTP.Items.Add(c.Nombre);
            }
        }
        private void BindearTareas()
        {
            string[] taskNames = st.GetTaskNames();
            lstTask.Items.Clear();
            foreach (String c in taskNames)
            {
                lstTask.Items.Add(c);
            }
        }

        private void chkDefFTP_CheckedChanged(object sender, EventArgs e)
        {
            cboFTP.Enabled = !chkDefFTP.Checked;
            SetearCadenaEjecutable();
        }

        private void chkDefOCR_CheckedChanged(object sender, EventArgs e)
        {
            cboOCR.Enabled = !chkDefOCR.Checked;
            SetearCadenaEjecutable();
        }

        private void cboFTP_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetearCadenaEjecutable();
        }

        private void cboOCR_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetearCadenaEjecutable();
        }


    }
}