﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GestiónDigital.Configuracion;
using GestiónDigital.Scheduller;
using ZoomCar.SecurityCode;
using System.IO;
using GestionDigitalCore.CoreObjects;
using GestionDigitalCore.CoreBussines;
using GestiónDigital.Procesadores;
using ToolBox;

namespace GestiónDigital
{
    public partial class frmSysTrayManager : Form
    {
        public frmSysTrayManager()
        {
            InitializeComponent();

        }

        private void frmSysTrayManager_Load(object sender, EventArgs e)
        {
            this.Top = Screen.PrimaryScreen.WorkingArea.Height - this.Height;
            this.Left = Screen.PrimaryScreen.WorkingArea.Width - this.Width;
            T1.Enabled = true;
            
        }

        private void T1_Tick(object sender, EventArgs e)
        {
            if (this.Opacity > 0.01)
            {
                this.Opacity = this.Opacity - 0.05;
            }
            else {
                T1.Enabled = false;
                this.WindowState = FormWindowState.Minimized;
                NI.Visible = true;
                TLimpiador.Enabled = true;
            }
        }

        private void salirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void configurarMóduloToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmConfiguración frmConfig = new frmConfiguración();
            frmConfig.Show();
        }

        private void procesarDocumentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //System.Diagnostics.Process.Start(Application.StartupPath + "\\GestionDigital.exe", "-OCR:Default");
            BBConfiguracionOCR BBOCR = new BBConfiguracionOCR();
            List<string> filelist;
            ConfiguracionOCR cfgocr=new ConfiguracionOCR();

            cfgocr = BBOCR.GetDefaultConfiguration(); 
            filelist = GetFileListFromConfig(cfgocr.DirectorioOrigen, cfgocr.TipoArchivo);
            ProcesadorOCR_V2 aplocr = new ProcesadorOCR_V2();
            aplocr.AutoRun = false;
            aplocr.cfg = cfgocr;
            aplocr.Show();
        }

        private static List<string> GetFileListFromConfig(string DirectorioOrigen, string filtro)
        {
            return FileTools.GetFilesRecursive(DirectorioOrigen, filtro);
        }

        private void enviarAFTPToolStripMenuItem_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(Application.StartupPath+ "\\GestionDigital.exe", "-FTP:Default");
                                                                         
        }

        private void programarTrabajosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmScheduller frmConfig = new frmScheduller();
            frmConfig.Show();
        }

        private void licenciaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmActivacion f = new frmActivacion();
            f.Show();
        }

        private void TLimpiador_Tick(object sender, EventArgs e)
        {
            // Force garbage collection so image file is closed properly

            string[] Listado = Directory.GetFiles(Application.StartupPath, "*.tif");
            foreach (string file in Listado)
            {
                try
                {
                    TimeSpan t = DateTime.Now.Subtract(File.GetCreationTime(file));
                    if (t.Minutes > 120)
                    {
                        File.Delete(file);
                    }
                }catch(Exception){}
            }
            List<string> Listado2 = ToolBox.FileTools.GetFilesRecursive(Application.StartupPath+"\\DocImg", "*.tif");
            BBDocumento BBD  = new BBDocumento();
            List<Documento> Docs= BBD.GetAll();

            foreach (string file in Listado2)
            {
                try
                {
                    TimeSpan t = DateTime.Now.Subtract(File.GetCreationTime(file));
                    if (t.Minutes > 120 && !EsUsado(file, Docs))
                    {
                        File.Delete(file);
                    }
                }
                catch (Exception) { }
            }
            LimpiarBase();
        }
        private void LimpiarBase()
        {
            BBLogFTP BBL = new BBLogFTP();
            BBL.DeleteOldValues();
            BBArchivo BBA = new BBArchivo();
            BBA.DeleteOldValues();
        }
        private bool EsUsado(string file, List<Documento> Docs)
        {
            bool usado = false;
            foreach (Documento d in Docs)
            {
                if (file == d.PathImage)
                    return true;
            }
            return usado;
        }

        private void TRamCleaner_Tick(object sender, EventArgs e)
        {
            GC.Collect();
            GC.WaitForPendingFinalizers();
        }
    }
}