﻿namespace GestiónDigital.Configuracion
{
    partial class frmDOCConfigAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDOCConfigAdmin));
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlDocument = new System.Windows.Forms.Panel();
            this.shpArea = new ShapeControl.ShapeControl();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label8 = new System.Windows.Forms.Label();
            this.lMostrarRegion = new System.Windows.Forms.LinkLabel();
            this.llAutoSize = new System.Windows.Forms.LinkLabel();
            this.llZomeIn = new System.Windows.Forms.LinkLabel();
            this.llOriginal = new System.Windows.Forms.LinkLabel();
            this.llZoomOut = new System.Windows.Forms.LinkLabel();
            this.cmdEliminar = new System.Windows.Forms.Button();
            this.cmdNuevoCampo = new System.Windows.Forms.Button();
            this.pnlCampo = new System.Windows.Forms.Panel();
            this.cboTipoDato = new System.Windows.Forms.ComboBox();
            this.txtLongitud = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.cboTipoProceso = new System.Windows.Forms.ComboBox();
            this.chkNombre = new System.Windows.Forms.CheckBox();
            this.cboGradoRotacion = new System.Windows.Forms.ComboBox();
            this.listViewBar = new System.Windows.Forms.ListView();
            this.cmdLeerDatos = new System.Windows.Forms.Button();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.chkMostrarArea = new System.Windows.Forms.CheckBox();
            this.cmdOkCampo = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.cmdTipoCampo = new System.Windows.Forms.ComboBox();
            this.txtValorIdentificacion = new System.Windows.Forms.TextBox();
            this.chkID = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtNombreCampo = new System.Windows.Forms.TextBox();
            this.txtZoneWidth = new System.Windows.Forms.TextBox();
            this.txtZoneX = new System.Windows.Forms.TextBox();
            this.txtZoneY = new System.Windows.Forms.TextBox();
            this.txtZoneHeight = new System.Windows.Forms.TextBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblOrden = new System.Windows.Forms.Label();
            this.chkActivo = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dgCampos = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmdCargarImagen = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.ocr1 = new OCRTools.OCR(this.components);
            this.cmdEliminarDoc = new System.Windows.Forms.PictureBox();
            this.cmdGuardarFTP = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.pnlDocument.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.pnlCampo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgCampos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEliminarDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGuardarFTP)).BeginInit();
            this.SuspendLayout();
            // 
            // txtNombre
            // 
            this.txtNombre.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNombre.Location = new System.Drawing.Point(6, 32);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(428, 20);
            this.txtNombre.TabIndex = 75;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 8);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 13);
            this.label14.TabIndex = 76;
            this.label14.Text = "Nombre:";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.pnlDocument);
            this.panel1.Controls.Add(this.splitContainer1);
            this.panel1.Location = new System.Drawing.Point(2, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1168, 671);
            this.panel1.TabIndex = 77;
            // 
            // pnlDocument
            // 
            this.pnlDocument.AutoScroll = true;
            this.pnlDocument.Controls.Add(this.shpArea);
            this.pnlDocument.Controls.Add(this.PictureBox1);
            this.pnlDocument.Location = new System.Drawing.Point(483, 3);
            this.pnlDocument.Name = "pnlDocument";
            this.pnlDocument.Size = new System.Drawing.Size(680, 675);
            this.pnlDocument.TabIndex = 82;
            // 
            // shpArea
            // 
            this.shpArea.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(164)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.shpArea.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.shpArea.BorderStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            this.shpArea.BorderWidth = 3;
            this.shpArea.CenterColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(255)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.shpArea.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.shpArea.Location = new System.Drawing.Point(128, 9);
            this.shpArea.Name = "shpArea";
            this.shpArea.Shape = ShapeControl.ShapeType.Rectangle;
            this.shpArea.Size = new System.Drawing.Size(75, 23);
            this.shpArea.SurroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.shpArea.TabIndex = 6;
            this.shpArea.UseGradient = false;
            this.shpArea.Visible = false;
            // 
            // PictureBox1
            // 
            this.PictureBox1.Location = new System.Drawing.Point(0, 0);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(71, 58);
            this.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.PictureBox1.TabIndex = 3;
            this.PictureBox1.TabStop = false;
            this.PictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PictureBox1_MouseUp);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.label8);
            this.splitContainer1.Panel1.Controls.Add(this.lMostrarRegion);
            this.splitContainer1.Panel1.Controls.Add(this.llAutoSize);
            this.splitContainer1.Panel1.Controls.Add(this.llZomeIn);
            this.splitContainer1.Panel1.Controls.Add(this.llOriginal);
            this.splitContainer1.Panel1.Controls.Add(this.llZoomOut);
            this.splitContainer1.Panel1.Controls.Add(this.cmdEliminar);
            this.splitContainer1.Panel1.Controls.Add(this.cmdNuevoCampo);
            this.splitContainer1.Panel1.Controls.Add(this.pnlCampo);
            this.splitContainer1.Panel1.Controls.Add(this.lblOrden);
            this.splitContainer1.Panel1.Controls.Add(this.chkActivo);
            this.splitContainer1.Panel1.Controls.Add(this.label1);
            this.splitContainer1.Panel1.Controls.Add(this.dgCampos);
            this.splitContainer1.Panel1.Controls.Add(this.label14);
            this.splitContainer1.Panel1.Controls.Add(this.txtNombre);
            this.splitContainer1.Panel1.Controls.Add(this.cmdCargarImagen);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.Color.White;
            this.splitContainer1.Size = new System.Drawing.Size(477, 678);
            this.splitContainer1.SplitterDistance = 448;
            this.splitContainer1.TabIndex = 81;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 311);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(430, 13);
            this.label8.TabIndex = 100;
            this.label8.Text = "(*) El grado de rotación del doc. estará dado al reconocer el valor del  campo Id" +
                "entificador";
            // 
            // lMostrarRegion
            // 
            this.lMostrarRegion.Location = new System.Drawing.Point(76, 73);
            this.lMostrarRegion.Name = "lMostrarRegion";
            this.lMostrarRegion.Size = new System.Drawing.Size(116, 16);
            this.lMostrarRegion.TabIndex = 98;
            this.lMostrarRegion.TabStop = true;
            this.lMostrarRegion.Text = "Región Seleccionada";
            // 
            // llAutoSize
            // 
            this.llAutoSize.Location = new System.Drawing.Point(246, 73);
            this.llAutoSize.Name = "llAutoSize";
            this.llAutoSize.Size = new System.Drawing.Size(52, 16);
            this.llAutoSize.TabIndex = 95;
            this.llAutoSize.TabStop = true;
            this.llAutoSize.Text = "AutoSize";
            this.llAutoSize.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llAutoSize_LinkClicked);
            // 
            // llZomeIn
            // 
            this.llZomeIn.Location = new System.Drawing.Point(302, 73);
            this.llZomeIn.Name = "llZomeIn";
            this.llZomeIn.Size = new System.Drawing.Size(48, 16);
            this.llZomeIn.TabIndex = 96;
            this.llZomeIn.TabStop = true;
            this.llZomeIn.Text = "Zoom In";
            this.llZomeIn.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llZomeIn_LinkClicked);
            // 
            // llOriginal
            // 
            this.llOriginal.Location = new System.Drawing.Point(198, 73);
            this.llOriginal.Name = "llOriginal";
            this.llOriginal.Size = new System.Drawing.Size(44, 16);
            this.llOriginal.TabIndex = 94;
            this.llOriginal.TabStop = true;
            this.llOriginal.Text = "Original";
            this.llOriginal.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llOriginal_LinkClicked);
            // 
            // llZoomOut
            // 
            this.llZoomOut.Location = new System.Drawing.Point(354, 73);
            this.llZoomOut.Name = "llZoomOut";
            this.llZoomOut.Size = new System.Drawing.Size(60, 16);
            this.llZoomOut.TabIndex = 97;
            this.llZoomOut.TabStop = true;
            this.llZoomOut.Text = "Zoom Out";
            this.llZoomOut.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llZoomOut_LinkClicked);
            // 
            // cmdEliminar
            // 
            this.cmdEliminar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdEliminar.Location = new System.Drawing.Point(322, 335);
            this.cmdEliminar.Name = "cmdEliminar";
            this.cmdEliminar.Size = new System.Drawing.Size(119, 23);
            this.cmdEliminar.TabIndex = 93;
            this.cmdEliminar.Text = "Eliminar";
            this.cmdEliminar.UseVisualStyleBackColor = true;
            this.cmdEliminar.Click += new System.EventHandler(this.cmdEliminar_Click);
            // 
            // cmdNuevoCampo
            // 
            this.cmdNuevoCampo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdNuevoCampo.Location = new System.Drawing.Point(10, 335);
            this.cmdNuevoCampo.Name = "cmdNuevoCampo";
            this.cmdNuevoCampo.Size = new System.Drawing.Size(119, 23);
            this.cmdNuevoCampo.TabIndex = 92;
            this.cmdNuevoCampo.Text = "Nuevo Campo";
            this.cmdNuevoCampo.UseVisualStyleBackColor = true;
            this.cmdNuevoCampo.Click += new System.EventHandler(this.cmdNuevoCampo_Click);
            // 
            // pnlCampo
            // 
            this.pnlCampo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlCampo.Controls.Add(this.cboTipoDato);
            this.pnlCampo.Controls.Add(this.txtLongitud);
            this.pnlCampo.Controls.Add(this.label2);
            this.pnlCampo.Controls.Add(this.label9);
            this.pnlCampo.Controls.Add(this.cboTipoProceso);
            this.pnlCampo.Controls.Add(this.chkNombre);
            this.pnlCampo.Controls.Add(this.cboGradoRotacion);
            this.pnlCampo.Controls.Add(this.listViewBar);
            this.pnlCampo.Controls.Add(this.cmdLeerDatos);
            this.pnlCampo.Controls.Add(this.numericUpDown1);
            this.pnlCampo.Controls.Add(this.label7);
            this.pnlCampo.Controls.Add(this.chkMostrarArea);
            this.pnlCampo.Controls.Add(this.cmdOkCampo);
            this.pnlCampo.Controls.Add(this.label5);
            this.pnlCampo.Controls.Add(this.cmdTipoCampo);
            this.pnlCampo.Controls.Add(this.txtValorIdentificacion);
            this.pnlCampo.Controls.Add(this.chkID);
            this.pnlCampo.Controls.Add(this.label4);
            this.pnlCampo.Controls.Add(this.txtNombreCampo);
            this.pnlCampo.Controls.Add(this.txtZoneWidth);
            this.pnlCampo.Controls.Add(this.txtZoneX);
            this.pnlCampo.Controls.Add(this.txtZoneY);
            this.pnlCampo.Controls.Add(this.txtZoneHeight);
            this.pnlCampo.Controls.Add(this.Label6);
            this.pnlCampo.Controls.Add(this.label3);
            this.pnlCampo.Location = new System.Drawing.Point(10, 364);
            this.pnlCampo.Name = "pnlCampo";
            this.pnlCampo.Size = new System.Drawing.Size(429, 301);
            this.pnlCampo.TabIndex = 91;
            this.pnlCampo.Visible = false;
            // 
            // cboTipoDato
            // 
            this.cboTipoDato.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoDato.FormattingEnabled = true;
            this.cboTipoDato.Items.AddRange(new object[] {
            "Omitir",
            "Numérico"});
            this.cboTipoDato.Location = new System.Drawing.Point(184, 132);
            this.cboTipoDato.Name = "cboTipoDato";
            this.cboTipoDato.Size = new System.Drawing.Size(98, 21);
            this.cboTipoDato.TabIndex = 108;
            // 
            // txtLongitud
            // 
            this.txtLongitud.Location = new System.Drawing.Point(136, 132);
            this.txtLongitud.Name = "txtLongitud";
            this.txtLongitud.Size = new System.Drawing.Size(44, 20);
            this.txtLongitud.TabIndex = 107;
            this.txtLongitud.Text = "0";
            this.txtLongitud.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtLongitud.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLongitud_KeyPress);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 132);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 16);
            this.label2.TabIndex = 106;
            this.label2.Text = "Validar Longitud/Tipo";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(6, 212);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 13);
            this.label9.TabIndex = 105;
            this.label9.Text = "Preproceso:";
            // 
            // cboTipoProceso
            // 
            this.cboTipoProceso.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTipoProceso.FormattingEnabled = true;
            this.cboTipoProceso.Items.AddRange(new object[] {
            "Ninguno",
            "Difuminación para Texto Normal",
            "Mejoramiento para códigos de barra claros",
            "Mejoramiento para códigos de barra oscuros",
            "Difuminación para Texto Oscuro",
            "Barras A5Texto Normal",
            "Texto A5Texto Normal"});
            this.cboTipoProceso.Location = new System.Drawing.Point(87, 209);
            this.cboTipoProceso.Name = "cboTipoProceso";
            this.cboTipoProceso.Size = new System.Drawing.Size(268, 21);
            this.cboTipoProceso.TabIndex = 104;
            // 
            // chkNombre
            // 
            this.chkNombre.AutoSize = true;
            this.chkNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkNombre.Location = new System.Drawing.Point(271, 183);
            this.chkNombre.Name = "chkNombre";
            this.chkNombre.Size = new System.Drawing.Size(137, 17);
            this.chkNombre.TabIndex = 103;
            this.chkNombre.Text = "Nombre Documento";
            this.chkNombre.UseVisualStyleBackColor = true;
            // 
            // cboGradoRotacion
            // 
            this.cboGradoRotacion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboGradoRotacion.FormattingEnabled = true;
            this.cboGradoRotacion.Items.AddRange(new object[] {
            "0°",
            "90°",
            "180°",
            "270°"});
            this.cboGradoRotacion.Location = new System.Drawing.Point(234, 108);
            this.cboGradoRotacion.Name = "cboGradoRotacion";
            this.cboGradoRotacion.Size = new System.Drawing.Size(48, 21);
            this.cboGradoRotacion.TabIndex = 102;
            this.cboGradoRotacion.SelectedIndexChanged += new System.EventHandler(this.cboGradoRotacion_SelectedIndexChanged);
            // 
            // listViewBar
            // 
            this.listViewBar.Location = new System.Drawing.Point(8, 235);
            this.listViewBar.Name = "listViewBar";
            this.listViewBar.Size = new System.Drawing.Size(414, 58);
            this.listViewBar.TabIndex = 101;
            this.listViewBar.UseCompatibleStateImageBehavior = false;
            // 
            // cmdLeerDatos
            // 
            this.cmdLeerDatos.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdLeerDatos.Location = new System.Drawing.Point(288, 109);
            this.cmdLeerDatos.Name = "cmdLeerDatos";
            this.cmdLeerDatos.Size = new System.Drawing.Size(115, 21);
            this.cmdLeerDatos.TabIndex = 100;
            this.cmdLeerDatos.Text = "Leer Datos";
            this.cmdLeerDatos.UseVisualStyleBackColor = true;
            this.cmdLeerDatos.Click += new System.EventHandler(this.cmdLeerDatos_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(87, 182);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(102, 20);
            this.numericUpDown1.TabIndex = 98;
            this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(6, 187);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(45, 13);
            this.label7.TabIndex = 97;
            this.label7.Text = "Orden:";
            // 
            // chkMostrarArea
            // 
            this.chkMostrarArea.AutoSize = true;
            this.chkMostrarArea.Location = new System.Drawing.Point(288, 90);
            this.chkMostrarArea.Name = "chkMostrarArea";
            this.chkMostrarArea.Size = new System.Drawing.Size(86, 17);
            this.chkMostrarArea.TabIndex = 96;
            this.chkMostrarArea.Text = "Mostrar Area";
            this.chkMostrarArea.UseVisualStyleBackColor = true;
            this.chkMostrarArea.CheckedChanged += new System.EventHandler(this.chkMostrarArea_CheckedChanged);
            // 
            // cmdOkCampo
            // 
            this.cmdOkCampo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOkCampo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdOkCampo.Location = new System.Drawing.Point(361, 206);
            this.cmdOkCampo.Name = "cmdOkCampo";
            this.cmdOkCampo.Size = new System.Drawing.Size(61, 23);
            this.cmdOkCampo.TabIndex = 95;
            this.cmdOkCampo.Text = "Ok";
            this.cmdOkCampo.UseVisualStyleBackColor = true;
            this.cmdOkCampo.Click += new System.EventHandler(this.cmdOkCampo_Click);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 16);
            this.label5.TabIndex = 94;
            this.label5.Text = "Tipo de Campo";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmdTipoCampo
            // 
            this.cmdTipoCampo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdTipoCampo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmdTipoCampo.FormattingEnabled = true;
            this.cmdTipoCampo.Items.AddRange(new object[] {
            "Texto",
            "Barras"});
            this.cmdTipoCampo.Location = new System.Drawing.Point(137, 54);
            this.cmdTipoCampo.Name = "cmdTipoCampo";
            this.cmdTipoCampo.Size = new System.Drawing.Size(259, 21);
            this.cmdTipoCampo.TabIndex = 93;
            // 
            // txtValorIdentificacion
            // 
            this.txtValorIdentificacion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtValorIdentificacion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtValorIdentificacion.Location = new System.Drawing.Point(87, 156);
            this.txtValorIdentificacion.Name = "txtValorIdentificacion";
            this.txtValorIdentificacion.Size = new System.Drawing.Size(337, 20);
            this.txtValorIdentificacion.TabIndex = 92;
            // 
            // chkID
            // 
            this.chkID.AutoSize = true;
            this.chkID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkID.Location = new System.Drawing.Point(9, 159);
            this.chkID.Name = "chkID";
            this.chkID.Size = new System.Drawing.Size(59, 17);
            this.chkID.TabIndex = 91;
            this.chkID.Text = "Es Id.";
            this.chkID.UseVisualStyleBackColor = true;
            this.chkID.CheckedChanged += new System.EventHandler(this.chkID_CheckedChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(5, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 13);
            this.label4.TabIndex = 90;
            this.label4.Text = "Nombre:";
            // 
            // txtNombreCampo
            // 
            this.txtNombreCampo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNombreCampo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNombreCampo.Location = new System.Drawing.Point(8, 30);
            this.txtNombreCampo.Name = "txtNombreCampo";
            this.txtNombreCampo.Size = new System.Drawing.Size(414, 20);
            this.txtNombreCampo.TabIndex = 89;
            // 
            // txtZoneWidth
            // 
            this.txtZoneWidth.Location = new System.Drawing.Point(136, 109);
            this.txtZoneWidth.Name = "txtZoneWidth";
            this.txtZoneWidth.Size = new System.Drawing.Size(44, 20);
            this.txtZoneWidth.TabIndex = 85;
            this.txtZoneWidth.Text = "0";
            this.txtZoneWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZoneWidth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtZoneWidth_KeyPress);
            // 
            // txtZoneX
            // 
            this.txtZoneX.Location = new System.Drawing.Point(136, 87);
            this.txtZoneX.Name = "txtZoneX";
            this.txtZoneX.Size = new System.Drawing.Size(44, 20);
            this.txtZoneX.TabIndex = 83;
            this.txtZoneX.Text = "0";
            this.txtZoneX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZoneX.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtZoneX_KeyPress);
            // 
            // txtZoneY
            // 
            this.txtZoneY.Location = new System.Drawing.Point(184, 87);
            this.txtZoneY.Name = "txtZoneY";
            this.txtZoneY.Size = new System.Drawing.Size(44, 20);
            this.txtZoneY.TabIndex = 84;
            this.txtZoneY.Text = "0";
            this.txtZoneY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZoneY.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtZoneY_KeyPress);
            // 
            // txtZoneHeight
            // 
            this.txtZoneHeight.Location = new System.Drawing.Point(184, 109);
            this.txtZoneHeight.Name = "txtZoneHeight";
            this.txtZoneHeight.Size = new System.Drawing.Size(44, 20);
            this.txtZoneHeight.TabIndex = 86;
            this.txtZoneHeight.Text = "0";
            this.txtZoneHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtZoneHeight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtZoneHeight_KeyPress);
            // 
            // Label6
            // 
            this.Label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label6.Location = new System.Drawing.Point(5, 87);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(132, 16);
            this.Label6.TabIndex = 87;
            this.Label6.Text = "Esquina Sup. Izq (X,Y)";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(5, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 16);
            this.label3.TabIndex = 88;
            this.label3.Text = "Ancho/Alto/Rotación";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblOrden
            // 
            this.lblOrden.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblOrden.AutoSize = true;
            this.lblOrden.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOrden.Location = new System.Drawing.Point(386, 54);
            this.lblOrden.Name = "lblOrden";
            this.lblOrden.Size = new System.Drawing.Size(56, 13);
            this.lblOrden.TabIndex = 90;
            this.lblOrden.Text = "Orden: 0";
            this.lblOrden.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // chkActivo
            // 
            this.chkActivo.AutoSize = true;
            this.chkActivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkActivo.Location = new System.Drawing.Point(6, 54);
            this.chkActivo.Name = "chkActivo";
            this.chkActivo.Size = new System.Drawing.Size(62, 17);
            this.chkActivo.TabIndex = 89;
            this.chkActivo.Text = "Activo";
            this.chkActivo.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 82;
            this.label1.Text = "Campos";
            // 
            // dgCampos
            // 
            this.dgCampos.AllowUserToAddRows = false;
            this.dgCampos.AllowUserToDeleteRows = false;
            this.dgCampos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgCampos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCampos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dgCampos.Location = new System.Drawing.Point(6, 92);
            this.dgCampos.Name = "dgCampos";
            this.dgCampos.RowHeadersVisible = false;
            this.dgCampos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCampos.Size = new System.Drawing.Size(435, 216);
            this.dgCampos.TabIndex = 81;
            this.dgCampos.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgCampos_CellMouseDoubleClick);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            this.Column1.Visible = false;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Nombre";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 150;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Clave";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 50;
            // 
            // Column4
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle1.Format = "N0";
            dataGridViewCellStyle1.NullValue = "0";
            this.Column4.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column4.HeaderText = "Orden";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // cmdCargarImagen
            // 
            this.cmdCargarImagen.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCargarImagen.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCargarImagen.Location = new System.Drawing.Point(162, 3);
            this.cmdCargarImagen.Name = "cmdCargarImagen";
            this.cmdCargarImagen.Size = new System.Drawing.Size(272, 23);
            this.cmdCargarImagen.TabIndex = 80;
            this.cmdCargarImagen.Text = "Cargar Imagen";
            this.cmdCargarImagen.UseVisualStyleBackColor = true;
            this.cmdCargarImagen.Click += new System.EventHandler(this.cmdCargarImagen_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // ocr1
            // 
            this.ocr1.Abort = false;
            this.ocr1.ActivationKey = "6732-8470-8351-9979";
            this.ocr1.AlternateExceptionList = new int[] {
        0};
            this.ocr1.AnalyzeAutomatic = true;
            this.ocr1.AnalyzeBrightnessIncrements = 0.5;
            this.ocr1.AnalyzeDefaultBrightness = 7;
            this.ocr1.AnalyzeDefaultResizeBitmap = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            this.ocr1.AnalyzeEndingBrightness = 9;
            this.ocr1.AnalyzeEndingResizeBitmap = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.ocr1.AnalyzeResizeBitmapIncrements = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.ocr1.AnalyzeStartingBrightness = 5;
            this.ocr1.AnalyzeStartingFontColorContrast = 50;
            this.ocr1.AnalyzeStartingResizeBitmap = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            this.ocr1.BitmapImage = ((System.Drawing.Bitmap)(resources.GetObject("ocr1.BitmapImage")));
            this.ocr1.BitmapImageFile = "";
            this.ocr1.Brightness = 7;
            this.ocr1.BrightnessMargin = 0;
            this.ocr1.BrightnessMarginIncrements = 0;
            this.ocr1.CalculateChecksum = false;
            this.ocr1.CustomerName = "Version5";
            this.ocr1.DefaultFolder = "E:\\MiTrabajo\\GestionDigital\\CodigoFuente\\Shareds\\OCR_Idiomas";
            this.ocr1.DisplayChecksum = false;
            this.ocr1.DisplayErrors = false;
            this.ocr1.EnforceSpaceRules = false;
            this.ocr1.ErrorCharacter = '?';
            this.ocr1.ErrorCorrection = OCRTools.ErrorCorrectionTypes.None;
            this.ocr1.FontColor = System.Drawing.Color.Black;
            this.ocr1.FontColorContrast = 0;
            this.ocr1.Language = OCRTools.LanguageType.English;
            this.ocr1.MaximumBarHeight = 500;
            this.ocr1.MaximumBarWidth = 100;
            this.ocr1.MaximumCharacters = 5000;
            this.ocr1.MaximumHeight = 150;
            this.ocr1.MaximumSize = 10000;
            this.ocr1.MaximumWidth = 150;
            this.ocr1.MinimumBarHeight = 20;
            this.ocr1.MinimumBarWidth = 1;
            this.ocr1.MinimumConfidence = new decimal(new int[] {
            1,
            0,
            0,
            458752});
            this.ocr1.MinimumHeight = 1;
            this.ocr1.MinimumSize = 2;
            this.ocr1.MinimumSpace = 4;
            this.ocr1.MinimumWidth = 1;
            this.ocr1.OCRType = OCRTools.OCRType.Barcode;
            this.ocr1.OrderID = "5108";
            this.ocr1.ProductName = "StandardBar";
            this.ocr1.RegistrationCodes = "4081-0097-0082-4945";
            this.ocr1.RemoveHorizontal = 0;
            this.ocr1.RemoveVertical = 0;
            this.ocr1.ResizeBitmap = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            this.ocr1.SectionHorizontalSpace = 1.5;
            this.ocr1.SectionVerticalSpace = 1.5;
            this.ocr1.StartStopCodesCharacter = "*";
            this.ocr1.StartStopCodesDisplay = false;
            this.ocr1.StartStopCodesRequired = false;
            this.ocr1.Statistics = false;
            this.ocr1.ThinCharacters = true;
            // 
            // cmdEliminarDoc
            // 
            this.cmdEliminarDoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdEliminarDoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmdEliminarDoc.Image = global::GestiónDigital.Properties.Resources.delete;
            this.cmdEliminarDoc.Location = new System.Drawing.Point(4, 678);
            this.cmdEliminarDoc.Name = "cmdEliminarDoc";
            this.cmdEliminarDoc.Size = new System.Drawing.Size(55, 54);
            this.cmdEliminarDoc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cmdEliminarDoc.TabIndex = 79;
            this.cmdEliminarDoc.TabStop = false;
            this.cmdEliminarDoc.Click += new System.EventHandler(this.cmdEliminarDoc_Click);
            // 
            // cmdGuardarFTP
            // 
            this.cmdGuardarFTP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdGuardarFTP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmdGuardarFTP.Image = global::GestiónDigital.Properties.Resources.Black_Apple_System_Icon_291;
            this.cmdGuardarFTP.Location = new System.Drawing.Point(1105, 678);
            this.cmdGuardarFTP.Name = "cmdGuardarFTP";
            this.cmdGuardarFTP.Size = new System.Drawing.Size(55, 54);
            this.cmdGuardarFTP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cmdGuardarFTP.TabIndex = 78;
            this.cmdGuardarFTP.TabStop = false;
            this.cmdGuardarFTP.Click += new System.EventHandler(this.cmdGuardarFTP_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmDOCConfigAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1172, 742);
            this.Controls.Add(this.cmdEliminarDoc);
            this.Controls.Add(this.cmdGuardarFTP);
            this.Controls.Add(this.panel1);
            this.Name = "frmDOCConfigAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Documento";
            this.Load += new System.EventHandler(this.frmDOCConfigAdmin_Load);
            this.panel1.ResumeLayout(false);
            this.pnlDocument.ResumeLayout(false);
            this.pnlDocument.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            this.pnlCampo.ResumeLayout(false);
            this.pnlCampo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgCampos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEliminarDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGuardarFTP)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button cmdCargarImagen;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgCampos;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        internal System.Windows.Forms.TextBox txtZoneX;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.TextBox txtZoneHeight;
        internal System.Windows.Forms.TextBox txtZoneY;
        internal System.Windows.Forms.TextBox txtZoneWidth;
        internal System.Windows.Forms.Label label3;
        private OCRTools.OCR ocr1;
        private System.Windows.Forms.Label lblOrden;
        private System.Windows.Forms.CheckBox chkActivo;
        private System.Windows.Forms.Button cmdEliminar;
        private System.Windows.Forms.Button cmdNuevoCampo;
        private System.Windows.Forms.Panel pnlCampo;
        private System.Windows.Forms.Button cmdOkCampo;
        internal System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmdTipoCampo;
        private System.Windows.Forms.TextBox txtValorIdentificacion;
        private System.Windows.Forms.CheckBox chkID;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtNombreCampo;
        private System.Windows.Forms.PictureBox cmdEliminarDoc;
        private System.Windows.Forms.PictureBox cmdGuardarFTP;
        private System.Windows.Forms.CheckBox chkMostrarArea;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button cmdLeerDatos;
        internal System.Windows.Forms.LinkLabel llAutoSize;
        internal System.Windows.Forms.LinkLabel llZomeIn;
        internal System.Windows.Forms.LinkLabel llOriginal;
        internal System.Windows.Forms.LinkLabel llZoomOut;
        private System.Windows.Forms.ListView listViewBar;
        private System.Windows.Forms.Panel pnlDocument;
        private System.Windows.Forms.PictureBox PictureBox1;
        private ShapeControl.ShapeControl shpArea;
        internal System.Windows.Forms.LinkLabel lMostrarRegion;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ComboBox cboGradoRotacion;
        private System.Windows.Forms.CheckBox chkNombre;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cboTipoProceso;
        internal System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboTipoDato;
        internal System.Windows.Forms.TextBox txtLongitud;

    }
}