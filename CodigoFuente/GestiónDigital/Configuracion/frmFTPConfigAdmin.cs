﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GestionDigitalCore.CoreBussines;
using GestionDigitalCore.CoreObjects;

namespace GestiónDigital.Configuracion
{
    public partial class frmFTPConfigAdmin : Form
    {
        public int id;
        private ConfiguracionFTP Obj;
        public frmFTPConfigAdmin()
        {
            id = 0;
            InitializeComponent();
        }

        private void frmFTPConfigAdmin_Load(object sender, EventArgs e)
        {
            BBConfiguracionFTP B = new BBConfiguracionFTP();
            if (id == 0)
            {
                Obj = B.getNew();
                cmdEliminar.Enabled = false;
            }
            else {
                Obj = B.getById(id);
            }
            BindearDatos();
        }

        private void BindearDatos()
        {


            try
            {
                txtBackupDirectory.Text = Obj.BackUpDirectory;
                txtDirectorioOrigen.Text = Obj.DirectorioOrigen;
                txtEmailAlerta.Text = Obj.EmailAlerta;
                txtFTPDirectorioBase.Text = Obj.FTPDirectorioBase;
                txtFTPPassword.Text = Obj.FtpPassword;
                txtFtpServer.Text = Obj.FtpServer;
                txtFTPUsuario.Text = Obj.FtpUsuario;
                txtNombre.Text = Obj.Nombre;
                txtSMTPPassword.Text = Obj.SMTPConfig.SMTPPassword;
                txtSMTPServer.Text = Obj.SMTPConfig.SMTPServer;
                txtSMTPUSer.Text = Obj.SMTPConfig.SMTPUser;

                chkDefaultConfig.Checked = Obj.DefaultConfig;
                chkEliminarArchivoOriginal.Checked = Obj.EliminarArchivoOriginal;
                chkRacionarAnchoDeBanda.Checked = Obj.RacionarAnchoDeBanda;
                chkRequiereBackup.Checked = Obj.RequiereBackup;
                chkRequiereCompresionZIP.Checked = Obj.RequiereCompresionZIP;
                numRacionarDesde.Value = Obj.RacionarDesde;
                numRacionarHasta.Value = Obj.RacionarHasta;

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ": " + ex.InnerException.Message;
                }
                MessageBox.Show(msg);
            }
			
        }
        private void GetFromScreen()
        {

            Obj.BackUpDirectory= txtBackupDirectory.Text;
            Obj.DirectorioOrigen = txtDirectorioOrigen.Text;
            Obj.EmailAlerta = txtEmailAlerta.Text;
            Obj.FTPDirectorioBase = txtFTPDirectorioBase.Text;
            Obj.FtpPassword = txtFTPPassword.Text;
            Obj.FtpServer = txtFtpServer.Text;
            Obj.FtpUsuario = txtFTPUsuario.Text;
            Obj.Nombre = txtNombre.Text;
            Obj.SMTPConfig.SMTPPassword = txtSMTPPassword.Text;
            Obj.SMTPConfig.SMTPServer = txtSMTPServer.Text;
            Obj.SMTPConfig.SMTPUser = txtSMTPUSer.Text;

            Obj.DefaultConfig = chkDefaultConfig.Checked;
            Obj.EliminarArchivoOriginal = chkEliminarArchivoOriginal.Checked;
            Obj.RacionarAnchoDeBanda = chkRacionarAnchoDeBanda.Checked;
            Obj.RequiereBackup = chkRequiereBackup.Checked;
            Obj.RequiereCompresionZIP = chkRequiereCompresionZIP.Checked;
            Obj.RacionarDesde = Convert.ToInt32( numRacionarDesde.Value);
            Obj.RacionarHasta = Convert.ToInt32(numRacionarHasta.Value);

        }
        private void cmdGuardarFTP_Click(object sender, EventArgs e)
        {


            Cursor.Current = Cursors.WaitCursor;
            try
            {
                BBConfiguracionFTP b = new BBConfiguracionFTP();
                GetFromScreen();
                b.Guardar(Obj);
                MessageBox.Show("Datos Guardados");
                this.Close();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ": " + ex.InnerException.Message;
                }
                MessageBox.Show(msg);
            } 
            Cursor.Current = Cursors.Default;
			
			
        }

        private void cmdSelectOrigen_Click(object sender, EventArgs e)
        {
            folder1.SelectedPath = txtDirectorioOrigen.Text;
            folder1.ShowDialog(this);
            txtDirectorioOrigen.Text = folder1.SelectedPath;

        }

        private void cmdSelectBackupDir_Click(object sender, EventArgs e)
        {
            folder1.SelectedPath = txtBackupDirectory.Text;
            folder1.ShowDialog(this);
            txtBackupDirectory.Text = folder1.SelectedPath;
        }

        private void cmdEliminar_Click(object sender, EventArgs e)
        {


            Cursor.Current = Cursors.WaitCursor;
            try
            {
                BBConfiguracionFTP b = new BBConfiguracionFTP();
                b.Eliminar(Obj);
                MessageBox.Show("Eliminación Exitosa");
                this.Close();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ": " + ex.InnerException.Message;
                }
                MessageBox.Show(msg);
            }

            Cursor.Current = Cursors.Default;
			
        }

    }
}