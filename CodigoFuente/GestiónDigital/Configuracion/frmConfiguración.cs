﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GestionDigitalCore.CoreBussines;
using GestionDigitalCore.CoreObjects;
using GestiónDigital.Log;

namespace GestiónDigital.Configuracion
{
    public partial class frmConfiguración : Form
    {
        public frmConfiguración()
        {
            InitializeComponent();
        }

        private void cmdFTP_MouseHover(object sender, EventArgs e)
        {
            cmdFTP.BackColor = Color.WhiteSmoke;
        }

        private void cmdFTP_MouseLeave(object sender, EventArgs e)
        {
            cmdFTP.BackColor = Color.White;
        }

        private void cmdDirs_MouseHover(object sender, EventArgs e)
        {
            cmdDirs.BackColor = Color.WhiteSmoke;
        }

        private void cmdDirs_MouseLeave(object sender, EventArgs e)
        {
            cmdDirs.BackColor = Color.White;
        }

        private void cmdDocs_MouseHover(object sender, EventArgs e)
        {
            cmdDocs.BackColor = Color.WhiteSmoke;
        }

        private void cmdDocs_MouseLeave(object sender, EventArgs e)
        {
            cmdDocs.BackColor = Color.White;
        }



        private void cmdFTP_Click(object sender, EventArgs e)
        {
            OcultarPaneles();
            pnlFTP.Visible = true;
            MostrarListaFTP();
        }

        private void OcultarPaneles()
        {
            pnlFTP.Visible = false;
            pnlDirs.Visible = false;
            pnlDocs.Visible = false;
        }

        private void cmdDirs_Click(object sender, EventArgs e)
        {
            OcultarPaneles();
            pnlDirs.Visible = true;
            MostrarListaOCR();
        }

        private void MostrarListaOCR()
        {


            Cursor.Current = Cursors.WaitCursor;
            try
            {
                BBConfiguracionOCR BBC = new BBConfiguracionOCR();
                List<ConfiguracionOCR> listaDatos = BBC.GetAll();
                dgDatosOCR.AutoGenerateColumns = false;
                dgDatosOCR.Rows.Clear();
                foreach (ConfiguracionOCR c in listaDatos)
                {
                    dgDatosOCR.Rows.Add(c.ID, c.Nombre, c.DefaultConfig ? "Default" : "", c.DirectorioOrigen, c.DirectorioDestino);
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ": " + ex.InnerException.Message;
                }
                MessageBox.Show(msg);
            } 
            Cursor.Current = Cursors.Default;
			
			
        }

        private void cmdDocs_Click(object sender, EventArgs e)
        {
            OcultarPaneles();
            pnlDocs.Visible = true;
            MostrarListaDOCS();
        }

        private void MostrarListaDOCS()
        {

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                BBDocumento BBC = new BBDocumento();
                List<Documento> listaDatos = BBC.GetAll();
                dgDatosDoc.AutoGenerateColumns = false;
                dgDatosDoc.Rows.Clear();
                foreach (Documento c in listaDatos)
                {
                    dgDatosDoc.Rows.Add(c.ID, c.Nombre, c.MisCampos.Count.ToString());
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ": " + ex.InnerException.Message;
                }
                MessageBox.Show(msg);
            } 
            Cursor.Current = Cursors.Default;
			
			
        }


        private void MostrarListaFTP()
        {


            Cursor.Current = Cursors.WaitCursor;
            try
            {
                BBConfiguracionFTP BBC = new BBConfiguracionFTP();
                List<ConfiguracionFTP> listaDatos = BBC.GetAll();
                dgDatosFTP.AutoGenerateColumns = false;
                dgDatosFTP.Rows.Clear();
                foreach (ConfiguracionFTP c in listaDatos)
                {
                    dgDatosFTP.Rows.Add(c.ID, c.Nombre, c.DefaultConfig ? "Default" : "", c.FtpServer, c.DirectorioOrigen);
                }

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ": " + ex.InnerException.Message;
                }
                MessageBox.Show(msg);
            } 
            Cursor.Current = Cursors.Default;
			
			
        }

        private void dgDatosFTP_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            EditarFTP((int)dgDatosFTP.Rows[e.RowIndex].Cells[0].Value );
        }

        private void EditarFTP(int IDFTP)
        {
            frmFTPConfigAdmin f = new frmFTPConfigAdmin();
            f.id = IDFTP;
            f.ShowDialog(this);
            MostrarListaFTP();
        }

        private void cmdNuevo_Click(object sender, EventArgs e)
        {
            EditarFTP(0);
        }

        private void cmdLogs_Click(object sender, EventArgs e)
        {
            frmFTPLogList f = new frmFTPLogList();
            f.IdConfiguracionFTP = Convert.ToInt32(dgDatosFTP.SelectedRows[0].Cells[0].Value);
            f.ShowDialog(this);
        }

        private void cmdNuevoOCR_Click(object sender, EventArgs e)
        {
            EditarOCR(0);
        }

        private void EditarOCR(int IDOCR)
        {
            frmOCRConfigAdmin f = new frmOCRConfigAdmin();
            f.id = IDOCR;
            f.ShowDialog(this);
            MostrarListaOCR();
        }

        private void dgDatosOCR_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            EditarOCR((int)dgDatosOCR.Rows[e.RowIndex].Cells[0].Value);
        }

        private void dgDatosDoc_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            EditarDOC((int)dgDatosDoc.Rows[e.RowIndex].Cells[0].Value);
        }

        private void EditarDOC(int IDDOC)
        {
            frmDOCConfigAdmin f = new frmDOCConfigAdmin();
            f.id = IDDOC;
            
            f.ShowDialog(this);
            MostrarListaDOCS();
        }

        private void cmdNuevoDoc_Click(object sender, EventArgs e)
        {
            EditarDOC(0);
        }

        private void cmdLogOCR_Click(object sender, EventArgs e)
        {
            frmOCRLogList f = new frmOCRLogList();
            f.ShowDialog(this);
        }


    }
}