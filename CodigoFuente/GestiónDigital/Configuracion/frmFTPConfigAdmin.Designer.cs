﻿namespace GestiónDigital.Configuracion
{
    partial class frmFTPConfigAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdGuardarFTP = new System.Windows.Forms.PictureBox();
            this.cmdEliminar = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmdSelectBackupDir = new System.Windows.Forms.Button();
            this.cmdSelectOrigen = new System.Windows.Forms.Button();
            this.numRacionarHasta = new System.Windows.Forms.NumericUpDown();
            this.numRacionarDesde = new System.Windows.Forms.NumericUpDown();
            this.chkDefaultConfig = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.chkRacionarAnchoDeBanda = new System.Windows.Forms.CheckBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtSMTPPassword = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSMTPUSer = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSMTPServer = new System.Windows.Forms.TextBox();
            this.txtEmailAlerta = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.chkRequiereCompresionZIP = new System.Windows.Forms.CheckBox();
            this.chkRequiereBackup = new System.Windows.Forms.CheckBox();
            this.txtFTPDirectorioBase = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.chkEliminarArchivoOriginal = new System.Windows.Forms.CheckBox();
            this.txtBackupDirectory = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDirectorioOrigen = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtFTPPassword = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFTPUsuario = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFtpServer = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.folder1 = new System.Windows.Forms.FolderBrowserDialog();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGuardarFTP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEliminar)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRacionarHasta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRacionarDesde)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmdGuardarFTP
            // 
            this.cmdGuardarFTP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmdGuardarFTP.Image = global::GestiónDigital.Properties.Resources.Black_Apple_System_Icon_291;
            this.cmdGuardarFTP.Location = new System.Drawing.Point(598, 502);
            this.cmdGuardarFTP.Name = "cmdGuardarFTP";
            this.cmdGuardarFTP.Size = new System.Drawing.Size(55, 54);
            this.cmdGuardarFTP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cmdGuardarFTP.TabIndex = 39;
            this.cmdGuardarFTP.TabStop = false;
            this.cmdGuardarFTP.Click += new System.EventHandler(this.cmdGuardarFTP_Click);
            // 
            // cmdEliminar
            // 
            this.cmdEliminar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmdEliminar.Image = global::GestiónDigital.Properties.Resources.delete;
            this.cmdEliminar.Location = new System.Drawing.Point(12, 501);
            this.cmdEliminar.Name = "cmdEliminar";
            this.cmdEliminar.Size = new System.Drawing.Size(55, 54);
            this.cmdEliminar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cmdEliminar.TabIndex = 55;
            this.cmdEliminar.TabStop = false;
            this.cmdEliminar.Click += new System.EventHandler(this.cmdEliminar_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cmdSelectBackupDir);
            this.panel2.Controls.Add(this.cmdSelectOrigen);
            this.panel2.Controls.Add(this.numRacionarHasta);
            this.panel2.Controls.Add(this.numRacionarDesde);
            this.panel2.Controls.Add(this.chkDefaultConfig);
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.chkRacionarAnchoDeBanda);
            this.panel2.Controls.Add(this.txtNombre);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.chkRequiereCompresionZIP);
            this.panel2.Controls.Add(this.chkRequiereBackup);
            this.panel2.Controls.Add(this.txtFTPDirectorioBase);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.chkEliminarArchivoOriginal);
            this.panel2.Controls.Add(this.txtBackupDirectory);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.txtDirectorioOrigen);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.txtFTPPassword);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.txtFTPUsuario);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.txtFtpServer);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Location = new System.Drawing.Point(3, 1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(662, 494);
            this.panel2.TabIndex = 0;
            // 
            // cmdSelectBackupDir
            // 
            this.cmdSelectBackupDir.Location = new System.Drawing.Point(621, 241);
            this.cmdSelectBackupDir.Name = "cmdSelectBackupDir";
            this.cmdSelectBackupDir.Size = new System.Drawing.Size(23, 20);
            this.cmdSelectBackupDir.TabIndex = 80;
            this.cmdSelectBackupDir.Text = "...";
            this.cmdSelectBackupDir.UseVisualStyleBackColor = true;
            this.cmdSelectBackupDir.Click += new System.EventHandler(this.cmdSelectBackupDir_Click);
            // 
            // cmdSelectOrigen
            // 
            this.cmdSelectOrigen.Location = new System.Drawing.Point(617, 46);
            this.cmdSelectOrigen.Name = "cmdSelectOrigen";
            this.cmdSelectOrigen.Size = new System.Drawing.Size(23, 20);
            this.cmdSelectOrigen.TabIndex = 79;
            this.cmdSelectOrigen.Text = "...";
            this.cmdSelectOrigen.UseVisualStyleBackColor = true;
            this.cmdSelectOrigen.Click += new System.EventHandler(this.cmdSelectOrigen_Click);
            // 
            // numRacionarHasta
            // 
            this.numRacionarHasta.Location = new System.Drawing.Point(334, 383);
            this.numRacionarHasta.Name = "numRacionarHasta";
            this.numRacionarHasta.Size = new System.Drawing.Size(55, 20);
            this.numRacionarHasta.TabIndex = 13;
            // 
            // numRacionarDesde
            // 
            this.numRacionarDesde.Location = new System.Drawing.Point(235, 383);
            this.numRacionarDesde.Name = "numRacionarDesde";
            this.numRacionarDesde.Size = new System.Drawing.Size(53, 20);
            this.numRacionarDesde.TabIndex = 12;
            // 
            // chkDefaultConfig
            // 
            this.chkDefaultConfig.AutoSize = true;
            this.chkDefaultConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDefaultConfig.Location = new System.Drawing.Point(6, 403);
            this.chkDefaultConfig.Name = "chkDefaultConfig";
            this.chkDefaultConfig.Size = new System.Drawing.Size(202, 17);
            this.chkDefaultConfig.TabIndex = 14;
            this.chkDefaultConfig.Text = "Configuración FTP por Defecto";
            this.chkDefaultConfig.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(294, 383);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(34, 13);
            this.label15.TabIndex = 78;
            this.label15.Text = "Max:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(197, 383);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(31, 13);
            this.label16.TabIndex = 11;
            this.label16.Text = "Min:";
            // 
            // chkRacionarAnchoDeBanda
            // 
            this.chkRacionarAnchoDeBanda.AutoSize = true;
            this.chkRacionarAnchoDeBanda.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRacionarAnchoDeBanda.Location = new System.Drawing.Point(6, 380);
            this.chkRacionarAnchoDeBanda.Name = "chkRacionarAnchoDeBanda";
            this.chkRacionarAnchoDeBanda.Size = new System.Drawing.Size(175, 17);
            this.chkRacionarAnchoDeBanda.TabIndex = 10;
            this.chkRacionarAnchoDeBanda.Text = "Racionar Ancho de Banda";
            this.chkRacionarAnchoDeBanda.UseVisualStyleBackColor = true;
            // 
            // txtNombre
            // 
            this.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNombre.Location = new System.Drawing.Point(87, 7);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(554, 20);
            this.txtNombre.TabIndex = 0;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 13);
            this.label14.TabIndex = 74;
            this.label14.Text = "Nombre:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtSMTPPassword);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.txtSMTPUSer);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.txtSMTPServer);
            this.panel1.Controls.Add(this.txtEmailAlerta);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Location = new System.Drawing.Point(6, 267);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(638, 107);
            this.panel1.TabIndex = 73;
            // 
            // txtSMTPPassword
            // 
            this.txtSMTPPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSMTPPassword.Location = new System.Drawing.Point(100, 79);
            this.txtSMTPPassword.Name = "txtSMTPPassword";
            this.txtSMTPPassword.PasswordChar = '*';
            this.txtSMTPPassword.Size = new System.Drawing.Size(524, 20);
            this.txtSMTPPassword.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(10, 79);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Password";
            // 
            // txtSMTPUSer
            // 
            this.txtSMTPUSer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSMTPUSer.Location = new System.Drawing.Point(100, 55);
            this.txtSMTPUSer.Name = "txtSMTPUSer";
            this.txtSMTPUSer.Size = new System.Drawing.Size(524, 20);
            this.txtSMTPUSer.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(10, 55);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Usuario:";
            // 
            // txtSMTPServer
            // 
            this.txtSMTPServer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSMTPServer.Location = new System.Drawing.Point(100, 29);
            this.txtSMTPServer.Name = "txtSMTPServer";
            this.txtSMTPServer.Size = new System.Drawing.Size(524, 20);
            this.txtSMTPServer.TabIndex = 1;
            // 
            // txtEmailAlerta
            // 
            this.txtEmailAlerta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmailAlerta.Location = new System.Drawing.Point(100, 3);
            this.txtEmailAlerta.Name = "txtEmailAlerta";
            this.txtEmailAlerta.Size = new System.Drawing.Size(524, 20);
            this.txtEmailAlerta.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Email Alerta:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(10, 29);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "SMTP Server:";
            // 
            // chkRequiereCompresionZIP
            // 
            this.chkRequiereCompresionZIP.AutoSize = true;
            this.chkRequiereCompresionZIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRequiereCompresionZIP.Location = new System.Drawing.Point(158, 205);
            this.chkRequiereCompresionZIP.Name = "chkRequiereCompresionZIP";
            this.chkRequiereCompresionZIP.Size = new System.Drawing.Size(170, 17);
            this.chkRequiereCompresionZIP.TabIndex = 8;
            this.chkRequiereCompresionZIP.Text = "Requiere Compresión ZIP";
            this.chkRequiereCompresionZIP.UseVisualStyleBackColor = true;
            // 
            // chkRequiereBackup
            // 
            this.chkRequiereBackup.AutoSize = true;
            this.chkRequiereBackup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkRequiereBackup.Location = new System.Drawing.Point(9, 205);
            this.chkRequiereBackup.Name = "chkRequiereBackup";
            this.chkRequiereBackup.Size = new System.Drawing.Size(124, 17);
            this.chkRequiereBackup.TabIndex = 7;
            this.chkRequiereBackup.Text = "Requiere Backup";
            this.chkRequiereBackup.UseVisualStyleBackColor = true;
            // 
            // txtFTPDirectorioBase
            // 
            this.txtFTPDirectorioBase.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFTPDirectorioBase.Location = new System.Drawing.Point(3, 158);
            this.txtFTPDirectorioBase.Name = "txtFTPDirectorioBase";
            this.txtFTPDirectorioBase.Size = new System.Drawing.Size(638, 20);
            this.txtFTPDirectorioBase.TabIndex = 5;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 142);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(66, 13);
            this.label10.TabIndex = 69;
            this.label10.Text = "FTP Base:";
            // 
            // chkEliminarArchivoOriginal
            // 
            this.chkEliminarArchivoOriginal.AutoSize = true;
            this.chkEliminarArchivoOriginal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEliminarArchivoOriginal.Location = new System.Drawing.Point(9, 182);
            this.chkEliminarArchivoOriginal.Name = "chkEliminarArchivoOriginal";
            this.chkEliminarArchivoOriginal.Size = new System.Drawing.Size(130, 17);
            this.chkEliminarArchivoOriginal.TabIndex = 6;
            this.chkEliminarArchivoOriginal.Text = "Eliminar Originales";
            this.chkEliminarArchivoOriginal.UseVisualStyleBackColor = true;
            // 
            // txtBackupDirectory
            // 
            this.txtBackupDirectory.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBackupDirectory.Location = new System.Drawing.Point(3, 241);
            this.txtBackupDirectory.Name = "txtBackupDirectory";
            this.txtBackupDirectory.Size = new System.Drawing.Size(614, 20);
            this.txtBackupDirectory.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(6, 225);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 13);
            this.label8.TabIndex = 66;
            this.label8.Text = "Carpeta BackUp:";
            // 
            // txtDirectorioOrigen
            // 
            this.txtDirectorioOrigen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDirectorioOrigen.Location = new System.Drawing.Point(3, 46);
            this.txtDirectorioOrigen.Name = "txtDirectorioOrigen";
            this.txtDirectorioOrigen.Size = new System.Drawing.Size(608, 20);
            this.txtDirectorioOrigen.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 64;
            this.label4.Text = "Carpeta Origen:";
            // 
            // txtFTPPassword
            // 
            this.txtFTPPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFTPPassword.Location = new System.Drawing.Point(87, 122);
            this.txtFTPPassword.Name = "txtFTPPassword";
            this.txtFTPPassword.PasswordChar = '*';
            this.txtFTPPassword.Size = new System.Drawing.Size(554, 20);
            this.txtFTPPassword.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 62;
            this.label3.Text = "Password";
            // 
            // txtFTPUsuario
            // 
            this.txtFTPUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFTPUsuario.Location = new System.Drawing.Point(87, 98);
            this.txtFTPUsuario.Name = "txtFTPUsuario";
            this.txtFTPUsuario.Size = new System.Drawing.Size(554, 20);
            this.txtFTPUsuario.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 98);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 60;
            this.label2.Text = "Usuario:";
            // 
            // txtFtpServer
            // 
            this.txtFtpServer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtFtpServer.Location = new System.Drawing.Point(87, 72);
            this.txtFtpServer.Name = "txtFtpServer";
            this.txtFtpServer.Size = new System.Drawing.Size(554, 20);
            this.txtFtpServer.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 13);
            this.label1.TabIndex = 58;
            this.label1.Text = "Servidor FTP:";
            // 
            // frmFTPConfigAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 568);
            this.Controls.Add(this.cmdEliminar);
            this.Controls.Add(this.cmdGuardarFTP);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmFTPConfigAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuración de FTP";
            this.Load += new System.EventHandler(this.frmFTPConfigAdmin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.cmdGuardarFTP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEliminar)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRacionarHasta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRacionarDesde)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox cmdGuardarFTP;
        private System.Windows.Forms.PictureBox cmdEliminar;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.NumericUpDown numRacionarHasta;
        private System.Windows.Forms.NumericUpDown numRacionarDesde;
        private System.Windows.Forms.CheckBox chkDefaultConfig;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.CheckBox chkRacionarAnchoDeBanda;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtSMTPPassword;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtSMTPUSer;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSMTPServer;
        private System.Windows.Forms.TextBox txtEmailAlerta;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chkRequiereCompresionZIP;
        private System.Windows.Forms.CheckBox chkRequiereBackup;
        private System.Windows.Forms.TextBox txtFTPDirectorioBase;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.CheckBox chkEliminarArchivoOriginal;
        private System.Windows.Forms.TextBox txtBackupDirectory;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtDirectorioOrigen;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtFTPPassword;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtFTPUsuario;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFtpServer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cmdSelectOrigen;
        private System.Windows.Forms.FolderBrowserDialog folder1;
        private System.Windows.Forms.Button cmdSelectBackupDir;
    }
}