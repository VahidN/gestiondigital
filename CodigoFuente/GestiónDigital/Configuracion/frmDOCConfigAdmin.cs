﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GestionDigitalCore.CoreBussines;
using GestionDigitalCore.CoreObjects;
using System.Drawing.Imaging;
using System.IO;
using AForge.Imaging.Filters;
using AForge;
using FSO_NH.log4Net;
using System.Threading;

namespace GestiónDigital.Configuracion
{
    public partial class frmDOCConfigAdmin : Form
    {
        public int id;
        private Documento Obj;
        private Campo CampoActual;
        bool CambiarImagen = false;
        private string selectedfile = "";
        public frmDOCConfigAdmin()
        {
            InitializeComponent();
        }

        private void cmdCargarImagen_Click(object sender, EventArgs e)
        {
            openFileDialog1.DefaultExt = "tif";
            openFileDialog1.ShowDialog(this);
            CargarImagen(openFileDialog1.FileName);
        }

        public void CargarImagen(string ImagePath)
        {
            selectedfile = ImagePath;
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                CambiarImagen = true;
                PictureBox1.ImageLocation = ImagePath;

                ocr1.SetPictureBox(PictureBox1);
                ocr1.LoadPictureBox(ImagePath);
                PictureBox1.Visible = true;
                ocr1.PictureBox.ScaleImage(pnlDocument.Width, pnlDocument.Height);

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ": " + ex.InnerException.Message;
                }
                MessageBox.Show(msg);
                PictureBox1.Visible = false;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }





        private void procesarSeleccion()
        {
            Cursor.Current = Cursors.WaitCursor;

            CoreEngine.OCRCore.OCRCore MyOCR = new CoreEngine.OCRCore.OCRCore(Application.StartupPath);
            if (CampoActual == null) return;
            getCampoFromScreen();
            txtValorIdentificacion.Text = MyOCR.ProcesarImagen(ocr1.BitmapImage, CampoActual, new Archivo());		
        }
  



        private void frmDOCConfigAdmin_Load(object sender, EventArgs e)
        {
            BBDocumento B = new BBDocumento();
            if (id == 0)
            {
                Obj = B.getNew();
                cmdEliminar.Enabled = false;
            }
            else
            {
                Obj = B.getById(id);
            }
            BindearDatos();
        }
        private void BindearDatos()
        {

            Cursor.Current = Cursors.WaitCursor;
            txtNombre.Text = Obj.Nombre;
            lblOrden.Text = "Orden: " + Obj.Orden.ToString();
            chkActivo.Checked = Obj.Activo;
            if (!string.IsNullOrEmpty(Obj.PathImage))
            {
                CargarImagen(Obj.PathImage);
            }
            BindearCampos();         
            Cursor.Current = Cursors.Default;
        }

        private void BindearCampos()
        {

            dgCampos.AutoGenerateColumns = false;
            dgCampos.Rows.Clear();
            if (Obj.MisCampos != null)
            {
 
                foreach (Campo c in Obj.MisCampos)
                {
     
                    
                    dgCampos.Rows.Add(c.ID, c.Nombre, c.EsIdentificador ? "SI" : "NO", c.Orden);
                }
            }
        }

        private void cmdGuardarFTP_Click(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                BBDocumento b = new BBDocumento();
                GetFromScreen();
                b.Guardar(Obj);
                MessageBox.Show("Datos Guardados");
                this.Close();
            }
 
            catch (Exception ex)
            {
                string msg=ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ": " + ex.InnerException.Message;
                }
                MessageBox.Show(msg);
            } 
            Cursor.Current = Cursors.Default;
			
        }

        private void GetFromScreen()
        {
            Obj.Nombre = txtNombre.Text;
            Obj.Activo = chkActivo.Checked;            
            string filename = "";
            if (ocr1.PictureBox != null && CambiarImagen && !string.IsNullOrEmpty(selectedfile))
            {                                
                string dir = Application.StartupPath + "\\DocImg";
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
                filename = dir + "\\" + Obj.Nombre +DateTime.Now.Ticks.ToString()+ ".tif";                
                File.Copy(selectedfile, filename);
            }
            Obj.PathImage = filename;
        }

        private void cmdNuevoCampo_Click(object sender, EventArgs e)
        {
            
            EditarCampo("");
        }

        private void dgCampos_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            EditarCampo((string)dgCampos.Rows[e.RowIndex].Cells[1].Value);
        }

        private void EditarCampo(string Nombre)
        {

            if (Nombre != "")
                foreach (Campo c in Obj.MisCampos)
                {
                    if (c.Nombre == Nombre)
                    {
                        CampoActual = c; break;
                    }
                }
            else
            {
                CampoActual = new Campo();
            }
            if (CampoActual.MejorarImagen)
                cboTipoProceso.SelectedIndex = CampoActual.TipoPreprocesamiento;
            else
            {
                cboTipoProceso.SelectedIndex = 0;
            }

            txtNombreCampo.Text = CampoActual.Nombre;
            cmdTipoCampo.SelectedIndex = (int)CampoActual.TipoCampo;
            txtZoneX.Text = CampoActual.XInicial.ToString();
            txtZoneY.Text = CampoActual.YInicial.ToString();
            txtZoneWidth.Text = CampoActual.Ancho.ToString();
            txtZoneHeight.Text = CampoActual.Alto.ToString();
            txtLongitud.Text = CampoActual.Longitud.ToString();
            cboTipoDato.SelectedItem = SeleccionarTipoDato(CampoActual.TipoDato);
            cboGradoRotacion.SelectedItem = SeleccionarGradoRotacion(CampoActual.GradoRotacion); 
            chkID.Checked = CampoActual.EsIdentificador;
            txtValorIdentificacion.Text = CampoActual.ValorIdentificacion;
            numericUpDown1.Value = CampoActual.Orden;
            chkNombre.Checked = CampoActual.EsNombreDocumento;
            pnlCampo.Visible = true;
            shpArea.Visible = false;
        }

        private void cmdOkCampo_Click(object sender, EventArgs e)
        {

            try
            {
                getCampoFromScreen();
                //Verificar si ya existe nombre.
                EliminarCampo(CampoActual.Nombre);
                Obj.MisCampos.Add(CampoActual);

                BindearCampos();
                pnlCampo.Visible = false;
                shpArea.Visible = false; 
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ": " + ex.InnerException.Message;
                }
                MessageBox.Show(msg);
            }
			
        }

        private void getCampoFromScreen()
        {
            CampoActual.Nombre = txtNombreCampo.Text;
            CampoActual.TipoCampo = (eTipoCampo)cmdTipoCampo.SelectedIndex;
            CampoActual.XInicial = Convert.ToInt32(txtZoneX.Text);
            CampoActual.YInicial = Convert.ToInt32(txtZoneY.Text);
            CampoActual.Ancho = Convert.ToInt32(txtZoneWidth.Text);
            CampoActual.Alto = Convert.ToInt32(txtZoneHeight.Text);
            CampoActual.Longitud = Convert.ToInt32(txtLongitud.Text);
            CampoActual.TipoDato = AsignarTipoDato();
            CampoActual.GradoRotacion = AsignarGradoRotacion();
            CampoActual.EsIdentificador = chkID.Checked;
            CampoActual.ValorIdentificacion = txtValorIdentificacion.Text;
            CampoActual.Orden = (int)numericUpDown1.Value;            
            CampoActual.EsNombreDocumento = chkNombre.Checked;
            if (cboTipoProceso.SelectedIndex == 0)
            {
                CampoActual.MejorarImagen = false;
            }
            else {
                CampoActual.MejorarImagen = true;
                CampoActual.TipoPreprocesamiento = cboTipoProceso.SelectedIndex;
            }
        }

        private void cmdEliminar_Click(object sender, EventArgs e)
        {
            pnlCampo.Visible = false;
            CampoActual = null;
            if (dgCampos.SelectedRows.Count > 0)
            {

                EliminarCampo((string)dgCampos.SelectedRows[0].Cells[1].Value);
                dgCampos.Rows.Remove(dgCampos.SelectedRows[0]);
            }
        }

        private void EliminarCampo(string Nombre)
        {
            int i = -1; int index = -1;
            foreach (Campo c in Obj.MisCampos)
            {
                i++;
                if (c.Nombre == Nombre)
                {
                    index=i;
                    break;
                }
                
            }
            if (index > -1)
                Obj.MisCampos.RemoveAt(i);
        }

        private void chkMostrarArea_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMostrarArea.Checked)
            {
                decimal relacion = ocr1.PictureBox.ImageOriginal.Width / ocr1.PictureBox.Image.Width;
                CampoActual.XInicial = Convert.ToInt32(txtZoneX.Text);
                CampoActual.YInicial = Convert.ToInt32(txtZoneY.Text);
                CampoActual.Ancho = Convert.ToInt32(txtZoneWidth.Text);
                CampoActual.Alto = Convert.ToInt32(txtZoneHeight.Text);

                shpArea.Top = PictureBox1.Top + CampoActual.YInicial / Convert.ToInt32( relacion);
                shpArea.Left = PictureBox1.Left + CampoActual.XInicial / Convert.ToInt32( relacion);
                shpArea.Width = CampoActual.Ancho / Convert.ToInt32( relacion);
                shpArea.Height = CampoActual.Alto / Convert.ToInt32(relacion);
                shpArea.Visible = true;
            }
            else {
                shpArea.Visible = false;
            }
        }

        private void cmdEliminarDoc_Click(object sender, EventArgs e)
        {

            try
            {
                BBDocumento BBD = new BBDocumento();

                BBD.Eliminar(Obj);
                this.Close();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ": " + ex.InnerException.Message;
                }
                MessageBox.Show(msg);
            }
			
        }

        private void cmdLeerDatos_Click(object sender, EventArgs e)
        {
            try
            {
   
                    procesarSeleccion();
               
            }
            catch (Exception) { }
            finally {
                Cursor.Current = Cursors.Default;
            }
        }


        private void llOriginal_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ocr1.PictureBox.ScaleImageOriginalSize();
        }

        private void llAutoSize_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ocr1.PictureBox.ScaleImage(pnlDocument.Width, pnlDocument.Height);
        }

        private void llZomeIn_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ocr1.PictureBox.Zoom(200);
        }

        private void llZoomOut_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ocr1.PictureBox.Zoom(-200);
        }




        private void PictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            if (pnlCampo.Enabled)
            {
                try
                {
                    txtZoneX.Text = ocr1.PictureBox.X.ToString();
                    txtZoneY.Text = ocr1.PictureBox.Y.ToString();
                    txtZoneWidth.Text = ocr1.PictureBox.Width.ToString();
                    txtZoneHeight.Text = ocr1.PictureBox.Height.ToString();

                    CampoActual.XInicial = Convert.ToInt32(txtZoneX.Text);
                    CampoActual.YInicial = Convert.ToInt32(txtZoneY.Text);
                    CampoActual.Ancho = Convert.ToInt32(txtZoneWidth.Text);
                    CampoActual.Alto = Convert.ToInt32(txtZoneHeight.Text);
                    CampoActual.GradoRotacion = AsignarGradoRotacion();

                    bool lResult = ocr1.SetRegion(CampoActual.XInicial,
                                                  CampoActual.YInicial,
                                                  CampoActual.Ancho,
                                                  CampoActual.Alto,
                                                  CampoActual.GradoRotacion);
                }
                catch { }

            }
        }

        private void chkID_CheckedChanged(object sender, EventArgs e)
        {
            CampoActual.EsIdentificador = chkID.Checked;
        }



        private void timer1_Tick(object sender, EventArgs e)
        {
            CargarImagen(selectedfile);
            timer1.Stop();
        }

        private void cboGradoRotacion_SelectedIndexChanged(object sender, EventArgs e)
        {
            CampoActual.GradoRotacion = AsignarGradoRotacion();
        }
        private int AsignarGradoRotacion()
        {
            switch (cboGradoRotacion.SelectedItem.ToString())
            {
                case "0°": return 0; 
                case "90°": return 90; 
                case "180°": return 180; 
                case "270°": return 270; 
                default: return 0; 
            }
        
        }
        private string  SeleccionarGradoRotacion(int valorgrado)
        {
            switch (valorgrado)
            {
                case 0: return "0°";
                case 90: return "90°";
                case 180: return "180°";
                case 270: return "270°";
                default: return "0°";
            }

        }

        private string AsignarTipoDato()
        {
            switch (cboTipoDato.SelectedItem.ToString())
            {
                case "Omitir": return "O";
                case "Texto": return "T";
                case "Numérico": return "N";
                default: return "O";
            }

        }
        private string SeleccionarTipoDato(string valortipodato)
        {
            switch (valortipodato)
            {
                case "O": return "Omitir";
                case "T": return "Texto";
                case "N": return "Numérico";
                default: return "Omitir";
            }

        }

        private void txtLongitud_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 58 && e.KeyChar <= 127)
            {
                e.Handled = true;
                return;
            }
        }

        private void txtZoneX_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 58 && e.KeyChar <= 127)
            {
                e.Handled = true;
                return;
            }
        }

        private void txtZoneY_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 58 && e.KeyChar <= 127)
            {
                e.Handled = true;
                return;
            }
        }

        private void txtZoneWidth_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 58 && e.KeyChar <= 127)
            {
                e.Handled = true;
                return;
            }
        }

        private void txtZoneHeight_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar >= 58 && e.KeyChar <= 127)
            {
                e.Handled = true;
                return;
            }
        }







        //private void ValidarIngresoNro(TextBox otxt) 
        //{
        //    int Result = 0;
        //    if (!int.TryParse(otxt.Text, out Result))
        //        otxt.Text = "";
       
        //}

        //private void txtLongitud_KeyUp(object sender, KeyEventArgs e)
        //{
        //    ValidarIngresoNro(txtLongitud);
        //}








    }
}
