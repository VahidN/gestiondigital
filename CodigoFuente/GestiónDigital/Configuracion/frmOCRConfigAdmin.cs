﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GestionDigitalCore.CoreObjects;
using GestionDigitalCore.CoreBussines;

namespace GestiónDigital.Configuracion
{
    public partial class frmOCRConfigAdmin : Form
    {
        public int id;
        private ConfiguracionOCR Obj;
        public frmOCRConfigAdmin()
        {
            id = 0;
            InitializeComponent();
        }

        private void cmdSelectOrigen_Click(object sender, EventArgs e)
        {
            folder1.SelectedPath = txtDirectorioOrigen.Text;
            folder1.ShowDialog(this);
            txtDirectorioOrigen.Text = folder1.SelectedPath;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            folder1.SelectedPath = txtDirectorioDestino.Text;
            folder1.ShowDialog(this);
            txtDirectorioDestino.Text = folder1.SelectedPath;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            folder1.SelectedPath = txtDirectorioErrores.Text;
            folder1.ShowDialog(this);
            txtDirectorioErrores.Text = folder1.SelectedPath;
        }

        private void frmOCRConfigAdmin_Load(object sender, EventArgs e)
        {
            BBConfiguracionOCR B = new BBConfiguracionOCR();
            if (id == 0)
            {
                Obj = B.getNew();
                cmdEliminar.Enabled = false;
            }
            else
            {
                Obj = B.getById(id);
            }
            BindearDatos();
        }
        private void BindearDatos()
        {



            Cursor.Current = Cursors.WaitCursor;
            txtDirectorioDestino.Text = Obj.DirectorioDestino;
            txtDirectorioErrores.Text = Obj.DirectorioErrores;
            txtDirectorioOrigen.Text = Obj.DirectorioOrigen;
            txtTemporales.Text = Obj.DirectorioTemp;
            txtEmailAlerta.Text = Obj.EmailAlerta;
            txtSMTPPassword.Text = Obj.MySMTPPassword;

            txtSMTPServer.Text = Obj.MySMTPServer;
            txtSMTPUSer.Text = Obj.MySMTPUser;
            txtNombre.Text = Obj.Nombre;
            chkProcesarTiffMultiplePag.Checked = Obj.ProcesarTiffMultipaginas;
            chkDefaultConfig.Checked = Obj.DefaultConfig;
            cboTipoArchivo.SelectedItem = Obj.TipoArchivo;
            txtDuplicados.Text = Obj.DirectorioDuplicados;
            Cursor.Current = Cursors.Default;


            BBDocumento BBD = new BBDocumento();
            List<Documento> l = BBD.GetAll();
            cboTipoDoc.DataSource = l;
            cboTipoDoc.DisplayMember = "Nombre";
            cboTipoDoc.ValueMember = "ID";

            cboTipoDoc.SelectedValue = Obj.IdDocumento;

        }
        private void GetFromScreen()
        {

            Obj.DirectorioDestino = txtDirectorioDestino.Text;
            Obj.DirectorioErrores = txtDirectorioErrores.Text;
            Obj.DirectorioOrigen = txtDirectorioOrigen.Text;
            Obj.EmailAlerta = txtEmailAlerta.Text;
            Obj.DirectorioTemp = txtTemporales.Text;
            Obj.MySMTPPassword = txtSMTPPassword.Text;

            Obj.MySMTPServer = txtSMTPServer.Text;
            Obj.MySMTPUser = txtSMTPUSer.Text;
            Obj.Nombre = txtNombre.Text;
            Obj.ProcesarTiffMultipaginas = chkProcesarTiffMultiplePag.Checked;
            Obj.DefaultConfig = chkDefaultConfig.Checked;
            Obj.TipoArchivo = (string)cboTipoArchivo.SelectedItem;
            Obj.IdDocumento = 0;
            Obj.DirectorioDuplicados = txtDuplicados.Text;
            if(cboTipoDoc.SelectedItem!=null)
                Obj.IdDocumento = ((Documento)cboTipoDoc.SelectedItem).ID;

        }

        private void cmdGuardarFTP_Click(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                BBConfiguracionOCR b = new BBConfiguracionOCR();
                GetFromScreen();
                b.Guardar(Obj);
                MessageBox.Show("Datos Guardados");
                this.Close();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ": " + ex.InnerException.Message;
                }
                MessageBox.Show(msg);
            } 
            Cursor.Current = Cursors.Default;
			
        }

        private void cmdEliminar_Click(object sender, EventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;
            try
            {
                BBConfiguracionOCR b = new BBConfiguracionOCR();
                b.Eliminar(Obj);
                MessageBox.Show("Eliminación Exitosa");
                this.Close();
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg += ": " + ex.InnerException.Message;
                }
                MessageBox.Show(msg);
            } 
            Cursor.Current = Cursors.Default;
			
			
        }

        private void button3_Click(object sender, EventArgs e)
        {
            folder1.SelectedPath = txtDirectorioErrores.Text;
            folder1.ShowDialog(this);
            txtDuplicados.Text = folder1.SelectedPath;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            folder1.SelectedPath = txtDuplicados.Text;
            folder1.ShowDialog(this);
            txtTemporales.Text  = folder1.SelectedPath;
        }
    }
}