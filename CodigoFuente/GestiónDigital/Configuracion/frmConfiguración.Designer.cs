﻿namespace GestiónDigital.Configuracion
{
    partial class frmConfiguración
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.cmdDocs = new System.Windows.Forms.PictureBox();
            this.cmdDirs = new System.Windows.Forms.PictureBox();
            this.cmdFTP = new System.Windows.Forms.PictureBox();
            this.pnlFTP = new System.Windows.Forms.Panel();
            this.cmdLogs = new System.Windows.Forms.Button();
            this.dgDatosFTP = new System.Windows.Forms.DataGridView();
            this.cmdNuevo = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.pnlDocs = new System.Windows.Forms.Panel();
            this.dgDatosDoc = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmdNuevoDoc = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlDirs = new System.Windows.Forms.Panel();
            this.cmdLogOCR = new System.Windows.Forms.Button();
            this.dgDatosOCR = new System.Windows.Forms.DataGridView();
            this.cmdNuevoOCR = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColNombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDocs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDirs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFTP)).BeginInit();
            this.pnlFTP.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDatosFTP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNuevo)).BeginInit();
            this.pnlDocs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDatosDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNuevoDoc)).BeginInit();
            this.pnlDirs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDatosOCR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNuevoOCR)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.cmdDocs);
            this.splitContainer1.Panel1.Controls.Add(this.cmdDirs);
            this.splitContainer1.Panel1.Controls.Add(this.cmdFTP);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.pnlFTP);
            this.splitContainer1.Panel2.Controls.Add(this.pnlDocs);
            this.splitContainer1.Panel2.Controls.Add(this.pnlDirs);
            this.splitContainer1.Panel2.Controls.Add(this.pictureBox1);
            this.splitContainer1.Size = new System.Drawing.Size(869, 594);
            this.splitContainer1.SplitterDistance = 157;
            this.splitContainer1.TabIndex = 0;
            // 
            // cmdDocs
            // 
            this.cmdDocs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmdDocs.Image = global::GestiónDigital.Properties.Resources.Mail4;
            this.cmdDocs.Location = new System.Drawing.Point(25, 242);
            this.cmdDocs.Name = "cmdDocs";
            this.cmdDocs.Size = new System.Drawing.Size(100, 96);
            this.cmdDocs.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cmdDocs.TabIndex = 2;
            this.cmdDocs.TabStop = false;
            this.toolTip1.SetToolTip(this.cmdDocs, "Configuración de Documentos");
            this.cmdDocs.MouseLeave += new System.EventHandler(this.cmdDocs_MouseLeave);
            this.cmdDocs.Click += new System.EventHandler(this.cmdDocs_Click);
            this.cmdDocs.MouseHover += new System.EventHandler(this.cmdDocs_MouseHover);
            // 
            // cmdDirs
            // 
            this.cmdDirs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmdDirs.Image = global::GestiónDigital.Properties.Resources.Folder;
            this.cmdDirs.Location = new System.Drawing.Point(25, 130);
            this.cmdDirs.Name = "cmdDirs";
            this.cmdDirs.Size = new System.Drawing.Size(100, 96);
            this.cmdDirs.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cmdDirs.TabIndex = 1;
            this.cmdDirs.TabStop = false;
            this.toolTip1.SetToolTip(this.cmdDirs, "Configuración de OCR");
            this.cmdDirs.MouseLeave += new System.EventHandler(this.cmdDirs_MouseLeave);
            this.cmdDirs.Click += new System.EventHandler(this.cmdDirs_Click);
            this.cmdDirs.MouseHover += new System.EventHandler(this.cmdDirs_MouseHover);
            // 
            // cmdFTP
            // 
            this.cmdFTP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmdFTP.Image = global::GestiónDigital.Properties.Resources.Location_FTP_256x256;
            this.cmdFTP.Location = new System.Drawing.Point(25, 18);
            this.cmdFTP.Name = "cmdFTP";
            this.cmdFTP.Size = new System.Drawing.Size(100, 96);
            this.cmdFTP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cmdFTP.TabIndex = 0;
            this.cmdFTP.TabStop = false;
            this.toolTip1.SetToolTip(this.cmdFTP, "Configración de Acceso a Servidor FTP");
            this.cmdFTP.MouseLeave += new System.EventHandler(this.cmdFTP_MouseLeave);
            this.cmdFTP.Click += new System.EventHandler(this.cmdFTP_Click);
            this.cmdFTP.MouseHover += new System.EventHandler(this.cmdFTP_MouseHover);
            // 
            // pnlFTP
            // 
            this.pnlFTP.Controls.Add(this.cmdLogs);
            this.pnlFTP.Controls.Add(this.dgDatosFTP);
            this.pnlFTP.Controls.Add(this.cmdNuevo);
            this.pnlFTP.Controls.Add(this.label5);
            this.pnlFTP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlFTP.Location = new System.Drawing.Point(0, 0);
            this.pnlFTP.Name = "pnlFTP";
            this.pnlFTP.Size = new System.Drawing.Size(708, 594);
            this.pnlFTP.TabIndex = 0;
            this.pnlFTP.Visible = false;
            // 
            // cmdLogs
            // 
            this.cmdLogs.Location = new System.Drawing.Point(593, 6);
            this.cmdLogs.Name = "cmdLogs";
            this.cmdLogs.Size = new System.Drawing.Size(75, 30);
            this.cmdLogs.TabIndex = 60;
            this.cmdLogs.Text = "ver Logs";
            this.cmdLogs.UseVisualStyleBackColor = true;
            this.cmdLogs.Click += new System.EventHandler(this.cmdLogs_Click);
            // 
            // dgDatosFTP
            // 
            this.dgDatosFTP.AllowUserToAddRows = false;
            this.dgDatosFTP.AllowUserToDeleteRows = false;
            this.dgDatosFTP.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgDatosFTP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDatosFTP.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.ColNombre,
            this.Column2,
            this.Column3,
            this.Column4});
            this.dgDatosFTP.Location = new System.Drawing.Point(0, 40);
            this.dgDatosFTP.Name = "dgDatosFTP";
            this.dgDatosFTP.ReadOnly = true;
            this.dgDatosFTP.RowHeadersVisible = false;
            this.dgDatosFTP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDatosFTP.Size = new System.Drawing.Size(705, 551);
            this.dgDatosFTP.TabIndex = 56;
            this.dgDatosFTP.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgDatosFTP_CellDoubleClick);
            // 
            // cmdNuevo
            // 
            this.cmdNuevo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdNuevo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmdNuevo.Image = global::GestiónDigital.Properties.Resources.Nuevo;
            this.cmdNuevo.Location = new System.Drawing.Point(671, 9);
            this.cmdNuevo.Name = "cmdNuevo";
            this.cmdNuevo.Size = new System.Drawing.Size(28, 25);
            this.cmdNuevo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cmdNuevo.TabIndex = 55;
            this.cmdNuevo.TabStop = false;
            this.cmdNuevo.Click += new System.EventHandler(this.cmdNuevo_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(1, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(319, 25);
            this.label5.TabIndex = 9;
            this.label5.Text = "Configuración de Acceso a FTP";
            // 
            // pnlDocs
            // 
            this.pnlDocs.Controls.Add(this.dgDatosDoc);
            this.pnlDocs.Controls.Add(this.cmdNuevoDoc);
            this.pnlDocs.Controls.Add(this.label1);
            this.pnlDocs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDocs.Location = new System.Drawing.Point(0, 0);
            this.pnlDocs.Name = "pnlDocs";
            this.pnlDocs.Size = new System.Drawing.Size(708, 594);
            this.pnlDocs.TabIndex = 59;
            this.pnlDocs.Visible = false;
            // 
            // dgDatosDoc
            // 
            this.dgDatosDoc.AllowUserToAddRows = false;
            this.dgDatosDoc.AllowUserToDeleteRows = false;
            this.dgDatosDoc.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgDatosDoc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDatosDoc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.Column5});
            this.dgDatosDoc.Location = new System.Drawing.Point(0, 35);
            this.dgDatosDoc.Name = "dgDatosDoc";
            this.dgDatosDoc.ReadOnly = true;
            this.dgDatosDoc.RowHeadersVisible = false;
            this.dgDatosDoc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDatosDoc.Size = new System.Drawing.Size(708, 554);
            this.dgDatosDoc.TabIndex = 58;
            this.dgDatosDoc.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgDatosDoc_CellMouseDoubleClick);
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.HeaderText = "ID";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 500;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "Cant. Campos";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 120;
            // 
            // cmdNuevoDoc
            // 
            this.cmdNuevoDoc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdNuevoDoc.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmdNuevoDoc.Image = global::GestiónDigital.Properties.Resources.Nuevo;
            this.cmdNuevoDoc.Location = new System.Drawing.Point(674, 4);
            this.cmdNuevoDoc.Name = "cmdNuevoDoc";
            this.cmdNuevoDoc.Size = new System.Drawing.Size(28, 25);
            this.cmdNuevoDoc.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cmdNuevoDoc.TabIndex = 57;
            this.cmdNuevoDoc.TabStop = false;
            this.cmdNuevoDoc.Click += new System.EventHandler(this.cmdNuevoDoc_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 25);
            this.label1.TabIndex = 9;
            this.label1.Text = "Documentos";
            // 
            // pnlDirs
            // 
            this.pnlDirs.Controls.Add(this.cmdLogOCR);
            this.pnlDirs.Controls.Add(this.dgDatosOCR);
            this.pnlDirs.Controls.Add(this.cmdNuevoOCR);
            this.pnlDirs.Controls.Add(this.label6);
            this.pnlDirs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDirs.Location = new System.Drawing.Point(0, 0);
            this.pnlDirs.Name = "pnlDirs";
            this.pnlDirs.Size = new System.Drawing.Size(708, 594);
            this.pnlDirs.TabIndex = 1;
            this.pnlDirs.Visible = false;
            // 
            // cmdLogOCR
            // 
            this.cmdLogOCR.Location = new System.Drawing.Point(590, 3);
            this.cmdLogOCR.Name = "cmdLogOCR";
            this.cmdLogOCR.Size = new System.Drawing.Size(75, 30);
            this.cmdLogOCR.TabIndex = 61;
            this.cmdLogOCR.Text = "ver Logs";
            this.cmdLogOCR.UseVisualStyleBackColor = true;
            this.cmdLogOCR.Click += new System.EventHandler(this.cmdLogOCR_Click);
            // 
            // dgDatosOCR
            // 
            this.dgDatosOCR.AllowUserToAddRows = false;
            this.dgDatosOCR.AllowUserToDeleteRows = false;
            this.dgDatosOCR.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgDatosOCR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDatosOCR.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.dgDatosOCR.Location = new System.Drawing.Point(0, 35);
            this.dgDatosOCR.Name = "dgDatosOCR";
            this.dgDatosOCR.ReadOnly = true;
            this.dgDatosOCR.RowHeadersVisible = false;
            this.dgDatosOCR.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgDatosOCR.Size = new System.Drawing.Size(705, 556);
            this.dgDatosOCR.TabIndex = 58;
            this.dgDatosOCR.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgDatosOCR_CellMouseDoubleClick);
            // 
            // cmdNuevoOCR
            // 
            this.cmdNuevoOCR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdNuevoOCR.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmdNuevoOCR.Image = global::GestiónDigital.Properties.Resources.Nuevo;
            this.cmdNuevoOCR.Location = new System.Drawing.Point(674, 4);
            this.cmdNuevoOCR.Name = "cmdNuevoOCR";
            this.cmdNuevoOCR.Size = new System.Drawing.Size(28, 25);
            this.cmdNuevoOCR.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cmdNuevoOCR.TabIndex = 57;
            this.cmdNuevoOCR.TabStop = false;
            this.cmdNuevoOCR.Click += new System.EventHandler(this.cmdNuevoOCR_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(234, 25);
            this.label6.TabIndex = 9;
            this.label6.Text = "Configuración del OCR";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = global::GestiónDigital.Properties.Resources.GestionDigital;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(708, 594);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 60;
            this.pictureBox1.TabStop = false;
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ShowAlways = true;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.toolTip1.ToolTipTitle = "Gestión Digital";
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "Nombre";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Default";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Directorio Origen";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 250;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Directorio Destino";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 250;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "ID";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // ColNombre
            // 
            this.ColNombre.HeaderText = "Nombre";
            this.ColNombre.Name = "ColNombre";
            this.ColNombre.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Default";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "FTP Server";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 250;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "Directorio Origen";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            this.Column4.Width = 250;
            // 
            // frmConfiguración
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(869, 594);
            this.Controls.Add(this.splitContainer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConfiguración";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuración";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdDocs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDirs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFTP)).EndInit();
            this.pnlFTP.ResumeLayout(false);
            this.pnlFTP.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDatosFTP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNuevo)).EndInit();
            this.pnlDocs.ResumeLayout(false);
            this.pnlDocs.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDatosDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNuevoDoc)).EndInit();
            this.pnlDirs.ResumeLayout(false);
            this.pnlDirs.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDatosOCR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNuevoOCR)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PictureBox cmdDocs;
        private System.Windows.Forms.PictureBox cmdDirs;
        private System.Windows.Forms.PictureBox cmdFTP;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel pnlFTP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel pnlDirs;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.PictureBox cmdNuevo;
        private System.Windows.Forms.DataGridView dgDatosFTP;
        private System.Windows.Forms.DataGridView dgDatosOCR;
        private System.Windows.Forms.PictureBox cmdNuevoOCR;
        private System.Windows.Forms.Button cmdLogs;
        private System.Windows.Forms.Panel pnlDocs;
        private System.Windows.Forms.DataGridView dgDatosDoc;
        private System.Windows.Forms.PictureBox cmdNuevoDoc;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button cmdLogOCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColNombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
    }
}