﻿namespace GestiónDigital.Configuracion
{
    partial class frmOCRConfigAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.txtDuplicados = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cboTipoDoc = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboTipoArchivo = new System.Windows.Forms.ComboBox();
            this.button2 = new System.Windows.Forms.Button();
            this.txtDirectorioErrores = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtDirectorioDestino = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdSelectOrigen = new System.Windows.Forms.Button();
            this.chkDefaultConfig = new System.Windows.Forms.CheckBox();
            this.txtNombre = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtSMTPPassword = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtSMTPUSer = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSMTPServer = new System.Windows.Forms.TextBox();
            this.txtEmailAlerta = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDirectorioOrigen = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkProcesarTiffMultiplePag = new System.Windows.Forms.CheckBox();
            this.cmdEliminar = new System.Windows.Forms.PictureBox();
            this.cmdGuardarFTP = new System.Windows.Forms.PictureBox();
            this.folder1 = new System.Windows.Forms.FolderBrowserDialog();
            this.button4 = new System.Windows.Forms.Button();
            this.txtTemporales = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEliminar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGuardarFTP)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button4);
            this.panel2.Controls.Add(this.txtTemporales);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.txtDuplicados);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.cboTipoDoc);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.cboTipoArchivo);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.txtDirectorioErrores);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.txtDirectorioDestino);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.cmdSelectOrigen);
            this.panel2.Controls.Add(this.chkDefaultConfig);
            this.panel2.Controls.Add(this.txtNombre);
            this.panel2.Controls.Add(this.label14);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.txtDirectorioOrigen);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(662, 436);
            this.panel2.TabIndex = 56;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(617, 175);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(23, 20);
            this.button3.TabIndex = 92;
            this.button3.Text = "...";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtDuplicados
            // 
            this.txtDuplicados.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDuplicados.Location = new System.Drawing.Point(3, 175);
            this.txtDuplicados.Name = "txtDuplicados";
            this.txtDuplicados.Size = new System.Drawing.Size(608, 20);
            this.txtDuplicados.TabIndex = 90;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 159);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 13);
            this.label5.TabIndex = 91;
            this.label5.Text = "Carpeta Duplicados";
            // 
            // cboTipoDoc
            // 
            this.cboTipoDoc.FormattingEnabled = true;
            this.cboTipoDoc.Location = new System.Drawing.Point(6, 266);
            this.cboTipoDoc.Name = "cboTipoDoc";
            this.cboTipoDoc.Size = new System.Drawing.Size(605, 21);
            this.cboTipoDoc.TabIndex = 89;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 249);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(157, 13);
            this.label3.TabIndex = 88;
            this.label3.Text = "Procesar Archivos de tipo:";
            // 
            // cboTipoArchivo
            // 
            this.cboTipoArchivo.FormattingEnabled = true;
            this.cboTipoArchivo.Items.AddRange(new object[] {
            "*.tif",
            "*.tiff",
            "*.jpg"});
            this.cboTipoArchivo.Location = new System.Drawing.Point(506, 406);
            this.cboTipoArchivo.Name = "cboTipoArchivo";
            this.cboTipoArchivo.Size = new System.Drawing.Size(121, 21);
            this.cboTipoArchivo.TabIndex = 87;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(617, 128);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(23, 20);
            this.button2.TabIndex = 86;
            this.button2.Text = "...";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // txtDirectorioErrores
            // 
            this.txtDirectorioErrores.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDirectorioErrores.Location = new System.Drawing.Point(3, 128);
            this.txtDirectorioErrores.Name = "txtDirectorioErrores";
            this.txtDirectorioErrores.Size = new System.Drawing.Size(608, 20);
            this.txtDirectorioErrores.TabIndex = 84;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 85;
            this.label2.Text = "Carpeta Errores:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(617, 86);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(23, 20);
            this.button1.TabIndex = 83;
            this.button1.Text = "...";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtDirectorioDestino
            // 
            this.txtDirectorioDestino.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDirectorioDestino.Location = new System.Drawing.Point(3, 86);
            this.txtDirectorioDestino.Name = "txtDirectorioDestino";
            this.txtDirectorioDestino.Size = new System.Drawing.Size(608, 20);
            this.txtDirectorioDestino.TabIndex = 81;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 82;
            this.label1.Text = "Carpeta Destino:";
            // 
            // cmdSelectOrigen
            // 
            this.cmdSelectOrigen.Location = new System.Drawing.Point(617, 46);
            this.cmdSelectOrigen.Name = "cmdSelectOrigen";
            this.cmdSelectOrigen.Size = new System.Drawing.Size(23, 20);
            this.cmdSelectOrigen.TabIndex = 79;
            this.cmdSelectOrigen.Text = "...";
            this.cmdSelectOrigen.UseVisualStyleBackColor = true;
            this.cmdSelectOrigen.Click += new System.EventHandler(this.cmdSelectOrigen_Click);
            // 
            // chkDefaultConfig
            // 
            this.chkDefaultConfig.AutoSize = true;
            this.chkDefaultConfig.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDefaultConfig.Location = new System.Drawing.Point(6, 408);
            this.chkDefaultConfig.Name = "chkDefaultConfig";
            this.chkDefaultConfig.Size = new System.Drawing.Size(205, 17);
            this.chkDefaultConfig.TabIndex = 14;
            this.chkDefaultConfig.Text = "Configuración OCR por Defecto";
            this.chkDefaultConfig.UseVisualStyleBackColor = true;
            // 
            // txtNombre
            // 
            this.txtNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNombre.Location = new System.Drawing.Point(87, 7);
            this.txtNombre.Name = "txtNombre";
            this.txtNombre.Size = new System.Drawing.Size(554, 20);
            this.txtNombre.TabIndex = 0;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 13);
            this.label14.TabIndex = 74;
            this.label14.Text = "Nombre:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtSMTPPassword);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.txtSMTPUSer);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.txtSMTPServer);
            this.panel1.Controls.Add(this.txtEmailAlerta);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Location = new System.Drawing.Point(3, 293);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(638, 107);
            this.panel1.TabIndex = 73;
            // 
            // txtSMTPPassword
            // 
            this.txtSMTPPassword.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSMTPPassword.Location = new System.Drawing.Point(100, 79);
            this.txtSMTPPassword.Name = "txtSMTPPassword";
            this.txtSMTPPassword.PasswordChar = '*';
            this.txtSMTPPassword.Size = new System.Drawing.Size(524, 20);
            this.txtSMTPPassword.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(10, 79);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Password";
            // 
            // txtSMTPUSer
            // 
            this.txtSMTPUSer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSMTPUSer.Location = new System.Drawing.Point(100, 55);
            this.txtSMTPUSer.Name = "txtSMTPUSer";
            this.txtSMTPUSer.Size = new System.Drawing.Size(524, 20);
            this.txtSMTPUSer.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(10, 55);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Usuario:";
            // 
            // txtSMTPServer
            // 
            this.txtSMTPServer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSMTPServer.Location = new System.Drawing.Point(100, 29);
            this.txtSMTPServer.Name = "txtSMTPServer";
            this.txtSMTPServer.Size = new System.Drawing.Size(524, 20);
            this.txtSMTPServer.TabIndex = 1;
            // 
            // txtEmailAlerta
            // 
            this.txtEmailAlerta.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmailAlerta.Location = new System.Drawing.Point(100, 3);
            this.txtEmailAlerta.Name = "txtEmailAlerta";
            this.txtEmailAlerta.Size = new System.Drawing.Size(524, 20);
            this.txtEmailAlerta.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(10, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(78, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Email Alerta:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(10, 29);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 13);
            this.label13.TabIndex = 6;
            this.label13.Text = "SMTP Server:";
            // 
            // txtDirectorioOrigen
            // 
            this.txtDirectorioOrigen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDirectorioOrigen.Location = new System.Drawing.Point(3, 46);
            this.txtDirectorioOrigen.Name = "txtDirectorioOrigen";
            this.txtDirectorioOrigen.Size = new System.Drawing.Size(608, 20);
            this.txtDirectorioOrigen.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 64;
            this.label4.Text = "Carpeta Origen:";
            // 
            // chkProcesarTiffMultiplePag
            // 
            this.chkProcesarTiffMultiplePag.AutoSize = true;
            this.chkProcesarTiffMultiplePag.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkProcesarTiffMultiplePag.Location = new System.Drawing.Point(81, 491);
            this.chkProcesarTiffMultiplePag.Name = "chkProcesarTiffMultiplePag";
            this.chkProcesarTiffMultiplePag.Size = new System.Drawing.Size(210, 17);
            this.chkProcesarTiffMultiplePag.TabIndex = 6;
            this.chkProcesarTiffMultiplePag.Text = "Procesar Multiples Páginas (Tiff)";
            this.chkProcesarTiffMultiplePag.UseVisualStyleBackColor = true;
            this.chkProcesarTiffMultiplePag.Visible = false;
            // 
            // cmdEliminar
            // 
            this.cmdEliminar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmdEliminar.Image = global::GestiónDigital.Properties.Resources.delete;
            this.cmdEliminar.Location = new System.Drawing.Point(20, 454);
            this.cmdEliminar.Name = "cmdEliminar";
            this.cmdEliminar.Size = new System.Drawing.Size(55, 54);
            this.cmdEliminar.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cmdEliminar.TabIndex = 58;
            this.cmdEliminar.TabStop = false;
            this.cmdEliminar.Click += new System.EventHandler(this.cmdEliminar_Click);
            // 
            // cmdGuardarFTP
            // 
            this.cmdGuardarFTP.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.cmdGuardarFTP.Image = global::GestiónDigital.Properties.Resources.Black_Apple_System_Icon_291;
            this.cmdGuardarFTP.Location = new System.Drawing.Point(613, 454);
            this.cmdGuardarFTP.Name = "cmdGuardarFTP";
            this.cmdGuardarFTP.Size = new System.Drawing.Size(55, 54);
            this.cmdGuardarFTP.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.cmdGuardarFTP.TabIndex = 57;
            this.cmdGuardarFTP.TabStop = false;
            this.cmdGuardarFTP.Click += new System.EventHandler(this.cmdGuardarFTP_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(617, 219);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(23, 20);
            this.button4.TabIndex = 95;
            this.button4.Text = "...";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // txtTemporales
            // 
            this.txtTemporales.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTemporales.Location = new System.Drawing.Point(3, 219);
            this.txtTemporales.Name = "txtTemporales";
            this.txtTemporales.Size = new System.Drawing.Size(608, 20);
            this.txtTemporales.TabIndex = 93;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 203);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 13);
            this.label6.TabIndex = 94;
            this.label6.Text = "Carpeta Temporales";
            // 
            // frmOCRConfigAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 522);
            this.Controls.Add(this.cmdEliminar);
            this.Controls.Add(this.cmdGuardarFTP);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.chkProcesarTiffMultiplePag);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmOCRConfigAdmin";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configuración de OCR";
            this.Load += new System.EventHandler(this.frmOCRConfigAdmin_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEliminar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGuardarFTP)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox cmdEliminar;
        private System.Windows.Forms.PictureBox cmdGuardarFTP;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button cmdSelectOrigen;
        private System.Windows.Forms.CheckBox chkDefaultConfig;
        private System.Windows.Forms.TextBox txtNombre;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtSMTPPassword;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtSMTPUSer;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSMTPServer;
        private System.Windows.Forms.TextBox txtEmailAlerta;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chkProcesarTiffMultiplePag;
        private System.Windows.Forms.TextBox txtDirectorioOrigen;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox txtDirectorioErrores;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox txtDirectorioDestino;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.FolderBrowserDialog folder1;
        private System.Windows.Forms.ComboBox cboTipoArchivo;
        private System.Windows.Forms.ComboBox cboTipoDoc;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtDuplicados;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox txtTemporales;
        private System.Windows.Forms.Label label6;
    }
}