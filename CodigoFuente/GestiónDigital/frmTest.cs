﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using GestionDigitalCore.CoreBussines;
using GestionDigitalCore.CoreObjects;

namespace GestiónDigital
{
    public partial class frmTest : Form
    {
        public frmTest()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BBConfiguracionOCR A = new BBConfiguracionOCR();
            List<ConfiguracionOCR> Listado = A.GetAll();
            foreach (ConfiguracionOCR o in Listado)
            {
                MessageBox.Show(o.Nombre);
            }
        }
    }
}