﻿namespace GestiónDigital
{
    partial class frmSysTrayManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSysTrayManager));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.T1 = new System.Windows.Forms.Timer(this.components);
            this.NI = new System.Windows.Forms.NotifyIcon(this.components);
            this.mnuContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.licenciaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.programarTrabajosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configurarMóduloToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.procesarDocumentosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.enviarAFTPToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.TLimpiador = new System.Windows.Forms.Timer(this.components);
            this.TRamCleaner = new System.Windows.Forms.Timer(this.components);
            this.mnuContext.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(250, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(224, 59);
            this.label1.TabIndex = 0;
            this.label1.Text = "Sistema de Gestión Digital de Documentos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(251, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(158, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Módulo OCR - Código de Barras";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Red;
            this.label3.Location = new System.Drawing.Point(406, 258);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Ver. 2009.06";
            // 
            // T1
            // 
            this.T1.Interval = 30;
            this.T1.Tick += new System.EventHandler(this.T1_Tick);
            // 
            // NI
            // 
            this.NI.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.NI.BalloonTipText = "Modulo OCR - Codigo de Barras";
            this.NI.BalloonTipTitle = "Gestión Digital";
            this.NI.ContextMenuStrip = this.mnuContext;
            this.NI.Icon = ((System.Drawing.Icon)(resources.GetObject("NI.Icon")));
            this.NI.Text = "Modulo OCR - Codigo de Barras";
            this.NI.Visible = true;
            // 
            // mnuContext
            // 
            this.mnuContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.licenciaToolStripMenuItem,
            this.programarTrabajosToolStripMenuItem,
            this.configurarMóduloToolStripMenuItem,
            this.toolStripMenuItem1,
            this.procesarDocumentosToolStripMenuItem,
            this.enviarAFTPToolStripMenuItem,
            this.toolStripMenuItem2,
            this.salirToolStripMenuItem});
            this.mnuContext.Name = "mnuContext";
            this.mnuContext.Size = new System.Drawing.Size(190, 148);
            // 
            // licenciaToolStripMenuItem
            // 
            this.licenciaToolStripMenuItem.Image = global::GestiónDigital.Properties.Resources.package;
            this.licenciaToolStripMenuItem.Name = "licenciaToolStripMenuItem";
            this.licenciaToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.licenciaToolStripMenuItem.Text = "Licencia";
            this.licenciaToolStripMenuItem.Click += new System.EventHandler(this.licenciaToolStripMenuItem_Click);
            // 
            // programarTrabajosToolStripMenuItem
            // 
            this.programarTrabajosToolStripMenuItem.Image = global::GestiónDigital.Properties.Resources.Calendar;
            this.programarTrabajosToolStripMenuItem.Name = "programarTrabajosToolStripMenuItem";
            this.programarTrabajosToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.programarTrabajosToolStripMenuItem.Text = "Programar Trabajos";
            this.programarTrabajosToolStripMenuItem.Click += new System.EventHandler(this.programarTrabajosToolStripMenuItem_Click);
            // 
            // configurarMóduloToolStripMenuItem
            // 
            this.configurarMóduloToolStripMenuItem.Image = global::GestiónDigital.Properties.Resources.Settings1;
            this.configurarMóduloToolStripMenuItem.Name = "configurarMóduloToolStripMenuItem";
            this.configurarMóduloToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.configurarMóduloToolStripMenuItem.Text = "Configurar Módulo";
            this.configurarMóduloToolStripMenuItem.Click += new System.EventHandler(this.configurarMóduloToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(186, 6);
            // 
            // procesarDocumentosToolStripMenuItem
            // 
            this.procesarDocumentosToolStripMenuItem.Image = global::GestiónDigital.Properties.Resources.Search;
            this.procesarDocumentosToolStripMenuItem.Name = "procesarDocumentosToolStripMenuItem";
            this.procesarDocumentosToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.procesarDocumentosToolStripMenuItem.Text = "Procesar Documentos";
            this.procesarDocumentosToolStripMenuItem.Click += new System.EventHandler(this.procesarDocumentosToolStripMenuItem_Click);
            // 
            // enviarAFTPToolStripMenuItem
            // 
            this.enviarAFTPToolStripMenuItem.Image = global::GestiónDigital.Properties.Resources._8up161;
            this.enviarAFTPToolStripMenuItem.Name = "enviarAFTPToolStripMenuItem";
            this.enviarAFTPToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.enviarAFTPToolStripMenuItem.Text = "Enviar a FTP";
            this.enviarAFTPToolStripMenuItem.Click += new System.EventHandler(this.enviarAFTPToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(186, 6);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Image = global::GestiónDigital.Properties.Resources.stop;
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(189, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            this.salirToolStripMenuItem.Click += new System.EventHandler(this.salirToolStripMenuItem_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::GestiónDigital.Properties.Resources.online_network_256x256;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(232, 259);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // TLimpiador
            // 
            this.TLimpiador.Interval = 180000;
            this.TLimpiador.Tick += new System.EventHandler(this.TLimpiador_Tick);
            // 
            // TRamCleaner
            // 
            this.TRamCleaner.Enabled = true;
            this.TRamCleaner.Interval = 10000;
            this.TRamCleaner.Tick += new System.EventHandler(this.TRamCleaner_Tick);
            // 
            // frmSysTrayManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(476, 275);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSysTrayManager";
            this.ShowInTaskbar = false;
            this.Text = "Gestión Digital";
            this.Load += new System.EventHandler(this.frmSysTrayManager_Load);
            this.mnuContext.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer T1;
        private System.Windows.Forms.NotifyIcon NI;
        private System.Windows.Forms.ContextMenuStrip mnuContext;
        private System.Windows.Forms.ToolStripMenuItem programarTrabajosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configurarMóduloToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem procesarDocumentosToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem enviarAFTPToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem licenciaToolStripMenuItem;
        private System.Windows.Forms.Timer TLimpiador;
        private System.Windows.Forms.Timer TRamCleaner;
    }
}

