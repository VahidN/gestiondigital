﻿namespace Editor
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.dgData = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.picPreview = new System.Windows.Forms.PictureBox();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmdScanLookUp = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txtScan = new System.Windows.Forms.TextBox();
            this.cmdOkLookup = new System.Windows.Forms.Button();
            this.cmdErrLockup = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdLeer = new System.Windows.Forms.Button();
            this.txtOk = new System.Windows.Forms.TextBox();
            this.txtErr = new System.Windows.Forms.TextBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.cmdSalir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.cmdMoverTifToScan = new System.Windows.Forms.ToolStripButton();
            this.cmdEliminar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.txtGradosRotacion = new System.Windows.Forms.ToolStripTextBox();
            this.cmdRotar = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.cmZoomMas = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.cmdZoomMenos = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.cmdProcesar = new System.Windows.Forms.ToolStripButton();
            this.progreso = new System.Windows.Forms.ToolStripProgressBar();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.cboOCRType = new System.Windows.Forms.ToolStripComboBox();
            this.cboMejoras = new System.Windows.Forms.ToolStripComboBox();
            this.OCR1 = new OCRTools.OCR(this.components);
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPreview)).BeginInit();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(5, 110);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dgData);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.AutoScroll = true;
            this.splitContainer1.Panel2.Controls.Add(this.picPreview);
            this.splitContainer1.Panel2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picPreview_MouseUp);
            this.splitContainer1.Size = new System.Drawing.Size(905, 330);
            this.splitContainer1.SplitterDistance = 350;
            this.splitContainer1.TabIndex = 1;
            // 
            // dgData
            // 
            this.dgData.AllowUserToAddRows = false;
            this.dgData.AllowUserToDeleteRows = false;
            this.dgData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3});
            this.dgData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgData.Location = new System.Drawing.Point(0, 0);
            this.dgData.Name = "dgData";
            this.dgData.Size = new System.Drawing.Size(350, 330);
            this.dgData.TabIndex = 0;
            this.dgData.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellEndEdit);
            this.dgData.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgData_CellEnter);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Nombre de Archivo";
            this.Column1.Name = "Column1";
            this.Column1.Width = 300;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "NombreOriginal";
            this.Column2.Name = "Column2";
            this.Column2.Visible = false;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Extension";
            this.Column3.Name = "Column3";
            this.Column3.Visible = false;
            // 
            // picPreview
            // 
            this.picPreview.Location = new System.Drawing.Point(0, 0);
            this.picPreview.Name = "picPreview";
            this.picPreview.Size = new System.Drawing.Size(80, 90);
            this.picPreview.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picPreview.TabIndex = 10;
            this.picPreview.TabStop = false;
            this.picPreview.MouseUp += new System.Windows.Forms.MouseEventHandler(this.picPreview_MouseUp);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmdScanLookUp);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtScan);
            this.panel1.Controls.Add(this.cmdOkLookup);
            this.panel1.Controls.Add(this.cmdErrLockup);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cmdLeer);
            this.panel1.Controls.Add(this.txtOk);
            this.panel1.Controls.Add(this.txtErr);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(911, 80);
            this.panel1.TabIndex = 0;
            // 
            // cmdScanLookUp
            // 
            this.cmdScanLookUp.Location = new System.Drawing.Point(517, 55);
            this.cmdScanLookUp.Name = "cmdScanLookUp";
            this.cmdScanLookUp.Size = new System.Drawing.Size(31, 23);
            this.cmdScanLookUp.TabIndex = 21;
            this.cmdScanLookUp.Text = "...";
            this.cmdScanLookUp.UseVisualStyleBackColor = true;
            this.cmdScanLookUp.Click += new System.EventHandler(this.cmdScanLookUp_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(5, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 20;
            this.label3.Text = "OCR-SCAN:";
            // 
            // txtScan
            // 
            this.txtScan.Location = new System.Drawing.Point(127, 55);
            this.txtScan.Name = "txtScan";
            this.txtScan.Size = new System.Drawing.Size(386, 20);
            this.txtScan.TabIndex = 19;
            this.txtScan.Leave += new System.EventHandler(this.txtScan_Leave);
            // 
            // cmdOkLookup
            // 
            this.cmdOkLookup.Location = new System.Drawing.Point(517, 30);
            this.cmdOkLookup.Name = "cmdOkLookup";
            this.cmdOkLookup.Size = new System.Drawing.Size(31, 23);
            this.cmdOkLookup.TabIndex = 16;
            this.cmdOkLookup.Text = "...";
            this.cmdOkLookup.UseVisualStyleBackColor = true;
            this.cmdOkLookup.Click += new System.EventHandler(this.LoadOkFolder_Click);
            // 
            // cmdErrLockup
            // 
            this.cmdErrLockup.Location = new System.Drawing.Point(517, 0);
            this.cmdErrLockup.Name = "cmdErrLockup";
            this.cmdErrLockup.Size = new System.Drawing.Size(31, 23);
            this.cmdErrLockup.TabIndex = 15;
            this.cmdErrLockup.Text = "...";
            this.cmdErrLockup.UseVisualStyleBackColor = true;
            this.cmdErrLockup.Click += new System.EventHandler(this.LoadErrorFolder_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(5, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "OCR-OK:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "OCR-ERR:";
            // 
            // cmdLeer
            // 
            this.cmdLeer.Location = new System.Drawing.Point(547, 0);
            this.cmdLeer.Name = "cmdLeer";
            this.cmdLeer.Size = new System.Drawing.Size(116, 23);
            this.cmdLeer.TabIndex = 11;
            this.cmdLeer.Text = "Leer Carpeta";
            this.cmdLeer.UseVisualStyleBackColor = true;
            this.cmdLeer.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtOk
            // 
            this.txtOk.Location = new System.Drawing.Point(127, 30);
            this.txtOk.Name = "txtOk";
            this.txtOk.Size = new System.Drawing.Size(386, 20);
            this.txtOk.TabIndex = 10;
            this.txtOk.Leave += new System.EventHandler(this.txtOk_Leave);
            // 
            // txtErr
            // 
            this.txtErr.Location = new System.Drawing.Point(128, 3);
            this.txtErr.Name = "txtErr";
            this.txtErr.Size = new System.Drawing.Size(385, 20);
            this.txtErr.TabIndex = 9;
            this.txtErr.Leave += new System.EventHandler(this.txtErr_Leave);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cmdSalir,
            this.toolStripSeparator1,
            this.cmdMoverTifToScan,
            this.cmdEliminar,
            this.toolStripSeparator3,
            this.txtGradosRotacion,
            this.cmdRotar,
            this.toolStripSeparator2,
            this.cmZoomMas,
            this.toolStripButton1,
            this.cmdZoomMenos,
            this.toolStripSeparator4,
            this.cmdProcesar,
            this.progreso,
            this.toolStripSeparator5,
            this.toolStripButton2,
            this.cboOCRType,
            this.cboMejoras,
            this.toolStripButton3});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(911, 25);
            this.toolStrip1.TabIndex = 18;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // cmdSalir
            // 
            this.cmdSalir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdSalir.Image = ((System.Drawing.Image)(resources.GetObject("cmdSalir.Image")));
            this.cmdSalir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdSalir.Name = "cmdSalir";
            this.cmdSalir.Size = new System.Drawing.Size(23, 22);
            this.cmdSalir.Text = "toolStripButton1";
            this.cmdSalir.ToolTipText = "Salir";
            this.cmdSalir.Click += new System.EventHandler(this.cmdSalir_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // cmdMoverTifToScan
            // 
            this.cmdMoverTifToScan.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdMoverTifToScan.Image = ((System.Drawing.Image)(resources.GetObject("cmdMoverTifToScan.Image")));
            this.cmdMoverTifToScan.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdMoverTifToScan.Name = "cmdMoverTifToScan";
            this.cmdMoverTifToScan.Size = new System.Drawing.Size(23, 22);
            this.cmdMoverTifToScan.Text = "toolStripButton2";
            this.cmdMoverTifToScan.ToolTipText = "Mover a Carpeta Scan";
            this.cmdMoverTifToScan.Click += new System.EventHandler(this.cmdMoverTifToScan_Click);
            // 
            // cmdEliminar
            // 
            this.cmdEliminar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdEliminar.Image = ((System.Drawing.Image)(resources.GetObject("cmdEliminar.Image")));
            this.cmdEliminar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdEliminar.Name = "cmdEliminar";
            this.cmdEliminar.Size = new System.Drawing.Size(23, 22);
            this.cmdEliminar.Text = "toolStripButton3";
            this.cmdEliminar.ToolTipText = "Eliminar Imagenes";
            this.cmdEliminar.Click += new System.EventHandler(this.cmdEliminar_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // txtGradosRotacion
            // 
            this.txtGradosRotacion.Name = "txtGradosRotacion";
            this.txtGradosRotacion.Size = new System.Drawing.Size(30, 25);
            this.txtGradosRotacion.Text = "90";
            this.txtGradosRotacion.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cmdRotar
            // 
            this.cmdRotar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdRotar.Image = global::Editor.Properties.Resources.refresh;
            this.cmdRotar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdRotar.Name = "cmdRotar";
            this.cmdRotar.Size = new System.Drawing.Size(23, 22);
            this.cmdRotar.Text = "toolStripButton1";
            this.cmdRotar.ToolTipText = "Rotar";
            this.cmdRotar.Click += new System.EventHandler(this.cmdRotar_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // cmZoomMas
            // 
            this.cmZoomMas.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmZoomMas.Image = ((System.Drawing.Image)(resources.GetObject("cmZoomMas.Image")));
            this.cmZoomMas.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmZoomMas.Name = "cmZoomMas";
            this.cmZoomMas.Size = new System.Drawing.Size(23, 22);
            this.cmZoomMas.Text = "Aumentar Zoom";
            this.cmZoomMas.Click += new System.EventHandler(this.cmZoomMas_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripButton1.ToolTipText = "Ajustar Imagen";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // cmdZoomMenos
            // 
            this.cmdZoomMenos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdZoomMenos.Image = ((System.Drawing.Image)(resources.GetObject("cmdZoomMenos.Image")));
            this.cmdZoomMenos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdZoomMenos.Name = "cmdZoomMenos";
            this.cmdZoomMenos.Size = new System.Drawing.Size(23, 22);
            this.cmdZoomMenos.Text = "Achicar Zoom";
            this.cmdZoomMenos.Click += new System.EventHandler(this.cmdZoomMenos_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // cmdProcesar
            // 
            this.cmdProcesar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cmdProcesar.Image = ((System.Drawing.Image)(resources.GetObject("cmdProcesar.Image")));
            this.cmdProcesar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cmdProcesar.Name = "cmdProcesar";
            this.cmdProcesar.Size = new System.Drawing.Size(23, 22);
            this.cmdProcesar.Text = "toolStripButton1";
            this.cmdProcesar.ToolTipText = "Procesar Cambios";
            this.cmdProcesar.Click += new System.EventHandler(this.cmdProcesar_Click);
            // 
            // progreso
            // 
            this.progreso.Name = "progreso";
            this.progreso.Size = new System.Drawing.Size(100, 22);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton2.Text = "toolStripButton2";
            this.toolStripButton2.ToolTipText = "Carpetas";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // cboOCRType
            // 
            this.cboOCRType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOCRType.Items.AddRange(new object[] {
            "Barras",
            "Texto",
            "Ambos",
            "Ninguno"});
            this.cboOCRType.Name = "cboOCRType";
            this.cboOCRType.Size = new System.Drawing.Size(121, 25);
            // 
            // cboMejoras
            // 
            this.cboMejoras.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboMejoras.Items.AddRange(new object[] {
            "Preprocesar Imagen",
            "No Preprocesar Imagen"});
            this.cboMejoras.Name = "cboMejoras";
            this.cboMejoras.Size = new System.Drawing.Size(121, 25);
            // 
            // OCR1
            // 
            this.OCR1.Abort = false;
            this.OCR1.AlternateExceptionList = new int[] {
        0};
            this.OCR1.AnalyzeAutomatic = true;
            this.OCR1.AnalyzeBrightnessIncrements = 0.5;
            this.OCR1.AnalyzeDefaultBrightness = 7;
            this.OCR1.AnalyzeDefaultResizeBitmap = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            this.OCR1.AnalyzeEndingBrightness = 9;
            this.OCR1.AnalyzeEndingResizeBitmap = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.OCR1.AnalyzeResizeBitmapIncrements = new decimal(new int[] {
            5,
            0,
            0,
            65536});
            this.OCR1.AnalyzeStartingBrightness = 5;
            this.OCR1.AnalyzeStartingFontColorContrast = 50;
            this.OCR1.AnalyzeStartingResizeBitmap = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            this.OCR1.BitmapImage = ((System.Drawing.Bitmap)(resources.GetObject("OCR1.BitmapImage")));
            this.OCR1.BitmapImageFile = "";
            this.OCR1.Brightness = 7;
            this.OCR1.BrightnessMargin = 0;
            this.OCR1.BrightnessMarginIncrements = 0;
            this.OCR1.CalculateChecksum = true;
            this.OCR1.DefaultFolder = "E:\\MiTrabajo\\GestionDigital\\CodigoFuente\\Shareds\\OCR_Idiomas";
            this.OCR1.DisplayChecksum = false;
            this.OCR1.DisplayErrors = false;
            this.OCR1.EnforceSpaceRules = false;
            this.OCR1.ErrorCharacter = '?';
            this.OCR1.ErrorCorrection = OCRTools.ErrorCorrectionTypes.None;
            this.OCR1.FontColor = System.Drawing.Color.Black;
            this.OCR1.FontColorContrast = 0;
            this.OCR1.Language = OCRTools.LanguageType.English;
            this.OCR1.MaximumBarHeight = 500;
            this.OCR1.MaximumBarWidth = 100;
            this.OCR1.MaximumCharacters = 5000;
            this.OCR1.MaximumHeight = 150;
            this.OCR1.MaximumSize = 10000;
            this.OCR1.MaximumWidth = 150;
            this.OCR1.MinimumBarHeight = 20;
            this.OCR1.MinimumBarWidth = 1;
            this.OCR1.MinimumConfidence = new decimal(new int[] {
            1,
            0,
            0,
            458752});
            this.OCR1.MinimumHeight = 1;
            this.OCR1.MinimumSize = 2;
            this.OCR1.MinimumSpace = 4;
            this.OCR1.MinimumWidth = 1;
            this.OCR1.RemoveHorizontal = 0;
            this.OCR1.RemoveVertical = 0;
            this.OCR1.ResizeBitmap = new decimal(new int[] {
            10,
            0,
            0,
            65536});
            this.OCR1.SectionHorizontalSpace = 1.5;
            this.OCR1.SectionVerticalSpace = 1.5;
            this.OCR1.StartStopCodesCharacter = "*";
            this.OCR1.StartStopCodesDisplay = false;
            this.OCR1.StartStopCodesRequired = false;
            this.OCR1.Statistics = false;
            this.OCR1.ThinCharacters = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 441);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(911, 22);
            this.statusStrip1.TabIndex = 19;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStatus
            // 
            this.toolStatus.Name = "toolStatus";
            this.toolStatus.Size = new System.Drawing.Size(59, 17);
            this.toolStatus.Text = "Loading...";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton3.Text = "Solo Comprimir";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(911, 463);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Correccion OCR";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picPreview)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView dgData;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button cmdOkLookup;
        private System.Windows.Forms.Button cmdErrLockup;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cmdLeer;
        private System.Windows.Forms.TextBox txtOk;
        private System.Windows.Forms.TextBox txtErr;
        private System.Windows.Forms.PictureBox picPreview;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton cmdSalir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton cmdMoverTifToScan;
        private System.Windows.Forms.ToolStripButton cmdEliminar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton cmdRotar;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton cmZoomMas;
        private System.Windows.Forms.ToolStripButton cmdZoomMenos;
        private System.Windows.Forms.ToolStripTextBox txtGradosRotacion;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton cmdProcesar;
        private System.Windows.Forms.ToolStripProgressBar progreso;
        private System.Windows.Forms.Button cmdScanLookUp;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtScan;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private OCRTools.OCR OCR1;
        private System.Windows.Forms.ToolStripComboBox cboOCRType;
        private System.Windows.Forms.ToolStripComboBox cboMejoras;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStatus;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
    }
}

