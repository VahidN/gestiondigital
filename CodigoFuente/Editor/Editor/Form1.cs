﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using AForge.Imaging.Filters;
using AForge.Imaging;
using System.Drawing.Imaging;
using System.Threading;
using CoreEngine.OCRCore;
using GestionDigitalCore.CoreObjects;
using ToolBox;


namespace Editor
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            picPreview.Height = splitContainer1.Panel2.Height*2;
            picPreview.Width = splitContainer1.Panel2.Width;
            OcultarCarpetas();
            OCR1.OCRType = OCRTools.OCRType.Barcode;
            OCR1.SetPictureBox(picPreview);
            OCR1.CustomerName = "Version5";
            OCR1.OrderID = "5108";
            OCR1.ProductName = "StandardBar";
            OCR1.RegistrationCodes = "4081-0097-0082-4945";
            //ocrdoc.ActivationKey = "2750-8441-7000-8746"; datos para compilar en PC FSO
            OCR1.ActivationKey = "6732-8470-8351-9979";
            OCR1.CalculateChecksum = true;
            OCR1.DisplayChecksum = false;
            cboOCRType.SelectedIndex = 1;
            cboMejoras.SelectedIndex = 1;
            MejoradorDeImagenes.SystemPath = Application.StartupPath;
        }

        
        private void button1_Click(object sender, EventArgs e)
        {
            CargarDatosCarpeta();
        }
        private bool IniciarCargaImagenes;
        private void CargarDatosCarpeta()
        {

            try
            {
                dgData.Rows.Clear();
                if (Directory.Exists(txtErr.Text))
                {
                    string[] Archivos = Directory.GetFiles(txtErr.Text);
                    IniciarCargaImagenes = false;
                    ShowStatus("Cargando Archivo");
                    foreach (string arc in Archivos)
                    {
                        try
                        {
                            if (FileTools.GetFileExtension(arc) == "tif" ||
                                FileTools.GetFileExtension(arc) == "tiff" ||
                                FileTools.GetFileExtension(arc) == "jpg" ||
                                FileTools.GetFileExtension(arc) == "jpeg")
                            {
                                string[] partesnombre = arc.Split('\\');
                                string Nombre = partesnombre[partesnombre.Length - 1];
                                partesnombre = Nombre.Split('.');
                                Nombre = partesnombre[0];
                                string Extension = partesnombre[1];
                                string[] reg = new string[3] { Nombre, arc, Extension };                                
                                dgData.Rows.Add(reg);
                                if (Extension == "tiff" || Extension == "tif")
                                {
                                    dgData.Rows[dgData.Rows.Count - 1].DefaultCellStyle.BackColor = Color.LightGray;
                                }
                            }
                        }
                        catch { }
                    }
                    Application.DoEvents();
                    IniciarCargaImagenes = true;
                    if (dgData.Rows.Count > 0)
                    {
                        LoadPictureFromSelectedFile(0);
                    }
                    ShowStatus("Inactivo");
                } 
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
			
        }


        Bitmap Buffer;
        bool LoadingBuffer=false;
        private void LoadPictureFromSelectedFile(int RowIndex)
        {
            
            if (!IniciarCargaImagenes)
                return;
            Cursor.Current = Cursors.WaitCursor;
            while (LoadingBuffer)
            {
                try
                {
                    Application.DoEvents();
                }
                catch (InvalidOperationException ex) { }
            }
            
            string filename = dgData.Rows[RowIndex].Cells[1].Value.ToString();
            ShowStatus("Cargando Imagen: " + filename);
            picPreview.Visible = false;
            OCR1.OCRType = OCRTools.OCRType.Barcode;
            OCR1.SetPictureBox(picPreview);
            Bitmap Image;
            if (BufferFilename != filename)
                Image = AForge.Imaging.Image.FromFile(filename);
            else{
                Image = Buffer;
            }
            OCR1.LoadPictureBox(Image);
            OCR1.PictureBox.ScaleImage(splitContainer1.Panel2.Width, splitContainer1.Panel2.Height);
            if (ZoomActual != 0)
            {
                OCR1.PictureBox.Zoom(ZoomActual);
            }

            picPreview.Visible = true;

            //Load buffer.
            if (RowIndex < dgData.Rows.Count-1)
            {
                BufferFilename = dgData.Rows[RowIndex + 1].Cells[1].Value.ToString();
                ThreadStart ts = new ThreadStart(LoadPicture);
                Thread t = new Thread(ts);
                t.Start();
            }
            Cursor.Current = Cursors.Default;
        }
        string BufferFilename = "";
        
        private void LoadPicture()
        {
            try
            {
                LoadingBuffer = true;
                ShowStatus("Cargando Buffer");
                if(File.Exists(BufferFilename))
                {
                    Buffer = AForge.Imaging.Image.FromFile(BufferFilename);
                }
                ShowStatus("Inactivo");
            }
            finally
            {
                LoadingBuffer = false;
            }

        }
        private void ShowStatus(string status)
        {
            CheckForIllegalCrossThreadCalls = false;
            toolStatus.Text = status;
            Application.DoEvents();
        }
        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {            
            VerificarValorIngresado(e.RowIndex);
        }

        private void VerificarValorIngresado(int RowIndex)
        {
            if (dgData.Rows[RowIndex].Cells[0].Value == null)
                dgData.Rows[RowIndex].Cells[0].Value = "";
            string valor = dgData.Rows[RowIndex].Cells[0].Value.ToString();
        
            if (valor.Length != 10 && valor.Length != 11)
            {
                dgData.Rows[RowIndex].Cells[0].Style.ForeColor = Color.Red;
            }
            else
            {
                dgData.Rows[RowIndex].Cells[0].Style.ForeColor = Color.Green;
            }
        }

        private void cmdProcesar_Click(object sender, EventArgs e)
        {
            string PathDestino = txtOk.Text;
            Cursor.Current = Cursors.WaitCursor;
            progreso.Minimum = 0;
            progreso.Maximum = dgData.Rows.Count;
            progreso.Value = 0;
            if (!Directory.Exists(txtOk.Text))
            {
                Cursor.Current = Cursors.Default;          
                MessageBox.Show("La Carpeta destino (Carpeta OK) Seleccionada no existe");
                return;
            }
            ShowStatus("Procesando Selección");
            Application.DoEvents();
            foreach (DataGridViewRow fila in dgData.Rows)
            {
                try
                {
                    string PathOrigen = fila.Cells[1].Value.ToString();
  
                    {
                        if (fila.Cells[0].Style.ForeColor == Color.Green)
                        {
                            string NuevoNombre;
                            if (fila.Cells[2].Value.ToString() == "tif")
                            {
                                NuevoNombre = PathDestino + "\\" + fila.Cells[0].Value.ToString() + ".jpg";
                                //Comprimir y Copiar.
                                Bitmap BM = AForge.Imaging.Image.FromFile(PathOrigen);

                                BM = AForge.Imaging.Image.Clone(BM, PixelFormat.Format32bppArgb);
   
                                CoreEngine.OCRCore.MejoradorDeImagenes.ComprimiryGuardar(BM,0, NuevoNombre);
                                BM.Dispose();                                
                            }
                            else
                            {
                                NuevoNombre = PathDestino + "\\" + fila.Cells[0].Value.ToString() + "." + fila.Cells[2].Value.ToString();
                                File.Copy(PathOrigen, NuevoNombre);
                            }
                            File.Delete(PathOrigen);
                        }
                    }
                }
                catch (Exception xe)
                {
                    string a = xe.Message;
                }
                finally {
                    progreso.Value++;
                    Application.DoEvents();
                }
            }
            ShowStatus("Inactivo");
            progreso.Value = 0;
            Cursor.Current = Cursors.Default;          
            CargarDatosCarpeta();
        }



        private void Form1_Load(object sender, EventArgs e)
        {
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (System.Diagnostics.Debugger.IsAttached)
            {
                txtErr.Text = @"C:\MiTrabajo\Andesmar\OCR_ERR";
                txtOk.Text = @"C:\MiTrabajo\Andesmar\OCR_OK";
                txtScan.Text = @"C:\MiTrabajo\Andesmar\SCAN";               
            }
            else
            {
                txtErr.Text = config.AppSettings.Settings["Origen"].Value;
                txtOk.Text = config.AppSettings.Settings["Destino"].Value;
                txtScan.Text = config.AppSettings.Settings["Scan"].Value;
            }
            CargarDatosCarpeta();
            dgData.Focus();
        }

        
        private void LoadErrorFolder_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = txtErr.Text;
            folderBrowserDialog1.ShowDialog();
            txtErr.Text = folderBrowserDialog1.SelectedPath;
            CargarDatosCarpeta();
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings["Origen"].Value = txtErr.Text;
            config.Save();
            ConfigurationManager.RefreshSection("appSettings");
            
        
        }

        private void LoadOkFolder_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = txtOk.Text;
            folderBrowserDialog1.ShowDialog();
            txtOk.Text = folderBrowserDialog1.SelectedPath;
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings["Destino"].Value = txtOk.Text;
            config.Save();
            ConfigurationManager.RefreshSection("appSettings");
        }


        int ZoomActual = 0;
        private void cmZoomMas_Click(object sender, EventArgs e)
        {
            ZoomActual += 80;
            OCR1.PictureBox.Zoom(80);
            //if (picPreview.Width <= 4000)
            //{
            //    picPreview.Width += 200;
            //    picPreview.Height += 200;
            //}
        }

        private void cmdZoomMenos_Click(object sender, EventArgs e)
        {
            ZoomActual -= 80;
            OCR1.PictureBox.Zoom(-80);
            //if (picPreview.Height > 300)
            //{
            //    picPreview.Width -= 200;
            //    picPreview.Height -= 200;
            //}
        }

        private void cmdRotar_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int angulo = 0;
                int.TryParse(txtGradosRotacion.Text, out angulo);
                if (angulo != 0)
                {
                    int RowIndex = dgData.SelectedCells[0].RowIndex;
                    string PathOrigen = dgData.Rows[RowIndex].Cells[1].Value.ToString();
                    //Bitmap BM = AForge.Imaging.Image.FromFile(PathOrigen);
                    Bitmap BM = AForge.Imaging.Image.Clone(new Bitmap(picPreview.Image), PixelFormat.Format24bppRgb);
                    BitmapData imageData = BM.LockBits(
                                new Rectangle(0, 0, BM.Width, BM.Height),
                                ImageLockMode.ReadWrite, BM.PixelFormat);

                    AForge.Imaging.Filters.RotateBilinear rot = new AForge.Imaging.Filters.RotateBilinear(angulo);
                    Bitmap Rotada = rot.Apply(imageData);
                    picPreview.Image = Rotada;
                    Rotada.Save(PathOrigen);
                  
                }
                else
                {
                    txtGradosRotacion.Text = "0";
                }
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error:" + ex.Message);
            }
            finally {
                Cursor.Current = Cursors.Default;                
            }
			
        }

        private void cmdMoverTifToScan_Click(object sender, EventArgs e)
        {
            if (dgData.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Va a volver la/las imagenes seleccionadas a la carpeta scan para su reproceso, esta seguro?", "Reprocesamiento de Imagenes", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    progreso.Minimum = 0;
                    progreso.Maximum = dgData.SelectedRows.Count;
                    progreso.Value = 0;

                    if(Directory.Exists(txtScan.Text))
                    {

                        foreach (DataGridViewRow fila in dgData.SelectedRows)
                        {
                            string filename =fila.Cells[1].Value.ToString();
                            if (ToolBox.FileTools.GetFileExtension(filename).ToLower().Contains("tif"))
                            {
                                File.Move(filename, txtScan.Text + "\\" + ToolBox.FileTools.getNombreUnico("r_"));
                                dgData.Rows.Remove(fila);
                                progreso.Value++;
                            }
                            Application.DoEvents();
                        }
                        progreso.Value = 0;
                    }else{
                        MessageBox.Show("La carpeta seleccionada como SCAN es INCORRECTA");
                    }
                }
            }
        }

        private void cmdScanLookUp_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = txtOk.Text;
            folderBrowserDialog1.ShowDialog();
            txtScan.Text = folderBrowserDialog1.SelectedPath;
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings["Scan"].Value = txtOk.Text;
            config.Save();
            ConfigurationManager.RefreshSection("appSettings");
        }

        private void txtScan_Leave(object sender, EventArgs e)
        {
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings["Scan"].Value = txtScan.Text;
            config.Save();
            ConfigurationManager.RefreshSection("appSettings");
        }

        private void txtOk_Leave(object sender, EventArgs e)
        {
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings["Destino"].Value = txtOk.Text;
            config.Save();
            ConfigurationManager.RefreshSection("appSettings");
        
        }

        private void txtErr_Leave(object sender, EventArgs e)
        {
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings["Origen"].Value = txtErr.Text;
            config.Save();
            ConfigurationManager.RefreshSection("appSettings");
        }

        private void cmdEliminar_Click(object sender, EventArgs e)
        {
            if (dgData.SelectedCells.Count > 0)
            {
                if (MessageBox.Show("Va a ELIMINAR la/las imagenes seleccionadas, este proceso no puede deshacerse, esta seguro?", "Eliminar Imagenes", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    progreso.Minimum = 0;
                    progreso.Maximum = dgData.SelectedCells.Count;
                    progreso.Value = 0;

                    picPreview.Image = null;                   
                    foreach (DataGridViewCell celda in dgData.SelectedCells)
                    {
                        File.Delete(dgData.Rows[celda.RowIndex].Cells[1].Value.ToString());
                        dgData.Rows.Remove(dgData.Rows[celda.RowIndex]);
                        progreso.Value++;
                        Application.DoEvents();
                    }
                    progreso.Value = 0;
    
                }
            }
        }

        private void cmdSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (panel1.Height > 0)
            {
                OcultarCarpetas();
            }
            else
            {
                MostrarCarpetas();
            }
        }

        private void MostrarCarpetas()
        {
            panel1.Height = 80;
            splitContainer1.Top = 27 + 80;
            splitContainer1.Height = this.Height - (67 + 80);
        }

        private void OcultarCarpetas()
        {
            panel1.Height = 0;
            splitContainer1.Top = 27;
            splitContainer1.Height = this.Height - 67;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            OCR1.PictureBox.ScaleImage(splitContainer1.Panel2.Width, splitContainer1.Panel2.Height);
            ZoomActual = 0;
            //picPreview.Height = splitContainer1.Panel2.Height;
            //picPreview.Width = splitContainer1.Panel2.Width;
        }
        //bool Creatingbox = false;
        //int startX = 0;
        //int startY = 0;
        //int finalX = 0;
        //int finalY = 0;
        //private void picPreview_MouseMove(object sender, MouseEventArgs e)
        //{
        //    if (e.Button == MouseButtons.Left)
        //    {
        //        if (!Creatingbox)
        //        {
        //            Creatingbox = true;
        //            startX = e.X;
        //            startY = e.Y;                  
        //        }
        //        else
        //        {
        //            //ya esta visible

        //        }
        //        finalY = startY;
        //        finalX = startX;
              

        //    }
        //}

        private void picPreview_MouseUp(object sender, MouseEventArgs e)
        {

            Cursor.Current = Cursors.WaitCursor;
            switch (cboOCRType.Text)
            {
                case "Barras":
                    OCRBarras();
                    break;
                case "Texto":
                    ProcesarOCRTexto();
                    break;
                case "Ambos":
                    OCRBarras();
                    int selectedRow = dgData.SelectedCells[0].RowIndex;
                    if (dgData.Rows[selectedRow].Cells[0].Style.ForeColor != Color.Green)
                        ProcesarOCRTexto();
                    break;

            }
            //Creatingbox = false; 
            Cursor.Current = Cursors.Default;
			

        }
       private Thread myThread;
        private void OCRBarras()
        {
         
         
            // Capture the user selected region of the PictureBox that the user selected with their mouse
            Campo c = new Campo();
            c.MejorarImagen = true;
            c.TipoPreprocesamiento = 2;
            if (cboMejoras.Text == "Preprocesar Imagen")
            {
                Bitmap Intermedio = MejoradorDeImagenes.ProcesarSubImagen(c, OCR1.PictureBox.Copy());                              
                OCR1.BitmapImage = Intermedio;
            }
            else {
                OCR1.BitmapImage = OCR1.PictureBox.Copy();
            }
            Application.DoEvents();
            myThread = OCR1.Process_Start();


            // Keep processing until the thread is completed
          while (myThread.IsAlive) { }
          if (OCR1.ThreadError)
              System.Windows.Forms.MessageBox.Show("Error: " + OCR1.ThreadErrorMessage);

          else
          {
              if (dgData.SelectedCells.Count > 0 && OCR1.Text.Trim()!="")
              {
                  dgData.SelectedCells[0].Value = OCR1.Text;
                  int selectedRow = dgData.SelectedCells[0].RowIndex;
                  VerificarValorIngresado(selectedRow);
              }
          }
        }
        private MODI.Document md;
        private void ProcesarOCRTexto()
        {
            try
            {                
                string filename = MejoradorDeImagenes.SystemPath + "\\" + ToolBox.FileTools.getNombreUnico();
                string strText = "";
                OCR1.BitmapImage = OCR1.PictureBox.Copy();

                
                OCR1.BitmapImage.Save(filename, System.Drawing.Imaging.ImageFormat.Tiff);

                Bitmap ImagenATratar = AForge.Imaging.Image.FromFile(filename);
                if (cboMejoras.Text == "Preprocesar Imagen")
                {
                    ImagenATratar = MejoradorDeImagenes.PreProcesarTextoClaro(ImagenATratar, new Campo());                    
                }
                ImagenATratar.Save(filename, System.Drawing.Imaging.ImageFormat.Tiff);
                #region Prepara y Ejecuta el OCR de texto
                md = new MODI.Document();
                md.Create(filename);

                // The Create method grabs the picture from
                //   disk snd prepares for OCR.

                // Do the OCR.
                try
                {
                    md.OCR(MODI.MiLANGUAGES.miLANG_SPANISH, true, true);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("OCR"))
                    {
                        return;
                    }
                }
                md.Save();

                #endregion

                #region Obtiene los Textos resultantes del OCR
                // Get the first (and only image)
                MODI.Image image = (MODI.Image)md.Images[0];

                // Get the layout.
                MODI.Layout layout = image.Layout;

                string TextoDetectado = "";
                // Loop through the words.
                for (int j = 0; j < layout.Words.Count; j++)
                {
                    // Get this word.
                    MODI.Word word = (MODI.Word)layout.Words[j];
                    // Add a blank space to separate words.
                    if (strText.Length > 0)
                    {
                        strText += " ";
                    }
                    // Add the word.
                     TextoDetectado += word.Text;
                   
                }

                if (TextoDetectado.Trim() != "")
                {
                    dgData.SelectedCells[0].Value = TextoDetectado;
                    int selectedRow = dgData.SelectedCells[0].RowIndex;
                    VerificarValorIngresado(selectedRow);
                }
                // Close the MODI.Document object.
                md.Close(false);
                #endregion
                
                File.Delete(filename);
            

            }
            catch (Exception ex)
            {
                string a = ex.Message;
            }
            finally
            {
                try
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(md);
                    md = null;
                    // Force garbage collection so image file is closed properly
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch { }
            }
        }

        private void dgData_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            LoadPictureFromSelectedFile(e.RowIndex);
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            string PathOrigen = txtErr.Text;
            if (dgData.SelectedRows.Count > 0)
            {
                if (MessageBox.Show("Va a comprimir la/las imagenes seleccionadas, no podran ser reprocesadas, esta seguro?", "Reprocesamiento de Imagenes", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                {
                    progreso.Minimum = 0;
                    progreso.Maximum = dgData.SelectedRows.Count;
                    progreso.Value = 0;

                    if (Directory.Exists(txtScan.Text))
                    {
                        
                        foreach (DataGridViewRow fila in dgData.SelectedRows)
                        {
                            string filename = fila.Cells[1].Value.ToString();
                            if (ToolBox.FileTools.GetFileExtension(filename).ToLower().Contains("tif"))
                            {
                                string NuevoNombre;
                                if (File.Exists(filename) && fila.Cells[2].Value.ToString() == "tif")
                                {
                                    NuevoNombre = PathOrigen + "\\" + fila.Cells[0].Value.ToString() + ".jpg";
                                    ShowStatus("Comprimiendo Imagen: " + filename);
                                    Application.DoEvents();
                                    //Comprimir y Copiar.
                                    Bitmap BM = AForge.Imaging.Image.FromFile(filename);

                                    BM = AForge.Imaging.Image.Clone(BM, PixelFormat.Format32bppArgb);

                                    CoreEngine.OCRCore.MejoradorDeImagenes.ComprimiryGuardar(BM, 0, NuevoNombre);
                                    BM.Dispose();
                                    fila.Cells[1].Value = NuevoNombre;
                                }
                                fila.DefaultCellStyle.BackColor = Color.White;
                                File.Delete(filename);


                                progreso.Value++;
                            }
                            Application.DoEvents();
                        }
                        ShowStatus("Inactivo");
                        progreso.Value = 0;
                    }
                    else
                    {
                        MessageBox.Show("La carpeta seleccionada como SCAN es INCORRECTA");
                    }
                }
            }
        }

 



        
    }
}


