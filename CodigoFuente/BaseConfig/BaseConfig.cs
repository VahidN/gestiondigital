﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.IO;

namespace BaseConfig
{
    public class AppConfig
    {
        public AppConfig()
        { 
            //Leer del App.config.

            XmlDocument x = new XmlDocument();
            string path = System.IO.Path.GetDirectoryName(typeof(AppConfig).Assembly.Location);
            if (!File.Exists(path + "\\Configuracion.xml"))
            {
                string text = "<?xml version=\"1.0\" encoding=\"utf-8\" ?> " +
                                "<configuration> " +
                                "  <appSettings> " +
                                "    <SQLType>SQLServerCE35</SQLType> " +
                                "    <Coneccion>DataSource='" + path + "\\DB35.sdf'; Password=123456</Coneccion> " +
                                "    <ShowSQL>false</ShowSQL> " +
                                "  </appSettings> " +
                                "</configuration> ";

                StreamWriter sw = File.CreateText(path + "\\Configuracion.xml");
                sw.Write(text);
                sw.Close();
            }
            x.Load(path +"\\Configuracion.xml");
            _ConectionString = x.GetElementsByTagName("Coneccion")[0].InnerText;
            _ShowSQL = x.GetElementsByTagName("ShowSQL")[0].InnerText.ToUpper();
            SQLType = x.GetElementsByTagName("SQLType")[0].InnerText.ToUpper();

        }
        private string _SQLType;

        public string SQLType
        {
            get { return _SQLType; }
            set { _SQLType = value; }
        }
        private   string _ConectionString;

        public   string ConectionString
        {
            get { return _ConectionString; }
            set { _ConectionString = value; }
        }
        private string _ShowSQL;

        public string ShowSQL
        {
            get { return _ShowSQL; }
            set { _ShowSQL = value; }
        }
    }
}
