﻿using System;
using System.Collections.Generic;
using System.Text;
using GestionDigitalCore.CoreObjects;

using System.Drawing;
using OCRTools;
using System.Threading;
using System.IO;
using System.Drawing.Imaging;

namespace CoreEngine.OCRCore
{   
    public class OCRCore
    {
        private OCR ocrdoc;
        private int LogitudDelTextoBuscado = 0;
        private Thread myThread;
        private MODI.Document md;

        public OCRCore(string _SystemaPath)
        {
            MejoradorDeImagenes.SystemPath = _SystemaPath;
        }

        public string ProcesarImagen(Bitmap Imagen,  Campo campo, Archivo arch)
        {
            LimpiarOCR(); 
            ocrdoc.BitmapImage = Imagen;
            try
            {

                string strText = "";
                SetearArea(campo);

                //Verifico si el campo a leer es codigo de barra o un texto
                if (campo.TipoCampo == eTipoCampo.CodigoBarra)
                {
                    FSO_NH.log4Net.FSOLog4Net.LogDebug("Inicio Proceso Codigo de Barra");
                    strText = ProcesarCodigoDeBarras(campo);
                    FSO_NH.log4Net.FSOLog4Net.LogDebug("Fin Proceso Codigo de Barra");
                }
                else
                {
                    FSO_NH.log4Net.FSOLog4Net.LogDebug("Inicio Proceso Texto");
                    ocrdoc.BitmapImage = MejoradorDeImagenes.ProcesarSubImagen(campo, ocrdoc.BitmapImage);
                    strText = ProcesarOCRTexto(campo);
                    FSO_NH.log4Net.FSOLog4Net.LogDebug("Fin Proceso Texto");
                }
                strText = ProcesarValorLeido(strText, campo);
                return strText;
            }
            catch (Exception ex)
            {
                FSO_NH.log4Net.FSOLog4Net.LogDebug("Error al procesar imagen OCR: " + ex.Message);
                return "Error: " + ex.Message;
            }
            finally {
                LimpiarOCR();
            }
        }
        private void SetearArea(Campo campo)
        {

            #region Setear Area
            FSO_NH.log4Net.FSOLog4Net.LogDebug("Iniciar Seteo de Area a Procesar");
            //Verifico si hay que scannear un area o todo el documento
            if (campo.Alto == 0)
            {
                bool lResult = ocrdoc.SetRegion(0,
                          0,
                          ocrdoc.BitmapImage.Width,
                          ocrdoc.BitmapImage.Height,
                          0);
            }
            else
            {
                bool lResult = ocrdoc.SetRegion(campo.XInicial,
                          campo.YInicial,
                          campo.Ancho,
                          campo.Alto,
                          0);
            }
            FSO_NH.log4Net.FSOLog4Net.LogDebug("Fin Seteo de Area a Procesar");


            #endregion
        }
        private string ProcesarOCRTexto(Campo campo)
        {
            try
            {
                string filename = MejoradorDeImagenes.SystemPath + "\\" + ToolBox.FileTools.getNombreUnico();                
                string strText = "";
                ocrdoc.BitmapImage.Save(filename, System.Drawing.Imaging.ImageFormat.Tiff);

                #region Prepara y Ejecuta el OCR de texto
                    md = new MODI.Document();
                    md.Create(filename);

                    // The Create method grabs the picture from
                    //   disk snd prepares for OCR.
                    // Do the OCR.
                    try
                    {
                        md.OCR(MODI.MiLANGUAGES.miLANG_SPANISH, true, true);
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("OCR"))
                        {
                            return "";
                        }
                    }
                    md.Save();

                #endregion

                #region Obtiene los Textos resultantes del OCR
                    // Get the first (and only image)
                    MODI.Image image = (MODI.Image)md.Images[0];

                    // Get the layout.
                    MODI.Layout layout = image.Layout;

                    // Loop through the words.
                    for (int j = 0; j < layout.Words.Count; j++)
                    {
                        // Get this word.
                        MODI.Word word = (MODI.Word)layout.Words[j];
                        // Add a blank space to separate words.
                        if (strText.Length > 0)
                        {
                            strText += " ";
                        }
                        // Add the word.
                        strText += ProcesarValorLeido(word.Text, campo);
                        if (strText.Length == LogitudDelTextoBuscado && LogitudDelTextoBuscado != 0)
                            break;
                    }
                    // Close the MODI.Document object.
                    md.Close(false); 
                #endregion

                md.Close(false);
                try
                {
                    File.Delete(filename);
                }
                catch { }

                return strText;

            }
            catch (Exception ex)
            {
                return "";
            }
            finally
            {
                try
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(md);
                    md = null;
                    // Force garbage collection so image file is closed properly
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                }
                catch { }
            }
        }
        private string ProcesarCodigoDeBarras(Campo campo)
        {
            string strText = "";
            //Corto la imagen en 4 franjas y proceso cada una.
            int CantidadDeFranjas =  4;
            int AltoSubImagen = ocrdoc.BitmapImage.Height / CantidadDeFranjas;
            int CantidadIteraciones = 0;
            ocrdoc.OCRType = OCRTools.OCRType.Barcode;           
            do
            {

                if (((int)(CantidadDeFranjas / 2) - 1) != CantidadIteraciones) //Esto se hace para no repetir el procesamiento de la franja del medio, es decir la inicial.
                {
                    #region Procesar Imagen                  
                    if (CantidadIteraciones == 0)
                    {
                        //Primero pruebo con el segmento del medio, que es donde generalmente se encuentra el codigo.
                        int Py = ((int)(CantidadDeFranjas / 2) - 1) * AltoSubImagen;
                        ocrdoc.SetRegion(0,Py , ocrdoc.BitmapImage.Width, AltoSubImagen);
                    }
                    else
                    {
                        ocrdoc.SetRegion(0, (CantidadIteraciones - 1) * AltoSubImagen, ocrdoc.BitmapImage.Width, AltoSubImagen);
                    }                   
                    ocrdoc.BitmapImage = MejoradorDeImagenes.ProcesarSubImagen(campo, ocrdoc.BitmapImage);                   
                    myThread = ocrdoc.Process_Start();
                    while (myThread.IsAlive)
                    {

                    }
                    #endregion


                    #region Obtener Resultados
                    if (ocrdoc.ThreadError)
                    {
                        strText = "";
                    }
                    else
                    {
                        strText = ProcesarValorLeido(ocrdoc.Text, campo);
                    }
                    CantidadIteraciones += 1;
                    #endregion
                }
                else {
                    CantidadIteraciones += 1;
                }
            } while (CantidadIteraciones < CantidadDeFranjas +1&& strText=="");



           //ImagenPrimaria.Dispose();
           return strText;

        }
        private string ProcesarValorLeido(string valor, Campo CampoActual)
        {            
            valor = valor.Trim();
            string result = "";
            if (CampoActual.TipoDato != "O")
            {
                char[] caracteres = new char[valor.Length];
                valor.CopyTo(0, caracteres, 0, valor.Length);
                foreach (char c in caracteres)
                {
                    try
                    {
                        int.Parse(c.ToString());
                        result += c.ToString();
                    }
                    catch
                    {
                    }
                }
            }
            else
                result = valor;

            if (CampoActual.Longitud > 0)
            {
                LogitudDelTextoBuscado = CampoActual.Longitud;

                if (result.Length > LogitudDelTextoBuscado + 1)
                    return "";
                else
                    if (result.Length == LogitudDelTextoBuscado + 1 & !result.Contains("11111111111"))
                        return result;
                    else if (result.Length == LogitudDelTextoBuscado & !result.Contains("1111111111"))
                        return result;
                    else
                        return "";
            }
            else
                return result;
        }
        private void LimpiarOCR()
        {
            if (ocrdoc != null)
            {
                try
                {
                    ocrdoc.BitmapImage.Dispose();
                    ocrdoc.BitmapImage = null;
                }
                catch { }
                try
                {
                    ocrdoc.BitmapImageProcessed.Dispose();
                }
                catch { }
                try
                {
                    ocrdoc.Dispose();
                }
                catch { }
            }
            ocrdoc = new OCRTools.OCR();
            RegistrarOCR();
        }
        private void RegistrarOCR()
        {
            ocrdoc.CustomerName = "Version5";
            ocrdoc.OrderID = "5108";
            ocrdoc.ProductName = "StandardBar";
            ocrdoc.RegistrationCodes = "4081-0097-0082-4945";
            //ocrdoc.ActivationKey = "2750-8441-7000-8746"; datos para compilar en PC FSO
            ocrdoc.ActivationKey = "6732-8470-8351-9979";
            ocrdoc.CalculateChecksum = true;
            ocrdoc.DisplayChecksum = false;
        }
    }
}
