﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using GestionDigitalCore.CoreObjects;
using AForge.Imaging.Filters;
using System.Drawing.Imaging;
using AForge.Imaging;

namespace CoreEngine.OCRCore
{
    public static class MejoradorDeImagenes
    {
        public static string SystemPath;

        public static Bitmap ProcesarSubImagen(Campo campo, Bitmap BM)
        {
            if (campo.MejorarImagen)
            {
                switch (campo.TipoPreprocesamiento)
                {
                    case 1: BM = MejoradorDeImagenes.PreProcesarTextoClaro(BM, campo); break;
                    case 2: BM = MejoradorDeImagenes.PreProcesarBarrasA4TextoNormal(BM, campo); break;
                    case 3: BM = MejoradorDeImagenes.PreProcesarBarrasOscuros(BM, campo); break;
                    case 4: BM = MejoradorDeImagenes.PreProcesarTextoOscuro(BM, campo); break;
                    case 5: BM = MejoradorDeImagenes.PreProcesarBarrasA5TextoNormal(BM, campo); break;
                    case 6: BM = MejoradorDeImagenes.PreProcesarTextoA5TextoNormal(BM, campo); break;
                }
            }
           return BM;
        }
        public static Bitmap PreProcesarBarrasOscuros(Bitmap BM, Campo campo)
        {
            BitmapData imageData = BM.LockBits(
                               new Rectangle(0, 0, BM.Width, BM.Height),
                               ImageLockMode.ReadWrite, BM.PixelFormat);
            AForge.Imaging.UnmanagedImage UIMAGE = new AForge.Imaging.UnmanagedImage(imageData);

            AForge.Imaging.Filters.GaussianBlur Conv1 = new GaussianBlur(.75, 5);
            ResizeNearestNeighbor R = new ResizeNearestNeighbor(BM.Width, (int)(BM.Height * 1.5M));


            AForge.Imaging.UnmanagedImage Paso2 = R.Apply(Conv1.Apply(UIMAGE));
            
            Bitmap Paso3 = BinarizarYVerticalizar(Paso2, .45M,600);
            
            return Paso3;

        }
        public static Bitmap PreProcesarBarrasA4TextoNormal(Bitmap BM, Campo campo)
        {
            try
            {
                BitmapData imageData = BM.LockBits(
                                new Rectangle(0, 0, BM.Width, BM.Height),
                                ImageLockMode.ReadWrite, BM.PixelFormat);
                AForge.Imaging.UnmanagedImage UIMAGE = new AForge.Imaging.UnmanagedImage(imageData);
                AForge.Imaging.Filters.GaussianBlur Conv1 = new GaussianBlur(.75, 5);
                ResizeNearestNeighbor R = new ResizeNearestNeighbor(BM.Width, (int) (BM.Height * 1.5M));

                //AForge.Imaging.UnmanagedImage Paso2 = R.Apply(Conv1.Apply(UIMAGE));
                AForge.Imaging.UnmanagedImage Paso2 = R.Apply(UIMAGE);
                //Paso2.ToManagedImage().Save("C:\\1.bmp", ImageFormat.Bmp);
                //Bitmap Paso3 = BinarizarYVerticalizar(Paso2, .75M, 200);
                Bitmap Paso3 = BinarizarYVerticalizar(Paso2, .35M, 200);
                //Paso3.Save("c:\\2.bmp", ImageFormat.Bmp);
                return Paso3;
            }
            catch (Exception) 
            {
                
                throw;
            }
        }
        
        public static Bitmap PreProcesarBarrasA5TextoNormal(Bitmap BM, Campo campo)
        {
            try
            {
                BitmapData imageData = BM.LockBits(
                   new Rectangle(0, 0, BM.Width, BM.Height),
                   ImageLockMode.ReadWrite, BM.PixelFormat);
                AForge.Imaging.UnmanagedImage UIMAGE = new AForge.Imaging.UnmanagedImage(imageData);

                AForge.Imaging.Filters.GaussianBlur Conv1 = new GaussianBlur(.75, 5);
                ResizeNearestNeighbor R = new ResizeNearestNeighbor(UIMAGE.Width, (int)(UIMAGE.Height * 1.5M));

                AForge.Imaging.UnmanagedImage Paso2 = R.Apply(Conv1.Apply(UIMAGE));
                Bitmap Paso3 = BinarizarYVerticalizar(Paso2, .97M, 220);

                return Paso3;
            }
            catch (Exception e)
            {

                throw;
            }
        }

        //private static BitmapData LimpiarPixelsSueltos(BitmapData bmd)
        //{
        //    const int LimiteIdentificacionColorNegro = 1;

            
        //    //BitmapData bmd = BM.LockBits(new Rectangle(0, 0, BM.Width, BM.Height), System.Drawing.Imaging.ImageLockMode.ReadWrite, BM.PixelFormat);
        //    byte R; byte G; byte B;

        //    if (bmd.PixelFormat == PixelFormat.Format24bppRgb || bmd.PixelFormat == PixelFormat.Format32bppArgb)
        //    {
        //        #region 24BBP RGB

        //        int BytesPorPixel = 4;
        //        int r = 0; int g =1; int b = 2;

        //        unsafe
        //        {

        //            for (int y = 0; y < bmd.Height-3; y+=3)
        //            {
        //                byte* row1 = (byte*)bmd.Scan0 + ((y + 0) * bmd.Stride);
        //                byte* row2 = (byte*)bmd.Scan0 + ((y + 1) * bmd.Stride);
        //                byte* row3 = (byte*)bmd.Scan0 + ((y + 2) * bmd.Stride);
        //                decimal ValoresNegros = 0;
        //                for (int x = 0; x < bmd.Width-3; x+=3)
        //                {
        //                    for (int xoffset = 0; xoffset <= 2; xoffset++)
        //                    {
        //                        R = row1[(x+xoffset) * BytesPorPixel+r];
        //                        G = row1[(x + xoffset) * BytesPorPixel +g];
        //                        B = row1[(x + xoffset) * BytesPorPixel + b];
        //                        if (R + G + B < LimiteIdentificacionColorNegro)
        //                        {
        //                            ValoresNegros++;
        //                        }
        //                        R = row2[(x + xoffset) * BytesPorPixel+r];
        //                        G = row2[(x + xoffset) * BytesPorPixel + g];
        //                        B = row2[(x + xoffset) * BytesPorPixel + b];
        //                        if (R + G + B < LimiteIdentificacionColorNegro)
        //                        {
        //                            ValoresNegros++;
        //                        }
        //                        R = row3[(x + xoffset) * BytesPorPixel+r];
        //                        G = row3[(x + xoffset) * BytesPorPixel + g];
        //                        B = row3[(x + xoffset) * BytesPorPixel + b];
        //                        if (R + G + B < LimiteIdentificacionColorNegro)
        //                        {
        //                            ValoresNegros++;
        //                        }
        //                    }
        //                    //--- CAMBIAR COLOR
        //                    byte Color = (byte)(ValoresNegros > 3 ? 0 : 255);
        //                    //Posicion Fila 1
        //                    for (int xoffset = 0; xoffset <= 2; xoffset++)
        //                    {
        //                        row1[(x + xoffset) * BytesPorPixel + r] = Color;
        //                        row1[(x + xoffset) * BytesPorPixel + g] = Color;
        //                        row1[(x + xoffset) * BytesPorPixel + b] = Color;//3-Azul

        //                        row2[(x + xoffset) * BytesPorPixel + r] = Color;
        //                        row2[(x + xoffset) * BytesPorPixel + g] = Color;
        //                        row2[(x + xoffset) * BytesPorPixel + b] = Color;

        //                        row3[(x + xoffset) * BytesPorPixel + r] = Color;
        //                        row3[(x + xoffset) * BytesPorPixel + g] = Color;
        //                        row3[(x + xoffset) * BytesPorPixel + b] = Color;
        //                    }
        //                }
        //            }

        //        }
        //        #endregion
        //    }
        //    else
        //    {
        //        //#region 8BBP Indexed
        //        //Color[] Paleta;
        //        //Paleta = new Color[BM.Palette.Entries.Length];
        //        //BM.Palette.Entries.CopyTo(Paleta, 0);

        //        //unsafe
        //        //{

        //        //    for (int y = 0; y < bmd.Height-3; y=y+3)
        //        //    {
        //        //        byte* row1 = (byte*)bmd.Scan0 + (y * bmd.Stride);
        //        //        byte* row2 = (byte*)bmd.Scan0 + ((y+1) * bmd.Stride);
        //        //        byte* row3 = (byte*)bmd.Scan0 + ((y+2) * bmd.Stride);
        //        //        decimal ValoresNegros = 0;
        //        //        for (int x = 0; x < bmd.Width-3; x=x+3)
        //        //        {
        //        //            //Posicion Fila 1
        //        //            for (int xoffset = 0; xoffset <= 2; xoffset++)
        //        //            {
        //        //                R = Paleta[row1[x + xoffset]].R;
        //        //                G = Paleta[row1[x + xoffset]].G;
        //        //                B = Paleta[row1[x + xoffset]].B;
        //        //                if (R + G + B < LimiteIdentificacionColorNegro)
        //        //                {
        //        //                    ValoresNegros++;
        //        //                }
        //        //                R = Paleta[row2[x + xoffset]].R;
        //        //                G = Paleta[row2[x + xoffset]].G;
        //        //                B = Paleta[row2[x + xoffset]].B;
        //        //                if (R + G + B < LimiteIdentificacionColorNegro)
        //        //                {
        //        //                    ValoresNegros++;
        //        //                }
        //        //                R = Paleta[row3[x + xoffset]].R;
        //        //                G = Paleta[row3[x + xoffset]].G;
        //        //                B = Paleta[row3[x + xoffset]].B;
        //        //                if (R + G + B < LimiteIdentificacionColorNegro)
        //        //                {
        //        //                    ValoresNegros++;
        //        //                }
        //        //            }
        //        //            //--- CAMBIAR COLOR
        //        //            byte Color = (byte)(ValoresNegros > 3 ? 0 : 1);
        //        //                                        //Posicion Fila 1
        //        //            for (int xoffset = 0; xoffset <= 2; xoffset++)
        //        //            {
        //        //                row1[x + xoffset] = Color;
        //        //                row2[x + xoffset] = Color;
        //        //                row3[x + xoffset] = Color;
        //        //            }

        //        //        }
                       

        //        //    }

        //        //}
                
        //        //#endregion
        //    }
        //     //BM.UnlockBits(bmd);
        //     return bmd;
        //}
        private static Bitmap BinarizarYVerticalizar(AForge.Imaging.UnmanagedImage bmd, decimal PorcentajeDeUnosNecesariosEnColumnas, int LimiteIdentificacionColorNegro)
        {
            //const int LimiteIdentificacionColorNegro = 600;           
            int LimiteSeteoColorNegro = (int)(bmd.Height * PorcentajeDeUnosNecesariosEnColumnas);
            decimal[] ValoresNegros = new decimal[bmd.Width];
            //BitmapData bmd = BM.LockBits(new Rectangle(0, 0, BM.Width, BM.Height), System.Drawing.Imaging.ImageLockMode.ReadWrite, BM.PixelFormat);

            //bmd.ToManagedImage().Save("c:\\p1.bmp",ImageFormat.Bmp);
            int OffsetR=0; 
            int OffsetG=1; 
            int OffsetB=2;
            byte R; byte G; byte B;
            if (bmd.PixelFormat == PixelFormat.Format32bppArgb)
            {
                #region 32BBP RGB

                int BytesPorPixel = 4;

                unsafe
                {

                    for (int y = 0; y < bmd.Height; y++)
                    {
                        byte* row = (byte*)bmd.ImageData + (y * bmd.Stride);
                        for (int x = 0; x < bmd.Width; x++)
                        {
                            R = row[x * BytesPorPixel + OffsetR];
                            G = row[x * BytesPorPixel + OffsetG];
                            B = row[x * BytesPorPixel + OffsetB];
                            if ( (R + G + B)/3 < LimiteIdentificacionColorNegro)
                            {
                                ValoresNegros[x]++;
                                if(x<bmd.Width-1)
                                    ValoresNegros[x + 1] += .2M;
                                if (x >0)
                                    ValoresNegros[x - 1] += .2M;
                            }
                        }
                    }

                    for (int y = 0; y < bmd.Height; y++)
                    {
                        byte* row = (byte*)bmd.ImageData + (y * bmd.Stride);
                        for (int x = 0; x < bmd.Width; x++)
                        {
                            {
                                if (ValoresNegros[x] > LimiteSeteoColorNegro)
                                {
                                    row[x * BytesPorPixel + OffsetR] = 0;
                                    row[x * BytesPorPixel + OffsetG] = 0;
                                    row[x * BytesPorPixel + OffsetB] = 0;
                           

                                }
                                else
                                {
                                    row[x * BytesPorPixel + OffsetR] = 255;
                                    row[x * BytesPorPixel + OffsetG] = 255;
                                    row[x * BytesPorPixel + OffsetB] = 255;                           
                                }
                            }
                        }
                    }
                }
                //BM.UnlockBits(bmd);
                #endregion
            }
            else
            {
                #region 8BBP Indexed
                //Color[] Paleta;
                //Paleta = new Color[BM.Palette.Entries.Length];
                //BM.Palette.Entries.CopyTo(Paleta, 0);
                
                //unsafe
                //{

                //    for (int y = 0; y < bmd.Height; y++)
                //    {
                //        byte* row = (byte*)bmd.Scan0 + (y * bmd.Stride);
                //        for (int x = 0; x < bmd.Width; x++)
                //        {

                //            R = Paleta[row[x]].R;
                //            G = Paleta[row[x]].G;
                //            B = Paleta[row[x]].B;
                //            //System.Diagnostics.Debug.WriteLine(x.ToString() + ":" + y.ToString() + " = R:" + R.ToString() + ", G:" + G.ToString() + ", B:" + B.ToString());
                //            if ((R + G + B)/3 < LimiteIdentificacionColorNegro)
                //            {
                //                ValoresNegros[x]++;
                //                if (x < bmd.Width - 1)
                //                    ValoresNegros[x + 1] += .2M;
                //                if (x > 0)
                //                    ValoresNegros[x - 1] += .2M;
                           
                //            }
                //        }
                //    }

                //    for (int y = 0; y < bmd.Height; y++)
                //    {
                //        byte* row = (byte*)bmd.Scan0 + (y * bmd.Stride);
                //        for (int x = 0; x < bmd.Width; x++)
                //        {
                //            {
                //                if (ValoresNegros[x] > LimiteSeteoColorNegro)
                //                {
                //                    row[x] = 0;

                //                }
                //                else
                //                {
                //                    row[x] = 255;
                //                }
                //            }
                //        }
                //    }
                //}
                //BM.UnlockBits(bmd); 
                #endregion
            }
            //bmd.ToManagedImage().Save("c:\\p2.bmp", ImageFormat.Bmp);
            return bmd.ToManagedImage();
        }      
        public static Bitmap PreProcesarTextoClaro(Bitmap image, Campo campo)
        {
            // set wait cursor
            Bitmap Mejorada = (Bitmap)image.Clone();
            if (!campo.EsIdentificador)
            {
                BitmapData imageData = image.LockBits(
                                      new Rectangle(0, 0, image.Width, image.Height),
                                      ImageLockMode.ReadWrite, image.PixelFormat);
                AForge.Imaging.UnmanagedImage UIMAGE = new AForge.Imaging.UnmanagedImage(imageData);                
                AForge.Imaging.Filters.GaussianBlur G = new GaussianBlur(1.4, 5);
                AForge.Imaging.Filters.Threshold T1 = new Threshold(128);
                Bitmap bm = G.Apply(UIMAGE).ToManagedImage();
                Bitmap UI = GrayscaleY.CommonAlgorithms.Y.Apply(bm);
                Mejorada = G.Apply(T1.Apply(UI));
            }

            return Mejorada;

        }
        public static Bitmap PreProcesarTextoOscuro(Bitmap image, Campo campo)
        {
            // set wait cursor
            Bitmap Mejorada = (Bitmap)image.Clone();
            if (!campo.EsIdentificador)
            {
                BitmapData imageData = image.LockBits(
                      new Rectangle(0, 0, image.Width, image.Height),
                      ImageLockMode.ReadWrite, image.PixelFormat);
                AForge.Imaging.UnmanagedImage UIMAGE = new AForge.Imaging.UnmanagedImage(imageData);  

                AForge.Imaging.Filters.GaussianBlur G = new GaussianBlur(1.4, 5);
                AForge.Imaging.Filters.Threshold T1 = new Threshold(128);
                Mejorada = G.Apply(T1.Apply(G.Apply(T1.Apply(G.Apply(UIMAGE))))).ToManagedImage();
            }

            return Mejorada;

        }
        public static Bitmap Rotar(Bitmap ImagenBase, int angulo)
        {
            if (ImagenBase != null)
            {
                if (angulo == 0)
                {
                    return ImagenBase;
                }
                else
                {
                    Bitmap Rotada = new Bitmap(Rotator.RotateImage(angulo, ImagenBase));
                    return Rotada;
                    //AForge.Imaging.Filters.RotateBilinear rot = new AForge.Imaging.Filters.RotateBilinear(angulo);
                    //return rot.Apply(ImagenBase);
                }
            }
            else {
                return null;
            }
        }

        public static Bitmap PreProcesarTextoA5TextoNormal(Bitmap image, Campo campo)
        {
            // set wait cursor
            Bitmap Mejorada = (Bitmap)image.Clone();
            if (!campo.EsIdentificador)
            {
                BitmapData imageData = image.LockBits(
                          new Rectangle(0, 0, image.Width, image.Height),
                          ImageLockMode.ReadWrite, image.PixelFormat);
                AForge.Imaging.UnmanagedImage UIMAGE = new AForge.Imaging.UnmanagedImage(imageData);

                ResizeNearestNeighbor R = new ResizeNearestNeighbor((int)(UIMAGE.Width * 0.2), (int)(UIMAGE.Height * 0.2M));
                AForge.Imaging.Filters.GaussianBlur G = new GaussianBlur(1.4, 5);
                Mejorada = G.Apply(R.Apply(UIMAGE)).ToManagedImage();
            }

            return Mejorada;

        }

        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }

        private static void SaveJPGWithCompressionSetting(System.Drawing.Image image, string szFileName, long lCompression)
        {
            EncoderParameters eps = new EncoderParameters(1);
            eps.Param[0] = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, lCompression);
            ImageCodecInfo ici = GetEncoderInfo("image/jpeg");
            image.Save(szFileName, ici, eps);
            image.Dispose();
        }
        public static void ComprimiryGuardar(Bitmap ImagenBase, int AnguloDeRotacion, string NuevoNombre)
        {

            Bitmap BM = Grayscale.CommonAlgorithms.Y.Apply(ImagenBase);

            ResizeBicubic R = new ResizeBicubic((int)(BM.Width * .3), (int)(BM.Height * .3));
            BM = R.Apply(BM);
            if (AnguloDeRotacion > 0)
                BM = MejoradorDeImagenes.Rotar(BM, AnguloDeRotacion);
            MejoradorDeImagenes.SaveJPGWithCompressionSetting(BM, NuevoNombre, 35);

            FSO_NH.log4Net.FSOLog4Net.LogDebug("Nuevo Nombre Almacenado.");
            BM.Dispose();
        }
    }

}
