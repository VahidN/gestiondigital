﻿using System;
using System.Collections.Generic;
using System.Text;

using System.IO;
using GestionDigitalCore;

namespace CoreEngine
{
   
    public delegate void EventoBase();
    public delegate void _EventoDatosArchivoEnProceso(string FileName, long ByteSize, decimal PorcentajeAvance, ref bool Cancelar);
    public delegate void _EventoFinProcesoArchivoOk   (string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal);
    public delegate void _EventoFinProcesoArchivoError(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal, string Error); 
    public class Procesador
    {
        #region Eventos
            public event EventoBase ProcesoIniciado;
            public event EventoBase Working;

            public event _EventoFinProcesoArchivoOk PreArchivoProcesadoOk;
            public event _EventoFinProcesoArchivoError PreArchivoProcesadoError;

            public event _EventoDatosArchivoEnProceso ProcesandoArchivo;
            public event _EventoFinProcesoArchivoOk ArchivoProcesadoOk;
            public event _EventoFinProcesoArchivoError ArchivoProcesadoError;

            public event EventoBase ProcesoFinalizado; 
        #endregion


        #region Variables y Propiedades 
            protected ConfiguracionBase _MyCfg;

        protected ConfiguracionBase MyCfg
            {
                get { return _MyCfg; }
                set { _MyCfg = value; }
            }
            
        #endregion

        #region Metodos Normales

        public void ProcesarArchivos(ConfiguracionBase Cfg, List<string> Archivos)
        {
            string ArchivoActual = "Sin Datos"; 
            try
            {
                _MyCfg = Cfg;
                List<string> ArchivosPreprocesados = new List<string>();
                foreach (string Archivo in Archivos)
                {
                    ArchivosPreprocesados.AddRange(PreProcesar(Archivo));
                }

                if (ProcesoIniciado != null) ProcesoIniciado();
                int i = 0;
                decimal porc;
                foreach (string Archivo in ArchivosPreprocesados)
                {
                    ArchivoActual = Archivo;
                    FileInfo FI = new FileInfo(Archivo);
                    long Tamaño = FI.Length;
                    i++;
                    if (ProcesandoArchivo != null)
                    {
                        bool CancelarProceso = false;

                        porc = Convert.ToDecimal(i) / Convert.ToDecimal(ArchivosPreprocesados.Count);
                        ProcesandoArchivo(Archivo, Tamaño, porc, ref CancelarProceso);
                        if (CancelarProceso)
                            throw new ExcepcionPorCancelacion();
                    }
                    TimeSpan Inicio = new TimeSpan(DateTime.Now.Ticks);
                    try
                    {
                        Procesar(Archivo);
                        TimeSpan Fin = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan TiempoTotal = new TimeSpan();
                        TiempoTotal = Fin.Subtract(Inicio);

                        if (ArchivoProcesadoOk != null)
                        {
                            bool CancelarProceso = false;

                            porc = Convert.ToDecimal(i) / Convert.ToDecimal(ArchivosPreprocesados.Count);
                            ArchivoProcesadoOk(Archivo, Tamaño, porc, TiempoTotal);
                            if (CancelarProceso)
                                throw new ExcepcionPorCancelacion();
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("Couldn't connect to remote server"))
                        {
                            throw ex;
                        }
                    }
                }
            }
            catch (ExcepcionPorCancelacion)
            { }
            catch (Exception ex)
            {                    
                    DispararEvento_ArchivoProcesadoError(ArchivoActual, 0, 0, new TimeSpan(), ex.Message);
            }
            finally {
                if (ProcesoFinalizado != null) ProcesoFinalizado();
            }
        } 

        #endregion

        #region Metodos Virtuales
            protected virtual void CancelarProceso(){}
            protected virtual List<string> PreProcesar(string Archivo)
            {
                List<string> r = new List<string>();
                r.Add(Archivo);
                return r;
            }
            protected virtual string Procesar(string Archivo) { return ""; }           
        #endregion

        protected void DispararEvento_ProcesoIniciado()
        {
            if (ProcesoIniciado != null)
                ProcesoIniciado();
        }

        protected void DispararEvento_PreArchivoProcesadoOk(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal)
        {
            if (PreArchivoProcesadoOk != null)
                PreArchivoProcesadoOk(FileName, ByteSize, PorcentajeAvance, TiempoTotal);
        }
        protected void DispararEvento_PreArchivoProcesadoError(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal, string Error)
        {
            if (PreArchivoProcesadoError != null)
                PreArchivoProcesadoError(FileName, ByteSize, PorcentajeAvance, TiempoTotal,Error);
        }
        protected void DispararEvento_ProcesandoArchivo(string FileName, long ByteSize, decimal PorcentajeAvance, ref bool Cancelar)
        {
            if (ProcesandoArchivo != null)
                ProcesandoArchivo(FileName, ByteSize, PorcentajeAvance, ref Cancelar);
        }
        protected void DispararEvento_ArchivoProcesadoOk(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal)
        {
            if (ArchivoProcesadoOk != null)
                ArchivoProcesadoOk(FileName, ByteSize, PorcentajeAvance, TiempoTotal);
        }
        protected void DispararEvento_ArchivoProcesadoError(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal, string Error)
        {
            if (ArchivoProcesadoError != null)
                ArchivoProcesadoError(FileName, ByteSize, PorcentajeAvance, TiempoTotal, Error);
        }
        protected void DispararEvento_ProcesoFinalizado()
        {
            if (ProcesoFinalizado != null)
                ProcesoFinalizado();
        }
        protected void DispararEvento_Working()
        {
            if (Working != null)
                Working();
        }
    }
    public class ExcepcionPorCancelacion : Exception
    { }
}
