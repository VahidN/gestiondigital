﻿using System;
using System.Collections.Generic;
using System.Text;
using GestionDigitalCore.CoreObjects;
using GestionDigitalCore.CoreBussines;
using System.IO;
using System.Security.Principal;
using System.Drawing;
using AForge.Imaging.Filters;
using System.Drawing.Imaging;
using System.Threading;

using CoreEngine.OCRCore;

namespace CoreEngine
{
    public delegate void EventoBaseOCR(string Accion);
    public delegate void _EventoFinProcesoOCRArchivoOk(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal);
    public delegate void _EventoFinProcesoOCRArchivoError(string FileName, long ByteSize, decimal PorcentajeAvance, TimeSpan TiempoTotal, string Error); 
    
    public class ProcesadorOCR:Procesador
    {

        public event _EventoFinProcesoOCRArchivoOk ArchivoProcesadoOCROk;
        public event _EventoFinProcesoOCRArchivoError ArchivoProcesadoOCRError;
        public event EventoBaseOCR ProcesandoOCR;
        private ConfiguracionOCR cfg;
        private static int AnguloDeRotacion;
        private Bitmap ImagenBase;
        private OCRCore.OCRCore MyOCR;
        private int IdUltimoCampoBarraConExito = 0;
        //private ulong TiempoTotalEnTicks = 0; Puede haber Overflow
        private int ArchivoProcesados = 0;
        public bool IniciarConCampos;
        private string _SystemPath;
        public ProcesadorOCR(string SystemPath):base()
        {
            ArchivoProcesados = 0;
            IniciarConCampos = true;
            MyOCR = new OCRCore.OCRCore(SystemPath);
            _SystemPath = SystemPath;
        }

        protected override string Procesar(string Archivo)
        {
            FSO_NH.log4Net.FSOLog4Net.LogDebug("--------------------------------------------------------------------------------------------------------------");
            FSO_NH.log4Net.FSOLog4Net.LogDebug("Iniciando procesamiento de Archivo");
            FSO_NH.log4Net.FSOLog4Net.LogDebug("Archivo:\t\t" + Archivo);
            FSO_NH.log4Net.FSOLog4Net.LogDebug("Fecha y Hora:\t\t" + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss.mmm"));
            AnguloDeRotacion = 0;
            Archivo archivo = new Archivo();
            cfg = (ConfiguracionOCR)base.MyCfg;
            MejoradorDeImagenes.SystemPath = cfg.DirectorioTemp;
            BBDocumento bbdoc = new BBDocumento();
            Documento tipodoc = new Documento();
            bool Exito = false;

            DateTime FI = DateTime.Now;
            try
            {
                FileInfo InfoArchivo = new FileInfo(Archivo);
                long Tamaño = InfoArchivo.Length;
                archivo = InicializarLog(Archivo);
                if (cfg.MaxKBytesToProcess > 0 && (Tamaño > cfg.MaxKBytesToProcess * 1024))
                {
                    FSO_NH.log4Net.FSOLog4Net.LogDebug("El Archivo exede el tamaño máximo:\t\t" + Archivo);
                    CambiarNombreArchivoError(archivo, archivo.Error);
                }
                else
                {                    
                    CargarImagenArchivo(archivo);
                    if (IniciarConCampos)
                    {
                        Exito = BuscarConCamposDeOtrosDocumentos(new Documento(), archivo);
                    }
                    else
                    {
                        tipodoc = ObtenerTipoDocumento(archivo);

                        if (tipodoc.ID > 0)//Si se encontro un tipo de documento para el archivo
                        {
                            #region Proceso Sabiendo el Tipo de Documento Actual
                            archivo.IdDocumento = tipodoc.ID;
                            //Proceso los campos del tipo de documento al que pertenece el archivo
                            Exito = ProcesarCamposSeleccionados(tipodoc.MisCampos, archivo);
                            if (!Exito)
                            {
                                //Si no puede identificar con los campos del documento indicado, entonces intento con los campos 
                                //del resto de los documentos, esto me asegura un mayor grado de exito.
                                Exito = BuscarConCamposDeOtrosDocumentos(tipodoc, archivo);
                            }
                            #endregion
                        }
                        else
                        {
                            #region Proceso Sin Saber el Tipo de Documento Actual.
                            //Puedo Buscar Probando con todos los campos activos definidos, si tengo suerte, puedo identificarlo.
                            //si le paso un tipo de documento nuevo me va a analizar todos los documentos nuevamente
                            Exito = BuscarConCamposDeOtrosDocumentos(new Documento(), archivo);
                            if (!Exito)
                            {
                                archivo.Error = "No se encontro un tipo de documento válido para el archivo.";
                            }
                            #endregion
                        }
                    }
                    //try
                    //{
                    //    File.Delete(archivo.TemporaryFileName);
                    //}
                    //catch { }                   
                }
                return archivo.NombreModificado;
            }
            catch (Exception ex)
            {
                FSO_NH.log4Net.FSOLog4Net.LogFatal("Error General al Procesar Archivo: " + ex.Message );
                FSO_NH.log4Net.FSOLog4Net.LogFatal("Volcado de Pila de Llamadas--------------------------");
                FSO_NH.log4Net.FSOLog4Net.LogFatal(ex.StackTrace);
                FSO_NH.log4Net.FSOLog4Net.LogFatal("FIN Volcado de Pila de Llamadas--------------------------");
                if (ex.Message == "Problema Moviendo Archivo.")
                {
                    throw ex;
                }
                try
                {
                    archivo.Error = ex.Message;
                    archivo = new BBArchivo().Guardar(archivo);
                    return archivo.NombreModificado;
                }
                catch {
                    return "Error!!";
                }
            }
            finally
            {
                FSO_NH.log4Net.FSOLog4Net.LogDebug(" .-. .-. .-. .-. .-. .-. .-. .-. .-. .-. .-. .-. .-. .-. .-. .-.");
                if (!Exito)
                {
                    CambiarNombreArchivoError(archivo, archivo.Error);
                }
                DateTime FF = DateTime.Now;
                TimeSpan tt = FF.Subtract(FI);
                ArchivoProcesados++;                                                
                if (!Exito)
                {
                    if (ArchivoProcesadoOCRError != null)
                        ArchivoProcesadoOCRError(Archivo, archivo.TamañoOriginal, ArchivoProcesados, new TimeSpan(), archivo.Error);
                }
                else
                {
                    if (ArchivoProcesadoOCROk != null)
                        ArchivoProcesadoOCROk(Archivo, archivo.TamañoOriginal, ArchivoProcesados,new TimeSpan());
                }
                FSO_NH.log4Net.FSOLog4Net.LogDebug("Proceso Finalizado");
                FSO_NH.log4Net.FSOLog4Net.LogDebug(" .-. .-. .-. .-. .-. .-. .-. .-. .-. .-. .-. .-. .-. .-. .-. .-.");
            }
        }

        #region PostProcesamientoDeArchivos
            private void CambiarNombreArchivo(Archivo Arch, string valor)
            {
                FSO_NH.log4Net.FSOLog4Net.LogDebug("Archivo Procesado.");
                BBArchivo BBArch = new BBArchivo();
                string CarpetaUsada = ""; string NuevoNombre;
                //Verificar si ya no está duplicado
                #region Logica de Guardado y Renombrado de Archivos
                    List<Archivo> Lista = BBArch.getByFileNameModificado(valor + ".jpg");
                    if (Lista.Count == 0)
                    {
                        FSO_NH.log4Net.FSOLog4Net.LogDebug("Archivo No Duplicado.");
                        int i = 0;
                        do
                        {
                            if (i == 0)
                            {
                                NuevoNombre = cfg.DirectorioDestino + "\\" + valor + ".jpg";
                                CarpetaUsada = cfg.DirectorioDestino;
                            }
                            else //Podria suceder que hayan limpiado el log y en la base no exista nada, pero si en la carpeta.
                            {
                                Arch.Error = "Archivo ya existente en carpeta destino"; 
                                NuevoNombre = cfg.DirectorioDuplicados + "\\" + valor + "_" + i.ToString() + ".jpg";
                                CarpetaUsada = cfg.DirectorioDuplicados;
                            }
                            i++;
                        } while (File.Exists(NuevoNombre));
                    }
                    else
                    { //Si entre por aca es porque en algun momento se scaneo este documento, sin importar si ya existe en la carpeta actual.
                        FSO_NH.log4Net.FSOLog4Net.LogDebug("Archivo Duplicado.");
                        int i = 0;
                        CarpetaUsada = cfg.DirectorioDuplicados;
                        do
                        {
                            NuevoNombre = cfg.DirectorioDuplicados + "\\" + valor + "_" + i.ToString() + ".jpg";
                            Arch.Error = "Archivo ya procesado con anterioridad"; 
                            i++;
                        } while (File.Exists(NuevoNombre));
                    } 
                #endregion

                FSO_NH.log4Net.FSOLog4Net.LogDebug("Nuevo Nombre de Archivo:\t\t\t: " + NuevoNombre);

                MejoradorDeImagenes.ComprimiryGuardar(ImagenBase, AnguloDeRotacion, NuevoNombre);
                ImagenBase.Dispose();
                File.Delete(Arch.GetOriginalFullName());

                #region Almacenar Registro de Operacion en Base de Datos
                Arch.NombreModificado = NuevoNombre.Substring(CarpetaUsada.Length + 1, NuevoNombre.Length - CarpetaUsada.Length - 1);
                Arch.TamañoModificado = 0;//File.ReadAllBytes(NuevoNombre).Length;
                Arch.PathDestino = CarpetaUsada;
                DispararEventoGenerico("Guardando Archivo: " + NuevoNombre);
                BBArch.EjecutarInsertUpdate("Delete ValorArchivo where ValorArchivo.Valor = '' and ValorArchivo.IdArchivo = " + Arch.ID);
                BBArch.Guardar(Arch); 
                #endregion

                #region No deberia necesitarse si borra bien.
                //try
                //{


                //    //Antes de borrar el archivo original, deberíamos certificar que el archivo se haya creado
                //    //correctamente                    
                //    if (File.Exists(NuevoNombre))
                //    {
                //        //TEMP File.Delete(Arch.TemporaryFileName);
                //        File.Delete(Arch.GetOriginalFullName());
                //        FSO_NH.log4Net.FSOLog4Net.LogDebug("Archivo Eliminado: " + Arch.GetOriginalFullName());
                //    }
                //    else
                //    {
                //        //Revisar !!
                //        //TEMP File.Copy(Arch.TemporaryFileName, cfg.DirectorioDestino + "\\ZZZ_Reprocess_" + valor + ".tif");
                //        File.Copy(Arch.GetOriginalFullName(), cfg.DirectorioDestino + "\\ZZZ_Reprocess_" + valor + ".tif");
                //    }

                //}
                //catch  {
                //    FSO_NH.log4Net.FSOLog4Net.LogDebug("Error al Eliminar Temporal: " + Arch.GetOriginalFullName());
                //} 
                #endregion
            }
            private void CambiarNombreArchivoError(Archivo Arch, string Error)
            {
                bool Comprimir = false;
                FSO_NH.log4Net.FSOLog4Net.LogDebug("Archivo con Error.");
                #region Calculo del Nuevo Nombre
                    string NuevoNombre; int i = 0;
                    do
                    {
                        if (i == 0)
                            NuevoNombre = cfg.DirectorioErrores + "\\" + DateTime.Now.Ticks.ToString();
                        else
                            NuevoNombre = cfg.DirectorioErrores + "\\" + DateTime.Now.Ticks.ToString() + "_" + i.ToString() + ".jpg";

                        i++;
                    } while (File.Exists(NuevoNombre)); 
                #endregion
                #region Almacenar Registro en la Base de datos
                    FSO_NH.log4Net.FSOLog4Net.LogDebug("Procesamiento Final Terminado, Archivo Guardado.");
                    Arch.NombreModificado = NuevoNombre.Substring(cfg.DirectorioErrores.Length + 1, NuevoNombre.Length - cfg.DirectorioErrores.Length - 1);
                    Arch.TamañoModificado = 0;//File.ReadAllBytes(NuevoNombre).Length;
                    Arch.PathDestino = cfg.DirectorioErrores;
                    if (Error == "")
                        Error = "Documento No Identificado.";
                    Arch.Error = Error;
                    DispararEventoGenerico("Guardando Archivo: " + NuevoNombre);
                    BBArchivo BBA = new BBArchivo();
                    BBA.EjecutarInsertUpdate("Delete ValorArchivo where ValorArchivo.Valor = '' and ValorArchivo.IdArchivo = " + Arch.ID);
                    BBA.Guardar(Arch);
                #endregion
                if (Comprimir)
                {
                    NuevoNombre += ".jpg";
                    FSO_NH.log4Net.FSOLog4Net.LogDebug("Archivo con Error: " + NuevoNombre + ". Iniciando Procesamiento Final");
                    MejoradorDeImagenes.ComprimiryGuardar(ImagenBase, 0, NuevoNombre);
                    ImagenBase.Dispose();
                    File.Delete(Arch.GetOriginalFullName());

                    #region No Necesario si borra correctamente.
                    //try
                    //{
                    //    //Antes de borrar el archivo original, deberíamos certificar que el archivo se haya creado
                    //    //correctamente                    
                    //    if (File.Exists(NuevoNombre))
                    //    {                                                    
                    //        File.Delete(Arch.GetOriginalFullName());
                    //        FSO_NH.log4Net.FSOLog4Net.LogDebug("Archivo Eliminado: " + Arch.TemporaryFileName);
                    //    }
                    //    else
                    //    {
                    //        File.Move(Arch.TemporaryFileName, cfg.DirectorioErrores + "\\ZZZ_Reprocess_" + DateTime.Now.Ticks.ToString() + ".tif");
                    //    }
                    //}
                    //catch
                    //{
                    //    FSO_NH.log4Net.FSOLog4Net.LogDebug("No se puede eliminar archivo: " + Arch.TemporaryFileName);
                    //} 
                    #endregion
                }
                else {
                    NuevoNombre += ".tif";
                    ImagenBase.Dispose();
                    File.Move(Arch.GetOriginalFullName(), NuevoNombre);
                }
            }
        #endregion

        #region Procesamiento de Campos

            private Documento ObtenerTipoDocumento(Archivo archivo)
            {
                BBDocumento bbtipoDoc = new BBDocumento();
                BBCampo bbcampo = new BBCampo();

                if (cfg.IdDocumento > 0)
                    return bbtipoDoc.getById(cfg.IdDocumento);


                #region Identificación por Campos ID y Rotación Automática
                List<Documento> listdoc = bbtipoDoc.GetAll();

                foreach (Documento documento in listdoc)
                {
                    int CantidadDeRotaciones = 0;
                    foreach (Campo campo in documento.MisCampos)
                    {
                        if (campo.EsIdentificador)
                        {
                            do
                            {
                                DispararEvento_Working();
                                DispararEventoGenerico("Identificando Documento A: " + AnguloDeRotacion.ToString() + " grados, Campo: " + campo.Nombre);
                                if(cfg.RotarImagenes)
                                    ImagenBase = MejoradorDeImagenes.Rotar(ImagenBase, AnguloDeRotacion);
                                string ValorLeido = MyOCR.ProcesarImagen(ImagenBase, campo, archivo);

                                if (ValorLeido.ToLower().Contains(campo.ValorIdentificacion.ToLower()))
                                {                              
                                    return documento;
                                }
                                else
                                {
                                    CantidadDeRotaciones++;
                                    AnguloDeRotacion = ObtenerNuevoAngulo(AnguloDeRotacion);//AnguloDeRotacion == 270 ? 0 : AnguloDeRotacion + 90;
                                }
                            } while (CantidadDeRotaciones < 4 && cfg.RotarImagenes);
                        }
                    }
                }
                #endregion
                return new Documento();
            }
            private int ObtenerNuevoAngulo(int AnguloActual)
            {
                switch(AnguloActual)
                {
                    case 0: return 270;
                    case 270: return 90;
                    case 90: return 180;
                    case 180: return 1000;
                    default: return 0;
                }
            }
            private bool ProcesarCamposSeleccionados(List<Campo> listado, Archivo arch)
            {           
                foreach (Campo campo in listado)
                {
                    DispararEvento_Working();
                    //En el caso que venga por defecto el tipo de documento ¿No deberíamos leer el campo tipo identificador?
                    if (!campo.EsIdentificador)
                    {
                        DispararEventoGenerico("Procesando Campo: " + campo.Nombre);
                        FSO_NH.log4Net.FSOLog4Net.LogDebug("Procesando Campo:\t" + campo.Nombre);
                        ValorArchivo valor = new ValorArchivo();
                        valor.idArchivo = arch.ID;
                        valor.IdCampo = campo.ID;
                        valor.Valor = MyOCR.ProcesarImagen(ImagenBase, campo, arch);
                          
                        new BBValorArchivo().Guardar(valor);
                        if (valor.Valor.Length > 0 )                        
                        {
                            arch.IdDocumento = campo.IdDocumento;
                            CambiarNombreArchivo(arch, valor.Valor);
                            if (campo.TipoCampo == eTipoCampo.CodigoBarra)
                            {
                                IdUltimoCampoBarraConExito = campo.ID;
                            }
                            else {
                                IdUltimoCampoBarraConExito = 0;
                            }
                            return true;
                        }
                    }
                }
                return false;
            }
            private bool BuscarConCamposDeOtrosDocumentos(Documento tipodoc, Archivo arch)
            {          
                BBCampo BBC = new BBCampo();         
                bool Exito = false;
                int CantidadDeRotaciones = 0;

                
                if (IdUltimoCampoBarraConExito > 0)
                {
                    Exito = ProcesarUltimoCampoConExito(arch);
                }
                if (!Exito)
                {
                    List<Campo> listado = BBC.EjecutarConsulta("Select c.* from Campo c join documento d on c.iddocumento = d.iddocumento and d.iddocumento <> " + tipodoc.ID.ToString() + " and d.activo = 1 and c.esidentificador = 0 order by c.orden");

                    do
                    {
                        DispararEvento_Working();

                        if (cfg.RotarImagenes)
                        {
                            DispararEventoGenerico("Rotando a: " + AnguloDeRotacion.ToString());
                            FSO_NH.log4Net.FSOLog4Net.LogDebug("Rotando a :\t\t" + AnguloDeRotacion.ToString());
                            ImagenBase = MejoradorDeImagenes.Rotar(ImagenBase, AnguloDeRotacion);
                            //ImagenBase.Save("C:\\3.bmp", ImageFormat.Bmp);
                            DispararEventoGenerico("Fin Rotación");
                        }
                        Exito = ProcesarCamposSeleccionados(listado, arch);
                        if (!Exito)                    
                        {
                            CantidadDeRotaciones++;
                            AnguloDeRotacion = ObtenerNuevoAngulo(AnguloDeRotacion);//AnguloDeRotacion == 270 ? 0 : AnguloDeRotacion + 90;                        
                        }

                    } while (!Exito && CantidadDeRotaciones < 4 && cfg.RotarImagenes);
                }
                return Exito;
            }
            private bool ProcesarUltimoCampoConExito(Archivo arch)
            {
                List<Campo> listado = new BBCampo().EjecutarConsulta("Select * from Campo Where IdCampo = " + IdUltimoCampoBarraConExito);
                return ProcesarCamposSeleccionados(listado, arch);
            }

        #endregion

        #region Procesamiento De Imagenes
            private void CargarImagenArchivo(Archivo Arch)
            {
                try
                {
                    FSO_NH.log4Net.FSOLog4Net.LogDebug("Acción:\t\t\tCargando y Moviendo Archivo a carpeta temporal.");
                    //if (!Directory.Exists(cfg.DirectorioTemp ))
                    //    Directory.CreateDirectory(cfg.DirectorioTemp); 


                    //string tempfilename = cfg.DirectorioTemp +"\\"+ ToolBox.FileTools.getNombreUnico();
                    //Arch.TemporaryFileName = tempfilename;
                    //File.Move(Arch.GetOriginalFullName(), tempfilename);
                    //ImagenBase = new Bitmap(tempfilename);


                    ImagenBase = AForge.Imaging.Image.FromFile(Arch.GetOriginalFullName());
                    ImagenBase = AForge.Imaging.Image.Clone(ImagenBase, PixelFormat.Format32bppArgb);
                    Arch.ResolucionHorizontal = ImagenBase.HorizontalResolution;
                    Arch.ResolucionVertical = ImagenBase.VerticalResolution;
                    DispararEvento_Working();
                    FSO_NH.log4Net.FSOLog4Net.LogDebug("Arch. Temp.:\t\t" + Arch.GetOriginalFullName());
                }
                catch (Exception Ex)
                {
                    throw new Exception("Problema Moviendo Archivo.");
                }
            }
        #endregion
        private void DispararEventoGenerico(string Accion)
        {
            if (ProcesandoOCR != null)
                ProcesandoOCR(Accion);
        }
        private Archivo InicializarLog(string Archivo)
        {
            //creo e inicializo el objeto archivo
            FSO_NH.log4Net.FSOLog4Net.LogDebug("Acción:\t\t\tIniciando Log.");
            Archivo archivo = new Archivo();
            FileInfo file = new FileInfo(Archivo);
            archivo.NombreOriginal = file.Name;
            archivo.PathOrigen = file.DirectoryName;
            archivo.TamañoOriginal = file.Length;
            archivo.FechaProceso = DateTime.Now;
            archivo.UsuarioProceso = Environment.UserName;
            new BBArchivo().Guardar(archivo);
            DispararEvento_Working();
            FSO_NH.log4Net.FSOLog4Net.LogDebug("Acción:\t\t\tLog Iniciado.");
            return archivo;
        }
    }
}
