﻿using System;
using System.Collections.Generic;
using System.Text;
using GestionDigitalCore.CoreObjects;
using FSO.NH.WEB;
using System.IO;

namespace CoreEngine
{
    public delegate void _WaitingSending(int waittime);
    public delegate void _WaitPeriodoFinished(string Archivo);
    public class ProcesadorFTP : Procesador
    {
        public event _WaitingSending WaitingSending;
        public event _WaitPeriodoFinished WaitPeriodoFinished;
        ConfiguracionFTP cfg;
        protected override List<string> PreProcesar(string Archivo)
        {
            return base.PreProcesar(Archivo);
        }
        protected override string Procesar(string Archivo)
        {
            try
            {
                cfg = (ConfiguracionFTP)MyCfg;

                if (cfg.RacionarAnchoDeBanda)
                {
                    int secondstowait = GetRamdomNumber(cfg.RacionarDesde, cfg.RacionarHasta);
                    if (WaitingSending != null) WaitingSending(secondstowait);
                    System.Threading.Thread.Sleep(secondstowait * 1000);
                    if (WaitPeriodoFinished != null) WaitPeriodoFinished(Archivo);
                }

                FtpClient ftp = new FtpClient(cfg.FtpServer, cfg.FtpUsuario, cfg.FtpPassword);
                ftp.WorkingEvent += new WorkingDelegate(ftp_WorkingEvent);
                ftp.Login();
                if (!string.IsNullOrEmpty(cfg.FTPDirectorioBase))
                {
                    ftp.ChangeDir(cfg.FTPDirectorioBase);
                }

                ftp.Upload(Archivo);
                string zipFileName = Comprimir(Archivo);
                DoBackup(zipFileName);
                EliminarArchivoOriginal(zipFileName);
                return zipFileName;
            }
            catch (Exception ex)
            {

                FileInfo fi = new FileInfo(Archivo);
                base.DispararEvento_ArchivoProcesadoError(Archivo, fi.Length, 0, new TimeSpan(), ex.Message);
                throw ex;
            }
        }

        private int GetRamdomNumber(int min, int max)
        {
            Random r = new Random();
            return r.Next(min, max);
        }

        void ftp_WorkingEvent()
        {
            base.DispararEvento_Working();
        }

        private void EliminarArchivoOriginal(string Archivo)
        {
            try
            {
                if (cfg.EliminarArchivoOriginal)
                {
                    File.Delete(Archivo);
                }
            }
            catch (Exception ex)
            {
                
                throw new Exception("Error Al Eliminar Archivo Original: " + ex.Message);
            }
        }

        private void DoBackup(string FileName)
        {
            try
            {
                if (cfg.RequiereBackup)
                {
                    FileInfo FI = new FileInfo(FileName);
                    DirectoryInfo DI = new DirectoryInfo(cfg.BackUpDirectory);

                    string SubPath = FI.DirectoryName.Substring(cfg.DirectorioOrigen.Length, FI.DirectoryName.Length - cfg.DirectorioOrigen.Length);
                    if (!String.IsNullOrEmpty(SubPath))
                    { 
                        if(!Directory.Exists(cfg.BackUpDirectory + SubPath))
                            DI.CreateSubdirectory(SubPath.Substring(1,SubPath.Length-1));
                    }

                    File.Copy(FileName, cfg.BackUpDirectory +SubPath +"\\" + FI.Name);
                }
            }
            catch (Exception ex)
            {
                if(!ex.Message.ToLower().Contains("ya existe"))
                    throw new Exception("Error Al Realizar Backup: " + ex.Message);
            }
        }

        private string Comprimir(string Archivo)
        {
            try
            {
                if (cfg.RequiereCompresionZIP)
                {
                    FileInfo fi = new FileInfo(Archivo);
                    ToolBox.ZipEngine.ComprimirZipFast(fi.DirectoryName+"\\", fi.Name + ".zip", fi);
                    File.Delete(Archivo);
                    return Archivo+".zip";
                }
                else
                {
                    return Archivo;
                }
            }
            catch (Exception ex)
            {

                throw new Exception("Error Al Comprimir: " + ex.Message);
            }
        }
    }
}
