using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using MySql.Data.MySqlClient;
using FSO_NH.log4Net;
using System.Data;

namespace DataAccess
{
    public class ConectorMySQL : BaseConnector
    {


        public ConectorMySQL()
        {
            myConn = new MySql.Data.MySqlClient.MySqlConnection();
            

        }
        protected override void Conectar()
        {
            try
            {
                RegistrarEvento(connectionString);
                myConn = new MySql.Data.MySqlClient.MySqlConnection(connectionString);                    
                myConn.Open();
                RegistrarEvento("Conectado");
            }
            catch (Exception ex)
            {
               
                Console.WriteLine(ex.Message.ToString());
                throw new FSOException(ex);
            }
           

        }
        public override void CerrarConexion()
        { 
                if (myConn != null)
                {
                    myConn.Close();
                    myConn.Dispose();
                }
        }
        protected override void RegistrarEvento(string Evento)
        {
            if (ShowSQL == "TRUE")
            {
                FSO_NH.log4Net.FSOLog4Net.LogInfo(Evento);
            }
        }
        public override IDataReader EjecutarSelect(string Command)
        {
            try{
                
                Conectar();
                RegistrarEvento(Command);
                MySqlCommand command = ((MySql.Data.MySqlClient.MySqlConnection)myConn).CreateCommand(); 
                command.CommandText = Command;                
                return command.ExecuteReader(); 

            }catch(Exception ex)
            {

                throw new FSOException(ex);
            }
        }
        public override int EjecutarInsertUpdate(string Command)
        {
            try
            {
                
                Conectar();
                RegistrarEvento(Command);
                MySqlCommand command = ((MySql.Data.MySqlClient.MySqlConnection)myConn).CreateCommand();
                command.CommandText = Command;
                return command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw new FSOException(ex);
            }
            finally
            {
                CerrarConexion();
            }
        }
    }
}
