﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    public class SentenciasSQL
    {
        private string tablename;

        public string Tablename
        {
            get { return tablename; }
            set { tablename = value; }
        }
        private string insert;

        public string Insert
        {
            get { return insert; }
            set { insert = value; }
        }
        private string update;

        public string Update
        {
            get { return update; }
            set { update = value; }
        }
        private string campoId;

        public string CampoId
        {
            get { return campoId; }
            set { campoId = value; }
        }
        private string delete;

        public string Delete
        {
            get { return delete; }
            set { delete = value; }
        }
    }
}
