﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlServerCe;
using FSO_NH.log4Net;
using System.Data;

namespace DataAccess
{
    public class ConectorSQLCE : BaseConnector
    {



        public ConectorSQLCE()
        {
            myConn = new SqlCeConnection();
            BaseConfig.AppConfig CFG = new BaseConfig.AppConfig();
            ShowSQL = CFG.ShowSQL;
            connectionString = CFG.ConectionString;                        
        }
        protected override void Conectar()
        {
            try
            {
                RegistrarEvento(connectionString);
                myConn = new SqlCeConnection(connectionString);
                myConn.Open();
                RegistrarEvento("Conectado");
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("The database file has been created by an earlier version of SQL Server Compact"))
                {
                    try
                    {
                        SqlCeEngine CEENG = new SqlCeEngine(connectionString);                        
                        CEENG.Upgrade(@"DataSource='C:\DB35.sdf'; Password=123456");                            
                    }
                    catch (Exception ex1)
                    {
                        throw new FSOException(ex1);
                    }
                }
                Console.WriteLine(ex.Message.ToString());
                throw new FSOException(ex);
            }


        }
        public override void CerrarConexion()
        {
            if (myConn != null)
            {
                myConn.Close();
                myConn.Dispose();
            }
        }
        protected override void RegistrarEvento(string Evento)
        {
            if (ShowSQL == "TRUE")
            {
                FSO_NH.log4Net.FSOLog4Net.LogInfo(Evento);
            }
        }
        public override IDataReader EjecutarSelect(string Command)
        {
            try
            {

                Conectar();
                RegistrarEvento(Command);
                SqlCeCommand command = ((SqlCeConnection)myConn).CreateCommand();
                command.CommandText = Command;
                return command.ExecuteReader();

            }
            catch (Exception ex)
            {

                throw new FSOException(ex);
            }
        }
        public override int EjecutarInsertUpdate(string Command)
        {
            try
            {

                Conectar();
                RegistrarEvento(Command);
                SqlCeCommand command = ((SqlCeConnection)myConn).CreateCommand();
                command.CommandText = Command;
                return command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                throw new FSOException(ex);
            }
            finally
            {
                CerrarConexion();
            }
        }
       
    }
}
