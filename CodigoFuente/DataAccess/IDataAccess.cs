using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace DataAccess
{
    public interface IDataAccess <T> where T: new() 
    {
      
        T Guardar(T m);        
        List<T> GetAll();
        T getById(int ID);
        T CargarRegistro(IDataReader Reader);
        T GetFromReader(IDataReader Reader);
        void CargarObjetosHijos(ref T m);
        List<T> CargarListas(IDataReader Reader);
    }
}
