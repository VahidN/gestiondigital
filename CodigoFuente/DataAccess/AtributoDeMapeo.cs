﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess
{
    [System.AttributeUsage(System.AttributeTargets.Property)]
    public class MapeoColumna : System.Attribute
    {
        public string ColumnName;

        public MapeoColumna()
        {
            this.ColumnName = "__FieldNameIsColumnName__";
        }
        public MapeoColumna(string _ColumnName)
        {
            this.ColumnName = _ColumnName;
        }
    }
    [System.AttributeUsage(System.AttributeTargets.Class)]
    public class MapeoClase : System.Attribute
    {
        public string TableName;
        public string IdFieldName;

        public MapeoClase()
        {
            this.TableName = "__ClassNameIsTableName__";
        }
        public MapeoClase(string _TableName)
        {
            this.TableName = _TableName;
        }
    }
}
