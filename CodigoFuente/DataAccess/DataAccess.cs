using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using FSO_NH.log4Net;
using BaseConfig;
using System.Reflection;

namespace DataAccess
{

    public class DataAccess<T> : IDataAccess<T> where T : DomainObject, new()
    {
        private static System.Collections.Hashtable MySQLs;
        private static BaseConnector MyConector;
        protected dbType MyTipo;
        protected string sqlInsert;
        protected string sqlUpdate;
        protected string sqlDelete;
        protected string tableName;
        protected string fieldlist;
        public bool LazyLoad = true;
        private string CampoId;
        private Type MyClase;
        public DataAccess()
        {
            if (MySQLs == null)
                MySQLs = new System.Collections.Hashtable();

            MyClase = typeof(T);
            tableName = MyClase.Name;
            ProcesarFieldList();

            if (MyConector == null)
            {
                AppConfig App = new AppConfig();
                switch (App.SQLType.ToLower())
                {
                    //SQLServer2000, SQLServer2005, SQLServer2008, MySQL, SQLServerCE35
                    case "sqlserverce35": MyConector = BaseConnector.Factory(dbType.SQLServerCE35); MyTipo = dbType.SQLServerCE35; break;
                    case "sqlserver2000": MyConector = BaseConnector.Factory(dbType.SQLServer2000); MyTipo = dbType.SQLServer2000; break;
                    case "sqlserver2005": MyConector = BaseConnector.Factory(dbType.SQLServer2005); MyTipo = dbType.SQLServer2005; break;
                    case "sqlserver2008": MyConector = BaseConnector.Factory(dbType.SQLServer2008); MyTipo = dbType.SQLServer2008; break;
                    case "mysql": MyConector = BaseConnector.Factory(dbType.MySQL); ; MyTipo = dbType.MySQL; break;
                }

                MyConector.ShowSQL = App.ShowSQL;
                MyConector.connectionString = App.ConectionString;

            }
           
        }


        public void ProcesarFieldList()
        {
            if (!MySQLs.ContainsKey(tableName))
            {
                string camposUpdate = "";
                string camposInsert = "";
                string whereUpdate = " where ";
              


              Object[] AtributosClase = MyClase.GetCustomAttributes(typeof(MapeoClase), true);
                if (AtributosClase.Length != 0)
                {
                    string tabla; string idfield;
                    MapeoClase mc = (MapeoClase)AtributosClase[0];
                    if (mc.TableName != "__ClassNameIsTableName__")
                    {
                        tableName = mc.TableName.ToLower(); //--Si cambiamos este valor ver que pasa con el objeto MySQLs
                        CampoId ="id"+ mc.IdFieldName.ToLower();
                      
                    }
                    else {
                        tabla = tableName;
                        CampoId = "id"+tableName.ToLower();                           
                    }
                     whereUpdate += CampoId + "= @@" + CampoId;
                }



                PropertyInfo[] ListaPropiedades = MyClase.GetProperties();
                foreach (PropertyInfo pi in ListaPropiedades)                
                {

                    Object[] AtributosPropiedades = pi.GetCustomAttributes(typeof(MapeoColumna), true);
                     if (AtributosPropiedades.Length != 0)
                     {
                         MapeoColumna mc = (MapeoColumna)AtributosPropiedades[0];
                         if(mc.ColumnName=="__FieldNameIsColumnName__")
                         {
                            fieldlist += pi.Name + ", ";
                         }else{
                             fieldlist += mc.ColumnName + ", ";
                         }
                     }
                    
                   
                }

                fieldlist = fieldlist.Substring(0, fieldlist.Length - 2);

                string[] campos = fieldlist.Split(',');


                string listaDeCampos = "";
                foreach (string campo in campos)
                {
                    string mycampo = campo.Trim().ToLower();
                
                    camposUpdate += mycampo + "= @@" + mycampo + ", ";
                    camposInsert += "@@" + mycampo + ", ";
                    listaDeCampos += mycampo + ", ";

                
                }
                camposUpdate = camposUpdate.Substring(0, camposUpdate.Length - 2);
                camposInsert = camposInsert.Substring(0, camposInsert.Length - 2);
                listaDeCampos = listaDeCampos.Substring(0, listaDeCampos.Length - 2);

                sqlInsert = "INSERT INTO " + tableName + " (" + listaDeCampos + ") values (" + camposInsert + ")";
                sqlUpdate= "UPDATE "+ tableName +" SET " + camposUpdate + whereUpdate;
                sqlDelete="DELETE "+ tableName + " where " + CampoId + " = @@" + CampoId;
                SentenciasSQL sSQL = new SentenciasSQL();
                sSQL.Tablename = tableName;
                sSQL.Insert = sqlInsert;
                sSQL.Update = sqlUpdate;
                sSQL.Delete = sqlDelete;
                sSQL.CampoId = CampoId;
                MySQLs.Add(tableName, sSQL);
            }
            else {
                SentenciasSQL sSQL = (SentenciasSQL)MySQLs[tableName];
                tableName = sSQL.Tablename;
                sqlInsert = sSQL.Insert;
                sqlUpdate = sSQL.Update;
                CampoId = sSQL.CampoId;
                sqlDelete = sSQL.Delete;
            }
        }



        public DataAccess(dbType tipo)
        {
            MyConector = BaseConnector.Factory(tipo);
            MyTipo = tipo;
        }

        protected void  SetValueToSQL(ref string sqlBase ,string FieldName, String FieldValue)
        {
            FieldValue = FieldValue == null ? "" : FieldValue;
            sqlBase = sqlBase.Replace("@@" + FieldName.Trim().ToLower(), "'" + FieldValue.Replace("'", "''") + "'");
        }
        protected void SetValueToSQL(ref string sqlBase, string FieldName, int? FieldValue)
        {
            sqlBase = sqlBase.Replace("@@" + FieldName.Trim().ToLower(), FieldValue.ToString().Replace(',', '.'));
        }
        protected void SetValueToSQL(ref string sqlBase, string FieldName, double? FieldValue)
        {
            sqlBase = sqlBase.Replace("@@" + FieldName.Trim().ToLower(), FieldValue.ToString().Replace(',', '.'));
        }
        protected void SetValueToSQL(ref string sqlBase, string FieldName, decimal? FieldValue)
        {
            sqlBase = sqlBase.Replace("@@" + FieldName.Trim().ToLower(), FieldValue.ToString().Replace(',', '.'));
        }
        protected void SetValueToSQL(ref string sqlBase, string FieldName, long? FieldValue)
        {
            sqlBase = sqlBase.Replace("@@" + FieldName.Trim().ToLower(), FieldValue.ToString().Replace(',', '.'));
        }
        protected void SetValueToSQL(ref string sqlBase, string FieldName, bool? FieldValue)
        {
            sqlBase = sqlBase.Replace("@@" + FieldName.Trim().ToLower(), FieldValue.Value ? "1" : "0");
        }
        protected void SetValueToSQL(ref string sqlBase, string FieldName, DateTime? FieldValue)
        {
            sqlBase = sqlBase.Replace("@@" + FieldName.Trim().ToLower(), "'" + FieldValue.Value.ToString("yyyyMMdd hh:mm") + "'");
        }

        #region IDataAccess<T> Members

        public virtual T Guardar(T m) {

            string sqlBase = "";
            try
            {
                Validar(m);
                
                if (m.ID == 0)
                {
                    sqlBase = sqlInsert;

                }
                else
                {
                    sqlBase = sqlUpdate;
                }

                ObtenerParametrosParaSQL(ref sqlBase, m);
                
                EjecutarInsertUpdate(sqlBase);
                
                if (m.ID == 0)
                {
                    T Nuevo = GetLastInserted();
                    m.ID = Nuevo.ID;
                }
                
                AccionesPostGrabacion(m);
                return m;
            }
            catch (Exception ex)
            {
                throw new FSOException(ex);
            }
			
        
        }

        protected virtual void AccionesPostGrabacion(T m)
        {
            
        }
        protected virtual void AccionesPostEliminación(int IdEliminado)
        {}

        protected virtual void Validar(T m)
        {
          
        }

        protected virtual void ObtenerParametrosParaSQL(ref string sqlBase, T m)
        {
            
        }

        public virtual void Eliminar(int Id)
        {
            T obj = getById(Id);
            Eliminar(obj);

        }
        protected virtual void ValidarEliminacion(T Obj) { }
        public virtual void Eliminar(T m) {
            ValidarEliminacion(m);
            EjecutarInsertUpdate(sqlDelete.Replace("@@" + CampoId, m.ID.ToString()));
            AccionesPostEliminación(m.ID);                  
        }
        public virtual T getNew()
        {
            return new T();
        }

        public virtual T GetLastInserted()
        {

            List<T> Listado=  GetAll( "", CampoId + " desc", 1);
            if (Listado.Count > 0)
                return Listado[0];
            else
            {
                throw new FSOException("No se pudo encontrar el ultimo registro insertado en " + tableName);
            }
        }
        public virtual List<T> GetAll()
        {
            return GetAll("", "", -1);
        }
        public virtual List<T> GetAll( string Filter)
        {
            return GetAll( Filter, "",-1);
        }

        public virtual List<T> GetAll(string Filter, string Order)
        {
            return GetAll(Filter, Order, -1);
        }

        public virtual List<T> GetAll( string Filter, string Order, int Top)
        {
            if (!string.IsNullOrEmpty(Filter))
                Filter = " Where " + Filter;

            if (!string.IsNullOrEmpty(Order))
                Order = " Order By " + Order;

            string sTop = "select @@SQL@@";
            if (Top > 0) {
                if (MyTipo == dbType.MySQL)
                {
                    sTop = "select @@SQL@@ limit " + Top.ToString();
                }
                else {
                    sTop = "select TOP (" + Top.ToString() + ") @@SQL@@";
                }
            }
            string SQL = sTop.Replace("@@SQL@@", " * from " + tableName + Filter + Order);
            IDataReader Reader = MyConector.EjecutarSelect(SQL);
            return CargarListas(Reader);
        }

   
        public virtual T getById(int ID)
        {
            IDataReader Reader = MyConector.EjecutarSelect("select * from " + tableName + " where " + CampoId + " = '" + ID.ToString() + "'");
            return CargarRegistro(Reader);
        }

        public void EjecutarInsertUpdate(string sql)
        {
            MyConector.EjecutarInsertUpdate(sql);
        }
        public void CerrarConexion()
        {
            MyConector.CerrarConexion();
        }
        public virtual List<T> EjecutarConsulta(string sql)
        {
            IDataReader Reader = MyConector.EjecutarSelect(sql);
            return CargarListas(Reader);
        }
        protected IDataReader EjecutarSelect(string sql)
        {
            return MyConector.EjecutarSelect(sql);
        }
        public virtual T CargarRegistro(IDataReader Reader)
        {
            T MyObj = new T();
            while (Reader.Read())
            {
                MyObj = GetFromReader(Reader);
            }
            MyConector.CerrarConexion();
            return MyObj;
        }

        public virtual T GetFromReader(IDataReader Reader) { return default(T); }

        public virtual void CargarObjetosHijos(ref T m) { }

        public virtual List<T> CargarListas(IDataReader Reader)
        {
            List<T> MyListado = new List<T>();
            while (Reader.Read())
            {
                MyListado.Add(GetFromReader(Reader));
            }
            MyConector.CerrarConexion();
            return MyListado;
        }



  
        #endregion
    }
}
