﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.Common;

namespace DataAccess
{
    public class BaseConnector
    {
        public dbType TipoActual;
        public string connectionString;
        public string ShowSQL = "FALSE";
        protected DbConnection myConn;

        protected virtual void ConectorMySQL(){}
        protected virtual void Conectar() { }
        public virtual void CerrarConexion() { }
        protected virtual void RegistrarEvento(string Evento) { }
        public virtual IDataReader EjecutarSelect(string Command) { return null; }
        public virtual int EjecutarInsertUpdate(string Command) { return 0; }
        public static BaseConnector Factory(dbType Tipo)
        {
            BaseConnector Base = new BaseConnector();
            switch (Tipo)
            {
                case dbType.SQLServer2000: Base = new ConectorSQLServer(); break;
                case dbType.SQLServer2005: Base = new ConectorSQLServer(); break;
                case dbType.SQLServer2008: Base = new ConectorSQLServer(); break;
                case dbType.SQLServerCE35: Base = new ConectorSQLCE(); break;
                case dbType.MySQL: Base = new ConectorMySQL(); ; break;
            }
            Base.TipoActual = Tipo;
            return Base;
        }
    }
    public enum dbType
    {
        SQLServer2000, SQLServer2005, SQLServer2008, MySQL, SQLServerCE35
    }
}
