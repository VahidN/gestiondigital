using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;

namespace GestionDigitalCore.CoreObjects
{
    [MapeoClase()]
    public class Archivo:DomainObject
    {

        public Archivo()
        {
            _MisValores = new List<ValorArchivo>();
            _NombreModificado = "";
            _TamañoModificado = 0;
            _TamañoOriginal = 0;
            _PathOrigen = "";
            _UsuarioProceso = "";
            _Error = "";
            _NombreOriginal="";
            _PathDestino="";
        }
        public string TemporaryFileName;
        private int _idDocumento;
        private List<ValorArchivo> _MisValores;
        private string _NombreModificado;
        private long _TamañoOriginal;
        private long _TamañoModificado;
        private float _ResolucionHorizontal;
        private float _ResolucionVertical;
        private string _PathOrigen;
        private string _UsuarioProceso;
        private string _Error;
        private string _NombreOriginal;
        private DateTime _FechaProceso;
        private string _PathDestino;
        private bool _CambioManual;

        [MapeoColumna()]
        public bool CambioManual
        {
            get { return _CambioManual; }
            set { _CambioManual = value; }
        }

        [MapeoColumna()]
        public int IdDocumento
        {
            get { return _idDocumento; }
            set { _idDocumento = value; }
        }
        
        [MapeoColumna()]
        public string NombreOriginal
        {
            get { return _NombreOriginal; }
            set { _NombreOriginal = value; }
        }

        [MapeoColumna()]
        public string NombreModificado
        {
            get { return _NombreModificado; }   
            set { _NombreModificado = value; }
        }


        [MapeoColumna("TamanoOriginal")]
        public long TamañoOriginal
        {
          get { return _TamañoOriginal; }
          set { _TamañoOriginal = value; }
        }

        [MapeoColumna("TamanoModificado")]
        public long TamañoModificado
        {
          get { return _TamañoModificado; }
          set { _TamañoModificado = value; }
        }
        [MapeoColumna()]
        public float ResolucionHorizontal
        {
            get { return _ResolucionHorizontal; }
            set { _ResolucionHorizontal = value; }
        }

        [MapeoColumna()]
        public float ResolucionVertical
        {
            get { return _ResolucionVertical; }
            set { _ResolucionVertical = value; }
        }


        [MapeoColumna()]
        public string PathOrigen
        {
            get { return _PathOrigen; }
            set { _PathOrigen = value; }
        }


        [MapeoColumna()]
        public string PathDestino
        {
            get { return _PathDestino; }
            set { _PathDestino = value; }
        }
        
        
        [MapeoColumna()]
        public DateTime FechaProceso
        {
            get { return _FechaProceso; }
            set { _FechaProceso = value; }
        }

        [MapeoColumna()]
        public string UsuarioProceso
        {
            get { return _UsuarioProceso; }
            set { _UsuarioProceso = value; }
        }

        [MapeoColumna()]
        public string Error
        {
            get { return _Error; }
            set { _Error = value; }
        }


        public List<ValorArchivo> MisValores
        {
            get { return _MisValores; }
            set { _MisValores = value; }
        }

        public string getTamañoOriginalFormateado()
        {
            return GetSizeFormateado("original");
        }
        public string getTamañoModificadoFormateado()
        {
            return GetSizeFormateado("modificado");
        }
        public string GetOriginalFullName()
        {
            return PathOrigen + "\\" + NombreOriginal;
        }
        public string GetModificadoFullName()
        {
            return PathDestino + "\\" + NombreModificado;
        }
        private string GetSizeFormateado(string campo)
        {
            long bytes;
            if(campo=="original")
                bytes = _TamañoOriginal;
            else
                bytes = _TamañoModificado;

            return ToolBox.FileTools.GetFileSizeFormateado(bytes);
        }

    }
}
