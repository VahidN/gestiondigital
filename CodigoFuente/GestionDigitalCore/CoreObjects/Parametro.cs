using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;

namespace GestionDigitalCore.CoreObjects
{
    [MapeoClase()]
    public class Parametro : DomainObject
    {

        string _Nombre;
        string _Valor;


        [MapeoColumna()]
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }
       
        [MapeoColumna()]
        public string Valor
        {
            get { 
                return _Valor; 
                }
            set { 
                _Valor = value; 
                }
        }

    }
}
