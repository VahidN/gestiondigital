﻿using System;
using System.Collections.Generic;
using System.Text;
using FSO.NH.WEB;
using DataAccess;

namespace GestionDigitalCore
{

    public class ConfiguracionBase : DomainObject
    {

        private string _EmailAlerta;
        private string _Nombre;
        private bool _DefaultConfig;
        private SMTPConnParameters _SMTPConfig;
        private int _maxKBytesToProcess;

        public int MaxKBytesToProcess
        {
            get { return _maxKBytesToProcess; }
            set { _maxKBytesToProcess = value; }
        }

        [MapeoColumna()]
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

       

        [MapeoColumna()]
        public string EmailAlerta
        {
            get
            {
                return _EmailAlerta;
            }
            set
            {
                _EmailAlerta = value;
            }
        }

        
        [MapeoColumna()]
        public bool DefaultConfig
        {
            get { return _DefaultConfig; }
            set { _DefaultConfig = value; }
        }




        [MapeoColumna("SMTPPassword")]
        public string MySMTPPassword {
            get {
                return _SMTPConfig.SMTPPassword;
            }
            set {
                _SMTPConfig.SMTPPassword = value;
            }
        }
        [MapeoColumna("SMTPServer")]
        public string MySMTPServer
        {
            get
            {
                return _SMTPConfig.SMTPServer;
            }
            set
            {
                _SMTPConfig.SMTPServer = value;
            }
        }
        [MapeoColumna("SMTPUser")]
        public string MySMTPUser
        {
            get
            {
                return _SMTPConfig.SMTPUser;
            }
            set
            {
                _SMTPConfig.SMTPUser = value;
            }
        }



        public SMTPConnParameters SMTPConfig
        {
            get { return _SMTPConfig; }
            set { _SMTPConfig = value; }
        }
        public string MySMTPPort
        {
            get
            {
                return _SMTPConfig.SMTPPort;
            }
            set
            {
                _SMTPConfig.SMTPPort = value;
            }
        }
        public bool MyUseSSL
        {
            get
            {
                return _SMTPConfig.UseSSL;
            }
            set
            {
                _SMTPConfig.UseSSL = value;
            }
        }
    }
}
