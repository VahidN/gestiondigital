﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;

namespace GestionDigitalCore.CoreObjects
{
    [MapeoClase()]
    public class Campo : DomainObject
    {

        private int _IdDocumento;
        private string _Nombre;
        private int _YInicial;
        private int _XInicial;
        private int _Alto;
        private int _Ancho;
        private int _GradoRotacion;
        private bool _EsNombreDocumento;
        private eTipoCampo _TipoCampo;
        private bool _EsIdentificador;
        private string _ValorIdentificacion;
        private int _Orden;
        private bool _MejorarImagen;
        private int _TipoProcesamiento;
        private int _Longitud;
        private string _TipoDato;

        [MapeoColumna()]
        public int TipoPreprocesamiento
        {
            get { return _TipoProcesamiento; }
            set { _TipoProcesamiento = value; }
        }
   

        [MapeoColumna()]
        public int IdDocumento
        {
            get { return _IdDocumento; }
            set { _IdDocumento = value; }
        }

        [MapeoColumna()]
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        [MapeoColumna("Arriba")]
        public int YInicial
        {
            get { return _YInicial; }
            set { _YInicial = value; }
        }

        [MapeoColumna("Izquierda")]
        public int XInicial
        {
            get { return _XInicial; }
            set { _XInicial = value; }
        }

        [MapeoColumna("Abajo")]
        public int Alto
        {
            get { return _Alto; }
            set { _Alto = value; }
        }

        [MapeoColumna("Derecha")]
        public int Ancho
        {
            get { return _Ancho; }
            set { _Ancho = value; }
        }

        [MapeoColumna()]
        public int Longitud
        {
            get { return _Longitud; }
            set { _Longitud = value; }
        }

        [MapeoColumna()]
        public string TipoDato
        {
            get { return _TipoDato; }
            set { _TipoDato = value; }
        }


        [MapeoColumna()]
        public int GradoRotacion
        {
            get { return _GradoRotacion; }
            set { _GradoRotacion = value; }
        }

        [MapeoColumna()]
        public bool EsNombreDocumento
        {
            get { return _EsNombreDocumento; }
            set { _EsNombreDocumento = value; }
        }

        [MapeoColumna()]
        public eTipoCampo TipoCampo
        {
            get { return _TipoCampo; }
            set { _TipoCampo = value; }
        }

        [MapeoColumna()]
        public bool EsIdentificador
        {
            get { return _EsIdentificador; }
            set { _EsIdentificador = value; }
        }

        [MapeoColumna()]
        public string ValorIdentificacion
        {
            get { return _ValorIdentificacion; }
            set { _ValorIdentificacion = value; }
        }

        

        [MapeoColumna()]
        public int Orden
        {
            get { return _Orden; }
            set { _Orden = value; }
        }

        [MapeoColumna()]
        public bool MejorarImagen
        {
            get { return _MejorarImagen; }
            set { _MejorarImagen = value; }
        }

        public string TipoCampoDescripcion
        {
            get {
                switch (_TipoCampo)
                {
                    case eTipoCampo.CodigoBarra: return "Codigo de Barras";
                    case eTipoCampo.Texto: return "Texto";
                    default: return "No definido";

                }
            }

        }
    }
    public enum eTipoCampo
    {
        Texto=0, CodigoBarra=1
    }
}
