using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;

namespace GestionDigitalCore.CoreObjects
{
    [MapeoClase()]
    public class ValorArchivo : DomainObject
    {

        private int _idArchivo;
        private int _IdCampo;
        private string _Valor="";
        private string _Error="";


        [MapeoColumna()]
        public int idArchivo
        {
            get { return _idArchivo; }
            set { _idArchivo = value; }
        }
       
        [MapeoColumna()]
        public int IdCampo
        {
            get { return _IdCampo; }
            set { _IdCampo = value; }
        }

        
        [MapeoColumna()]
        public string Valor
        {
            get { return _Valor; }
            set { _Valor = value; }
        }
       
        [MapeoColumna()]
        public string Error
        {
            get { return _Error; }
            set { _Error = value; }
        }

    }
}
