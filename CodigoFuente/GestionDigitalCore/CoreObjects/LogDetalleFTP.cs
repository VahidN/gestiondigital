﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;

namespace GestionDigitalCore.CoreObjects
{
    [MapeoClase()]
    public class LogDetalleFTP : DomainObject
    {

        private int _IdLogFTP;
        private string _FileName;
        private DateTime _FechaInicio;
        private DateTime _FechaFin;
        private long _TiempoSubida;
        private string _Error;
        private long _ByteSize;





        [MapeoColumna()]
        public int IdLogFTP
        {
            get { return _IdLogFTP; }
            set { _IdLogFTP = value; }
        }
       
        [MapeoColumna()]
        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }
        
        [MapeoColumna()]
        public DateTime FechaInicio
        {
            get { return _FechaInicio; }
            set { _FechaInicio = value; }
        }
        
        [MapeoColumna()]
        public DateTime FechaFin
        {
            get { return _FechaFin; }
            set { _FechaFin = value; }
        }
       
        [MapeoColumna()]
        public long TiempoSubida
        {
            get { return _TiempoSubida; }
            set { _TiempoSubida = value; }
        }
        
        [MapeoColumna()]
        public string Error
        {
            get { return _Error; }
            set { _Error = value; }
        }
        
        [MapeoColumna()]
        public long ByteSize
        {
            get { return _ByteSize; }
            set { _ByteSize = value; }
        }





        public string GetTiempoTotal()
        {
            TimeSpan ts = new TimeSpan(TiempoSubida);
            return ts.Hours.ToString() + ":"+ts.Minutes.ToString() + ":"+ts.Seconds.ToString() + "."+ts.Milliseconds.ToString();
        }


        public string GetSizeFormateado()
        {
            return ToolBox.FileTools.GetFileSizeFormateado(_ByteSize);
        }
    }
}
