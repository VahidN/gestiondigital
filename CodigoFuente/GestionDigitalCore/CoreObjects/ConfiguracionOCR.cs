﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;

namespace GestionDigitalCore.CoreObjects
{
    [MapeoClase()]
    public class ConfiguracionOCR : ConfiguracionBase
    {
        private bool _ProcesarTiffMultipaginas;
        private string _DirectorioOrigen;
        private string _TipoArchivo;
        private string _DirectorioDestino;
        private string _DirectorioTemp;
        private string _DirectorioErrores;        
        private int _IdDocumento;
        private string _DirectorioDuplicados;
        private bool _RotarImagenes;

        public bool RotarImagenes
        {
            get { return _RotarImagenes; }
            set { _RotarImagenes = value; }
        }

        [MapeoColumna()]
        public string DirectorioDuplicados
        {
            get { return _DirectorioDuplicados; }
            set { _DirectorioDuplicados = value; }
        }

        [MapeoColumna("ProcesarTifMultipaginas")]
        public bool ProcesarTiffMultipaginas
        {
            get { return _ProcesarTiffMultipaginas; }
            set { _ProcesarTiffMultipaginas = value; }
        }


        [MapeoColumna()]
        public string DirectorioOrigen
        {
            get { return _DirectorioOrigen; }
            set { _DirectorioOrigen = value; }
        }

       
        [MapeoColumna()]
        public string TipoArchivo
        {
            get { return _TipoArchivo; }
            set { _TipoArchivo = value; }
        }

        
        [MapeoColumna()]
        public string DirectorioDestino
        {
            get { return _DirectorioDestino; }
            set { _DirectorioDestino = value; }
        }

        [MapeoColumna()]
        public string DirectorioTemp
        {
            get { return _DirectorioTemp; }
            set { _DirectorioTemp = value; }
        }


        [MapeoColumna()]
        public string DirectorioErrores
        {
            get { return _DirectorioErrores; }
            set { _DirectorioErrores = value; }
        }

       
        [MapeoColumna()]
        public int IdDocumento
        {
            get { return _IdDocumento; }
            set { _IdDocumento = value; }
        }
        
    }
}
