﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;

namespace GestionDigitalCore.CoreObjects
{
    [MapeoClase()]
    public class ConfiguracionFTP: ConfiguracionBase
    {
        private string _FtpServer;
        private string _FtpUsuario;
        private string _FtpPassword;
        private bool _RequiereBackup;
        private string _BackUpDirectory;
        private bool _RequiereCompresiónZIP;
        private string _FTPDirectorioBase;
        private string _DirectorioOrigen;
        private bool _RacionarAnchoDeBanda;
        private int _RacionarDesde;
        private int _RacionarHasta;

        [MapeoColumna()]
        public string FtpServer
        {
            get { return _FtpServer; }
            set { _FtpServer = value; }
        }
       
        [MapeoColumna()]
        public string FtpUsuario
        {
            get { return _FtpUsuario; }
            set { _FtpUsuario = value; }
        }
       
        [MapeoColumna()]
        public string FtpPassword
        {
            get { return _FtpPassword; }
            set { _FtpPassword = value; }
        }

        
        [MapeoColumna()]
        public bool RequiereBackup
        {
            get { return _RequiereBackup; }
            set { _RequiereBackup = value; }
        }
        
        [MapeoColumna()]
        public string BackUpDirectory
        {
            get { return _BackUpDirectory; }
            set { _BackUpDirectory = value; }
        }
        
        [MapeoColumna()]
        public bool RequiereCompresionZIP
        {
            get { return _RequiereCompresiónZIP; }
            set { _RequiereCompresiónZIP = value; }
        }

        private bool _EliminarArchivoOriginal;
        [MapeoColumna()]
        public bool EliminarArchivoOriginal
        {
            get { return _EliminarArchivoOriginal; }
            set { _EliminarArchivoOriginal = value; }
        }

        
        [MapeoColumna()]
        public string FTPDirectorioBase
        {
          get { return _FTPDirectorioBase; }
          set { _FTPDirectorioBase = value; }
        }
        
        [MapeoColumna()]
        public string DirectorioOrigen
        {
            get { return _DirectorioOrigen; }
            set { _DirectorioOrigen = value; }
        }
        
        [MapeoColumna()]
        public bool RacionarAnchoDeBanda
        {
            get { return _RacionarAnchoDeBanda; }
            set { _RacionarAnchoDeBanda = value; }
        }
       
        [MapeoColumna()]
        public int RacionarDesde
        {
            get { return _RacionarDesde; }
            set { _RacionarDesde = value; }
        }
        
        [MapeoColumna()]
        public int RacionarHasta
        {
            get { return _RacionarHasta; }
            set { _RacionarHasta = value; }
        }


    }
}
