﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;

namespace GestionDigitalCore.CoreObjects
{
    [MapeoClase()]
    public class Documento : DomainObject
    {

        private string _Nombre;
        private string _PathImage;
        private List<Campo> _MisCampos;
        private int _Orden;
        private bool _Activo;


        [MapeoColumna()]
        public string Nombre
        {
            get { return _Nombre; }
            set { _Nombre = value; }
        }

        [MapeoColumna()]
        public string PathImage
        {
            get { return _PathImage; }
            set { _PathImage = value; }
        }


        
        [MapeoColumna()]
        public int Orden
        {
            get { return _Orden; }
            set { _Orden = value; }
        }

        [MapeoColumna()]
        public bool Activo
        {
            get { return _Activo; }
            set { _Activo = value; }
        }

        public List<Campo> MisCampos
        {
            get { return _MisCampos; }
            set { _MisCampos = value; }
        }
    }
}
