﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;

namespace GestionDigitalCore.CoreObjects
{
    [MapeoClase()]
    public class LogFTP : DomainObject
    {
        private DateTime _FechaInicio;
        private DateTime _FechaFin;
        private int _IdConfiguracionFTP;
        private int _ArchivosProcesados;
        private int _Errores;


        [MapeoColumna()]
        public DateTime FechaInicio
        {
            get { return _FechaInicio; }
            set { _FechaInicio = value; }
        }
        
        [MapeoColumna()]
        public DateTime FechaFin
        {
            get { return _FechaFin; }
            set { _FechaFin = value; }
        }
       
        [MapeoColumna()]
        public int IdConfiguracionFTP
        {
            get { return _IdConfiguracionFTP; }
            set { _IdConfiguracionFTP = value; }
        }
        
        [MapeoColumna()]
        public int ArchivosProcesados
        {
            get { return _ArchivosProcesados; }
            set { _ArchivosProcesados = value; }
        }
        
        [MapeoColumna()]
        public int Errores
        {
            get { return _Errores; }
            set { _Errores = value; }
        }

    }
}
