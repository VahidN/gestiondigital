﻿using System;
using System.Collections.Generic;
using System.Text;
using GestionDigitalCore.CoreObjects;
using DataAccess;
using System.Data;

namespace GestionDigitalCore.CoreBussines
{
    public class BBLogFTP : DataAccess<LogFTP>
    {

        protected override void ObtenerParametrosParaSQL(ref string sqlBase, LogFTP m)
        {
            SetValueToSQL(ref sqlBase, "FechaInicio", m.FechaInicio);
            SetValueToSQL(ref sqlBase, "FechaFin", m.FechaFin);
            SetValueToSQL(ref sqlBase, "IdConfiguracionFTP", m.IdConfiguracionFTP);
            SetValueToSQL(ref sqlBase, "ArchivosProcesados", m.ArchivosProcesados);
            SetValueToSQL(ref sqlBase, "Errores", m.Errores);
            SetValueToSQL(ref sqlBase, "IdLogFTP", m.ID);
        }



        public override void Eliminar(int IdLogFTP)
        {
            LogFTP logftp = getById(IdLogFTP);
            BBLogDetalleFTP BBL = new BBLogDetalleFTP();
            BBL.Eliminar(logftp);
            base.Eliminar(logftp);

        }
        public override void Eliminar(LogFTP m)
        {
            BBLogDetalleFTP BBL = new BBLogDetalleFTP();
            BBL.Eliminar(m);
            base.Eliminar(m);

        }
        public override LogFTP GetFromReader(IDataReader Reader)
        {
            LogFTP result = new LogFTP();

            result.ID = (int)Reader.GetValue(0);
            result.FechaInicio = (DateTime)Reader.GetValue(1);
            result.FechaFin = (DateTime)Reader.GetValue(2);
            result.IdConfiguracionFTP = (int)Reader.GetValue(3);
            result.ArchivosProcesados = (int)Reader.GetValue(4);
            result.Errores = (int)Reader.GetValue(5);
            return result;
        }



        public List<LogFTP> GetAll(int IdConfiguracion, DateTime? Desde, DateTime? Hasta)
        {
            string Where = "";
            if (IdConfiguracion > 0)
            {
                Where += "IdConfiguracionFTP = " + IdConfiguracion.ToString();
            }
            if (Desde!=null)
            {
                if (Where.Length > 0) Where += " and ";
                Where += "FechaInicio >= '" + Desde.Value.ToString("MM/dd/yyyy 00:00:00") + " AM'"; 
            }
            if (Hasta != null)
            {
                if (Where.Length > 0) Where += " and ";
                Where += "FechaInicio <= '" + Hasta.Value.ToString("MM/dd/yyyy 11:59:00") + " PM'";
            }
            return GetAll( Where);
        }

        public void DeleteOldValues()
        {
            BBLogDetalleFTP BBDet = new BBLogDetalleFTP();
            BBDet.DeleteOldValues();
            this.EjecutarInsertUpdate("delete " + this.tableName + " Where FechaInicio <= '" + DateTime.Now.AddDays(-4).ToString("MM/dd/yyyy 11:59:00") + "'");

        }
    }
}
