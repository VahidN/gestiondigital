﻿using System;
using System.Collections.Generic;
using System.Text;
using GestionDigitalCore.CoreObjects;
using DataAccess;
using System.Data;
using ToolBox;
using FSO_NH.log4Net;
using System.IO;

namespace GestionDigitalCore.CoreBussines
{
    public class BBConfiguracionOCR : DataAccess<ConfiguracionOCR>
    {

        protected override void  ObtenerParametrosParaSQL(ref string sqlBase, ConfiguracionOCR m)
        {
 	
            SetValueToSQL(ref sqlBase, "Nombre", m.Nombre);
            SetValueToSQL(ref sqlBase, "DirectorioOrigen", m.DirectorioOrigen);
            SetValueToSQL(ref sqlBase, "TipoArchivo", m.TipoArchivo);
            SetValueToSQL(ref sqlBase, "DirectorioDestino", m.DirectorioDestino);
            SetValueToSQL(ref sqlBase, "DirectorioErrores", m.DirectorioErrores);
            SetValueToSQL(ref sqlBase, "DirectorioTemp", m.DirectorioTemp);
            SetValueToSQL(ref sqlBase, "ProcesarTifMultipaginas", m.ProcesarTiffMultipaginas );

            SetValueToSQL(ref sqlBase, "EmailAlerta", m.EmailAlerta);
            SetValueToSQL(ref sqlBase, "SMTPServer", m.SMTPConfig.SMTPServer);
            SetValueToSQL(ref sqlBase, "SMTPUser", m.SMTPConfig.SMTPUser);
            SetValueToSQL(ref sqlBase, "SMTPPassword", setMailPassword(m.SMTPConfig.SMTPPassword));

            SetValueToSQL(ref sqlBase, "DefaultConfig", m.DefaultConfig);
            SetValueToSQL(ref sqlBase, "IdConfiguracionOCR", m.ID);
            SetValueToSQL(ref sqlBase, "IdDocumento", m.IdDocumento);
            SetValueToSQL(ref sqlBase, "DirectorioDuplicados", m.DirectorioDuplicados);
        }

        public override ConfiguracionOCR getNew()
        {
            ConfiguracionOCR n = new ConfiguracionOCR();
            n.SMTPConfig = new FSO.NH.WEB.SMTPConnParameters();
            return n;
        }

        protected override void ValidarEliminacion(ConfiguracionOCR Obj)
        {
            if (Obj.DefaultConfig)
                throw new Exception("No es posible eliminar la configuración por defecto");
        }
        protected override void Validar(ConfiguracionOCR m)
        {
            if (string.IsNullOrEmpty(m.Nombre))
                throw new Exception("Debe indicar un nombre para la configuración");
            if (!Directory.Exists(m.DirectorioDestino))
                throw new Exception("El Directorio destino no existe");
            if (!Directory.Exists(m.DirectorioErrores))
                throw new Exception("El Directorio de Errores no existe");
            if (!Directory.Exists(m.DirectorioOrigen))
                throw new Exception("El Directorio Origen no existe");
            if (!Directory.Exists(m.DirectorioTemp))
                throw new Exception("El Directorio Temporal no existe");
            

            if (m.DefaultConfig)
            {
                EjecutarInsertUpdate("Update " + tableName + " set DefaultConfig = 0 where IdConfiguracionOCR <> " + m.ID);
            }

        }
        public ConfiguracionOCR GetDefaultConfiguration()
        {

            List<ConfiguracionOCR> Lista = GetAll( "DefaultConfig = 1");
            if (Lista.Count > 0)
                return Lista[0];
            else
                throw new FSOException("Registro Configuración por defecto OCR no encontrado");

        }
        private string GetMailPassword(string encpassword)
        {

            try
            {
                TripleDES_Encriptador Encriptador = new TripleDES_Encriptador(@"CMom9EfyoR7cu4qabv2PyUC9SuyWrN9V", @"xRnyk/GTKMI=");
                return Encriptador.DecryptString(encpassword);
            }
            catch (Exception)
            {
                return encpassword;
            }

        }
        private string setMailPassword(string encpassword)
        {

            try
            {
                TripleDES_Encriptador Encriptador = new TripleDES_Encriptador(@"CMom9EfyoR7cu4qabv2PyUC9SuyWrN9V", @"xRnyk/GTKMI=");
                return Encriptador.EncryptString(encpassword);
            }
            catch (Exception)
            {
                return encpassword;
            }

        }
        public ConfiguracionOCR GetConfigurationByName(string name)
        {
            List<ConfiguracionOCR> Lista = GetAll( "Nombre = '" + name + "'");
            if (Lista.Count > 0)
                return Lista[0];
            else
                throw new FSOException("Registro COnfiguración no encontrado");
        }
        public override ConfiguracionOCR GetFromReader(IDataReader Reader)
        {
            ConfiguracionOCR result = new ConfiguracionOCR();

            result.ID = (int)Reader.GetValue(0);
            result.Nombre = (string)Reader.GetValue(1);
            result.DirectorioOrigen = (string)Reader.GetValue(2);
            result.DirectorioDestino = (string)Reader.GetValue(3);
            result.DirectorioErrores = (string)Reader.GetValue(4);
            result.ProcesarTiffMultipaginas =  (bool)Reader.GetValue(5);
            result.DefaultConfig = (bool)Reader.GetValue(6);
            result.SMTPConfig = new FSO.NH.WEB.SMTPConnParameters();
            result.SMTPConfig.SMTPServer = (string)Reader.GetValue(7);
            result.SMTPConfig.SMTPUser = (string)Reader.GetValue(8);
            result.SMTPConfig.SMTPPassword = GetMailPassword((string)Reader.GetValue(9));
            result.TipoArchivo = (string)Reader.GetValue(10).ToString();
            result.IdDocumento = (int)Reader.GetValue(11);
            result.EmailAlerta = (string)Reader.GetValue(12).ToString();
            result.DirectorioDuplicados = (string)Reader.GetValue(13).ToString();
            result.DirectorioTemp = (string)Reader.GetValue(14).ToString();
            return result;
        }


    }
}
