﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;
using GestionDigitalCore.CoreObjects;
using System.Data;

namespace GestionDigitalCore.CoreBussines
{
    public class BBCampo : DataAccess<Campo>
    {

        protected override void ObtenerParametrosParaSQL(ref string sqlBase, Campo m)
        {

            SetValueToSQL(ref sqlBase, "IdCampo", m.ID);
            SetValueToSQL(ref sqlBase, "IdDocumento", m.IdDocumento);
            SetValueToSQL(ref sqlBase, "Nombre", m.Nombre);
            SetValueToSQL(ref sqlBase, "Arriba", m.YInicial);
            SetValueToSQL(ref sqlBase, "Izquierda", m.XInicial);
            SetValueToSQL(ref sqlBase, "Abajo", m.Alto);
            SetValueToSQL(ref sqlBase, "Derecha", m.Ancho);
            SetValueToSQL(ref sqlBase, "GradoRotacion", m.GradoRotacion);
            SetValueToSQL(ref sqlBase, "EsNombreDocumento", m.EsNombreDocumento);
            SetValueToSQL(ref sqlBase, "TipoCampo", (int)m.TipoCampo);
            SetValueToSQL(ref sqlBase, "EsIdentificador",m.EsIdentificador );
            SetValueToSQL(ref sqlBase, "ValorIdentificacion", m.ValorIdentificacion);
            SetValueToSQL(ref sqlBase, "Orden", m.Orden);
            SetValueToSQL(ref sqlBase, "MejorarImagen", m.MejorarImagen);
            SetValueToSQL(ref sqlBase, "TipoPreprocesamiento", m.TipoPreprocesamiento);
            SetValueToSQL(ref sqlBase, "Longitud", m.Longitud);
            SetValueToSQL(ref sqlBase, "TipoDato", m.TipoDato);

        }

        protected override void Validar(Campo m)
        {
            if (m == null)
            {
                throw new Exception("No hay datos para almacenar, objeto nulo");
            }
            if (string.IsNullOrEmpty(m.Nombre))
            {
                throw new Exception("El Nombre no puede ser nulo");
            }
            if (m.YInicial < 0)
            {
                throw new Exception("El Valor Y no puede ser negativo");
            }
            if (m.XInicial < 0)
            {
                throw new Exception("El Valor X no puede ser negativo");
            }
            if (m.Alto < 0)
            {
                throw new Exception("El Valor Alto es inválido");
            }
            if (m.Ancho< 0)
            {
                throw new Exception("El Valor Ancho es inválido");
            }
            if (m.Orden < 0)
            {
                throw new Exception("El Orden es inválido");
            }
            
            if (m.EsIdentificador && string.IsNullOrEmpty( m.ValorIdentificacion))
            {
                throw new Exception("Si el Campo es identificatorio, debera incluir una descripción verificatoria");
            }
            Campo ElOtro = getByNombreYDocumento(m.Nombre, m.ID);

            if(ElOtro != null && ElOtro.ID!=m.ID)
            {
                throw new Exception("Otro Campo con ese nombre ya pertenece al documento actual.");
            }
        }





        public override Campo GetFromReader(IDataReader Reader)
        {
            Campo result = new Campo();

            result.ID = (int)Reader.GetValue(0);
            result.IdDocumento = (int)Reader.GetValue(1);
            result.Nombre = (string)Reader.GetValue(2);
            result.YInicial = (int)Reader.GetValue(3);
            result.XInicial= (int)Reader.GetValue(4);
            result.Alto = (int)Reader.GetValue(5);
            result.Ancho = (int)Reader.GetValue(6);
            result.TipoCampo = (eTipoCampo)(int)Reader.GetValue(7);
            result.EsIdentificador = (bool)Reader.GetValue(8);
            result.ValorIdentificacion=(string)Reader.GetValue(9);
            result.Orden = (int)Reader.GetValue(10);
            result.MejorarImagen = (bool)Reader.GetValue(11);
            result.GradoRotacion = (int)Reader.GetValue(12);
            result.EsNombreDocumento = (bool)Reader.GetValue(13);
            result.TipoPreprocesamiento = (int)Reader.GetValue(14);
            //para evitar que se rompa con las configuracines creadas antes de agregar este concepto
            if (!Reader.GetValue(15).Equals(System.DBNull.Value))
                result.Longitud = (int)Reader.GetValue(15);
            else
                result.Longitud = 10;
            
            if (!Reader.GetValue(16).Equals(System.DBNull.Value))
                result.TipoDato = (string)Reader.GetValue(16);
            else
                result.TipoDato = "N";

            return result;
        }
        public  Campo getByNombreYDocumento(String Nombre, int IdDocumento)
        {
            List<Campo> list = base.GetAll("Nombre = '"+Nombre+"' and IdDocumento = " + IdDocumento.ToString());
            if (list.Count > 0) return list[0];
            else return null;

        }
        public override void Eliminar(Campo m)
        {            
            this.EjecutarConsulta("Delete ValorArchivo where IdCampo = " + m.ID.ToString());            
            base.Eliminar(m);
        }

        public List<Campo> GetByDocumento(int IdDocumento)
        {
            return GetAll("IdDocumento = " + IdDocumento.ToString(),"Orden asc");
        }

        internal void SaveByDocumento(Documento m)
        {
            string idToDelete="";
            if (m.MisCampos != null)
            {
                foreach (Campo c in m.MisCampos)
                {
                    
                    c.IdDocumento = m.ID;
                    Campo Saved =Guardar(c);
                    idToDelete += Saved.ID.ToString() + ",";
                }
            }
            if (!string.IsNullOrEmpty(idToDelete))
            {
                List<Campo> AEliminar = GetListaAEliminar(idToDelete.Substring(0, idToDelete.Length - 1), m.ID);
                foreach (Campo c in AEliminar)
                {
                    Eliminar(c);
                }
            }
        }

        private List<Campo> GetListaAEliminar(string IdCamposInDoc, int IdDoc)
        {

            return GetAll("IdCampo not in (" + IdCamposInDoc + ") and IdDocumento = " +IdDoc.ToString());
        }

        internal void ValidarByDocumento(Documento doc)
        {
            foreach (Campo c in doc.MisCampos)
            {
                Validar(c);
            }
        }

        internal void DeleteByDocumento(int IdDocumento)
        {
            List<Campo> listado = GetByDocumento(IdDocumento);
            foreach (Campo c in listado)
            {
                Eliminar(c);
            }
        }
    }
}
