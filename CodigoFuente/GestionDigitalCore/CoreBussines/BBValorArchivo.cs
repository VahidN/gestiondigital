﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;
using GestionDigitalCore.CoreObjects;
using System.Data;

namespace GestionDigitalCore.CoreBussines
{
    public class BBValorArchivo:DataAccess<ValorArchivo>
    {

        protected override void ObtenerParametrosParaSQL(ref string sqlBase, ValorArchivo m )
        {
            SetValueToSQL(ref sqlBase, "IdValorArchivo", m.ID);
            SetValueToSQL(ref sqlBase, "IdArchivo", m.idArchivo);
            SetValueToSQL(ref sqlBase, "IdCampo", m.IdCampo);
            SetValueToSQL(ref sqlBase, "Valor", m.Valor);
            SetValueToSQL(ref sqlBase, "Error", m.Error);
        }


        protected void Validar(ValorArchivo m)
        {
            if (String.IsNullOrEmpty(m.IdCampo.ToString()))
            {
                throw new Exception("Debe indicar un campo");
            }
        }


        public override ValorArchivo GetFromReader(IDataReader Reader)
        {
            ValorArchivo result = new ValorArchivo();

            result.ID = (int)Reader.GetValue(0);
            result.idArchivo = (int)Reader.GetValue(1);
            result.IdCampo = (int)Reader.GetValue(2);
            result.Valor = (string)Reader.GetValue(3);
            result.Error = (string)Reader.GetValue(4);


            return result;
        }



        internal List<ValorArchivo> getByArchivo(int IdArchivo)
        {
            return GetAll("IdArchivo = " + IdArchivo.ToString());
        }

        internal void EliminarPorArchivo(Archivo m)
        {
            EjecutarInsertUpdate("delete " + tableName + " where IdArchivo = " + m.ID.ToString());
        }

        public void DeleteOldValues()
        {
            this.EjecutarInsertUpdate("delete " + this.tableName + "  Where IdArchivo in ( select  idarchivo from archivo where NombreModificado <> '' or (FechaProceso <= '" + DateTime.Now.AddDays(-10).ToString("MM/dd/yyyy 11:59:00") + "' and (Error = '' or Error like '%Archivo ya existente%')))");
        }
    }
}
