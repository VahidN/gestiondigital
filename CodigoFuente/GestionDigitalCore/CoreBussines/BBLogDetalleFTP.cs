﻿using System;
using System.Collections.Generic;
using System.Text;
using GestionDigitalCore.CoreObjects;
using DataAccess;
using System.Data;

namespace GestionDigitalCore.CoreBussines
{
    public class BBLogDetalleFTP : DataAccess<LogDetalleFTP>
    {

        protected override void ObtenerParametrosParaSQL(ref string sqlBase, LogDetalleFTP m)
        {
            SetValueToSQL(ref sqlBase, "IdLogFTP", m.IdLogFTP);
            SetValueToSQL(ref sqlBase, "FileName", m.FileName);
            SetValueToSQL(ref sqlBase, "FechaInicio", m.FechaInicio);
            SetValueToSQL(ref sqlBase, "FechaFin", m.FechaFin);
            SetValueToSQL(ref sqlBase, "TiempoSubida", m.TiempoSubida);
            SetValueToSQL(ref sqlBase, "Error", m.Error);
            SetValueToSQL(ref sqlBase, "IdLogDetalleFTP", m.ID);
            SetValueToSQL(ref sqlBase, "ByteSize", m.ByteSize);
        }

        public void Eliminar(LogFTP m)
        {
            base.EjecutarInsertUpdate("delete LogDetalleFTP where IdLogFTP = " + m.ID.ToString());
        }
        public override LogDetalleFTP GetFromReader(IDataReader Reader)
        {
            LogDetalleFTP result = new LogDetalleFTP();

            result.ID = (int)Reader.GetValue(0);
            result.IdLogFTP = (int)Reader.GetValue(1);
            result.FileName = (string)Reader.GetValue(2);
            result.FechaInicio = (DateTime)Reader.GetValue(3);
            result.FechaFin = (DateTime)Reader.GetValue(4);
            result.TiempoSubida = (long)Reader.GetValue(5);
            result.Error = (string)Reader.GetValue(6);
            result.ByteSize = (long)Reader.GetValue(7);
            return result;
        }

        public List<LogDetalleFTP> GetByLogId(int IdLog)
        {
            return GetAll( "IdLogFTP = " + IdLog.ToString());
        }

        internal void DeleteOldValues()
        {
            this.EjecutarInsertUpdate("delete " + this.tableName + "  Where IdLogFTP in (select idlogFTP from LogFTP Where FechaInicio <= '" + DateTime.Now.AddDays(-4).ToString("MM/dd/yyyy 11:59:00") + "')");

        }
    }
}
