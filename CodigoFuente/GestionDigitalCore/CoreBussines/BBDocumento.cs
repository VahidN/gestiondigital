﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;
using GestionDigitalCore.CoreObjects;
using System.Data;

namespace GestionDigitalCore.CoreBussines
{
    public class BBDocumento : DataAccess<Documento>
    {


        protected override void ObtenerParametrosParaSQL(ref string sqlBase, Documento doc)
        {
            SetValueToSQL(ref sqlBase, "IdDocumento", doc.ID);
            SetValueToSQL(ref sqlBase, "Nombre", doc.Nombre);
            SetValueToSQL(ref sqlBase, "PathImage", doc.PathImage);
            SetValueToSQL(ref sqlBase, "Activo", doc.Activo);
            SetValueToSQL(ref sqlBase, "Orden", doc.Orden);

            
        }
        public override Documento getNew()
        {
            Documento T = new Documento();
            T.Activo = true;
            T.MisCampos = new List<Campo>();
            return T;
        }
        protected override void AccionesPostGrabacion(Documento m)
        {
            new BBCampo().SaveByDocumento(m);
        }
        protected override void Validar(Documento doc)
        {
            if (string.IsNullOrEmpty(doc.Nombre))
            {
                throw new Exception("Debe indicar un nombre para el documento");
            }
            if (doc.MisCampos.Count==0)
            {
                throw new Exception("No hay una lista de campos para este documento");
            }
            if (string.IsNullOrEmpty(doc.PathImage))
            {
                throw new Exception("No hay una imagen asociada para este documento");
            }
            BBCampo BBC = new BBCampo();
            BBC.ValidarByDocumento(doc);
        }

 
        public override List<Documento> GetAll()
        {
            return base.GetAll("Activo = 1","Orden asc" );
        }

        public override void Eliminar(Documento m)
        {
            this.EjecutarConsulta("Delete ValorArchivo where IdArchivo in (Select IdArchivo from Archivo where IdDocumento = " + m.ID.ToString()+")");
            this.EjecutarConsulta("Delete Archivo where IdDocumento = " + m.ID.ToString());
            BBCampo BBC = new BBCampo();
            BBC.DeleteByDocumento(m.ID);

            base.Eliminar(m);
        }
        public override Documento GetFromReader(IDataReader Reader)
        {
            Documento result = new Documento();

            result.ID = (int)Reader.GetValue(0);
            result.Nombre = (string)Reader.GetValue(1);
            result.PathImage = (string)Reader.GetValue(2);
            result.MisCampos = new BBCampo().GetByDocumento(result.ID);
            result.Activo = ((bool)Reader.GetValue(3));
            result.Orden = (int)Reader.GetValue(4);
            return result;
        }



    }
}
