using System;
using System.Collections.Generic;
using System.Text;
using GestionDigitalCore.CoreObjects;
using System.Data;

namespace DataAccess
{
    public class BBParametro : DataAccess<Parametro>
    {
        public bool Lazy = true;

        
        #region IDataAccess<Parametro> Members


        protected override void ObtenerParametrosParaSQL(ref string sqlBase, Parametro m )
        {
            SetValueToSQL(ref sqlBase, "IdParametro", m.ID);
            SetValueToSQL(ref sqlBase, "Nombre", m.Nombre);
            SetValueToSQL(ref sqlBase, "Valor", m.Valor);
        }

        public override List<Parametro> GetAll()
        {
            return base.GetAll();
        }

        public override Parametro getById(int ID)
        {
            return base.getById(ID);
        }

        public Parametro getByNombre(string Nombre)
        {
            List<Parametro> Listado = GetAll("Nombre = '" + Nombre + "'");
            if (Listado.Count > 0)
                return Listado[0];
            else
                throw new Exception("Parametro Inexistente");
            
        }
        public Parametro getValue(string Nombre, bool DefaultValue)
        {
            return getValue(Nombre, DefaultValue.ToString().ToLower());
        }
        public Parametro getValue(string Nombre, int DefaultValue)
        {
            return getValue(Nombre, DefaultValue.ToString());
        }
        public Parametro getValue(string Nombre, decimal DefaultValue)
        {
            return getValue(Nombre, DefaultValue.ToString());
        }
        public Parametro getValue(string Nombre, string DefaultValue)
        {

            Parametro MyRegistro = getByNombre(Nombre);
            if (MyRegistro.ID == 0)
            {
                MyRegistro = setValue(Nombre, DefaultValue);
            }
            return MyRegistro;
        }

        public Parametro setValue(string Nombre, bool Value)
        {
            return setValue(Nombre, Value.ToString().ToLower());
        }
        public Parametro setValue(string Nombre, int Value)
        {
            return setValue(Nombre, Value.ToString());
        }
        public Parametro setValue(string Nombre, decimal Value)
        {
            return setValue(Nombre, Value.ToString());
        }
        public Parametro setValue(string Nombre, string Value)
        {
            Parametro MyRegistro = getByNombre(Nombre);

            MyRegistro.Nombre = Nombre;
            MyRegistro.Valor = Value;

            Guardar(MyRegistro);
            return MyRegistro;
        }
        public override Parametro GetFromReader(IDataReader Reader)
        {
            Parametro registro = new Parametro();
            registro.ID = (int)Reader.GetValue(0);
            registro.Nombre= Reader.GetValue(1).ToString();
            registro.Valor = Reader.GetValue(2).ToString().Replace("@@@", "\\");

            CargarObjetosHijos(ref registro);
            return registro;
        }


        #endregion
    }
}
