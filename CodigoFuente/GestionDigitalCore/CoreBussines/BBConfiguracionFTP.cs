﻿using System;
using System.Collections.Generic;
using System.Text;
using GestionDigitalCore.CoreObjects;
using DataAccess;
using ToolBox;
using System.Data;
using FSO_NH.log4Net;
using System.IO;

namespace GestionDigitalCore.CoreBussines
{
    public class BBConfiguracionFTP : DataAccess<ConfiguracionFTP>
    {

        protected override void ObtenerParametrosParaSQL(ref string sqlBase, ConfiguracionFTP m)
        {

            SetValueToSQL(ref sqlBase, "Nombre", m.Nombre);
            SetValueToSQL(ref sqlBase, "BackupDirectory", m.BackUpDirectory);
            SetValueToSQL(ref sqlBase, "EliminarArchivoOriginal", m.EliminarArchivoOriginal);
            SetValueToSQL(ref sqlBase, "EmailAlerta", m.EmailAlerta);
            SetValueToSQL(ref sqlBase, "FTPDirectorioBase", m.FTPDirectorioBase);
            SetValueToSQL(ref sqlBase, "FTPServer", m.FtpServer);
            SetValueToSQL(ref sqlBase, "FTPUsuario", m.FtpUsuario);
            SetValueToSQL(ref sqlBase, "RequiereBackup", m.RequiereBackup);
            SetValueToSQL(ref sqlBase, "RequiereCompresionZIP", m.RequiereCompresionZIP );
            SetValueToSQL(ref sqlBase, "SMTPServer", m.SMTPConfig.SMTPServer);
            SetValueToSQL(ref sqlBase, "SMTPUser", m.SMTPConfig.SMTPUser);
            SetValueToSQL(ref sqlBase, "SMTPPassword", setMailPassword(m.SMTPConfig.SMTPPassword));
            SetValueToSQL(ref sqlBase, "DirectorioOrigen", m.DirectorioOrigen);
            SetValueToSQL(ref sqlBase, "RacionarAnchoDeBanda", m.RacionarAnchoDeBanda);
            SetValueToSQL(ref sqlBase, "RacionarDesde", m.RacionarDesde);
            SetValueToSQL(ref sqlBase, "RacionarHasta", m.RacionarHasta);
            SetValueToSQL(ref sqlBase, "DefaultConfig", m.DefaultConfig);
            SetValueToSQL(ref sqlBase, "FTPPassword", m.FtpPassword);
            SetValueToSQL(ref sqlBase, "IdConfiguracionFTP", m.ID);
        }

        public override ConfiguracionFTP getNew()
        {
            ConfiguracionFTP n = new ConfiguracionFTP();
            n.SMTPConfig = new FSO.NH.WEB.SMTPConnParameters();
            return n;
        }
        protected override void ValidarEliminacion(ConfiguracionFTP Obj)
        {
            if (Obj.DefaultConfig)
                throw new Exception("No es posible eliminar la configuración por defecto");
        }
        protected override void Validar(ConfiguracionFTP m)
        {
            if (string.IsNullOrEmpty(m.Nombre))
                throw new Exception("Debe indicar un nombre para la configuración");
            if (m.RequiereBackup && !Directory.Exists(m.BackUpDirectory))
                throw new Exception("El Directorio de backup seleccionado no existe");
            if(!Directory.Exists( m.DirectorioOrigen))
                throw new Exception("El Directorio de Origen seleccionado no existe");
            if (m.DefaultConfig)
            {
                EjecutarInsertUpdate("Update " + tableName + " set DefaultConfig = 0 where IdConfiguracionFTP <> " + m.ID);
            }

        }
        public ConfiguracionFTP GetDefaultConfiguration()
        {

            List<ConfiguracionFTP> Lista= GetAll( "DefaultConfig = 1");
            if (Lista.Count > 0)
                return Lista[0];
            else
                throw new FSOException("Registro COnfiguración por defecto FTP no encontrado");

        }
        private string GetMailPassword(string encpassword)
        {

            try
            {
                TripleDES_Encriptador Encriptador = new TripleDES_Encriptador(@"CMom9EfyoR7cu4qabv2PyUC9SuyWrN9V", @"xRnyk/GTKMI=");
                return Encriptador.DecryptString(encpassword); 
            }
            catch (Exception)
            {
                return encpassword;
            }
			
        }

        private string setMailPassword(string encpassword)
        {

            try
            {
                TripleDES_Encriptador Encriptador = new TripleDES_Encriptador(@"CMom9EfyoR7cu4qabv2PyUC9SuyWrN9V", @"xRnyk/GTKMI=");
                return Encriptador.EncryptString(encpassword);
            }
            catch (Exception)
            {
                return encpassword;
            }

        }
        public ConfiguracionFTP GetConfigurationByName(string name)
        {
            List<ConfiguracionFTP> Lista = GetAll( "Nombre = '" + name  + "'");
            if (Lista.Count > 0)
                return Lista[0];
            else
                throw new FSOException("Registro COnfiguración no encontrado");
        }
        public override ConfiguracionFTP GetFromReader(IDataReader Reader)
        {
            ConfiguracionFTP result = new ConfiguracionFTP();

            result.ID = (int)Reader.GetValue(0);
            result.Nombre = (string)Reader.GetValue(1);
            result.BackUpDirectory = (string)Reader.GetValue(2);
            result.EliminarArchivoOriginal = (bool)Reader.GetValue(3) ;
            result.EmailAlerta = (string)Reader.GetValue(4);
            result.FTPDirectorioBase = (string)Reader.GetValue(5);
            result.FtpServer = (string)Reader.GetValue(6);
            result.FtpUsuario = (string)Reader.GetValue(7);
            result.RequiereBackup = (bool)Reader.GetValue(8);
            result.RequiereCompresionZIP = (bool)Reader.GetValue(9);
            result.SMTPConfig = new FSO.NH.WEB.SMTPConnParameters();
            result.SMTPConfig.SMTPServer = (string)Reader.GetValue(10);
            result.SMTPConfig.SMTPUser = (string)Reader.GetValue(11);
            result.SMTPConfig.SMTPPassword = GetMailPassword((string)Reader.GetValue(12));
            result.DirectorioOrigen = (string)Reader.GetValue(13);
            result.RacionarAnchoDeBanda = (bool)Reader.GetValue(14);
            result.RacionarDesde = (int)Reader.GetValue(15);
            result.RacionarHasta = (int)Reader.GetValue(16);
            result.DefaultConfig = (bool)Reader.GetValue(17);
            result.FtpPassword = (string)Reader.GetValue(18);
            return result;
        }

    }
}
