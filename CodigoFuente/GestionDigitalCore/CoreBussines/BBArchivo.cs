﻿using System;
using System.Collections.Generic;
using System.Text;
using DataAccess;
using GestionDigitalCore.CoreObjects;
using System.Data;

namespace GestionDigitalCore.CoreBussines
{
    public class BBArchivo: DataAccess<Archivo>
    {



        protected override void ObtenerParametrosParaSQL(ref string sqlBase, Archivo m)
        {

            SetValueToSQL(ref sqlBase, "IdArchivo", m.ID);
            SetValueToSQL(ref sqlBase, "IdDocumento", m.IdDocumento);
            SetValueToSQL(ref sqlBase, "NombreOriginal", m.NombreOriginal);
            SetValueToSQL(ref sqlBase, "NombreModificado", m.NombreModificado);
            SetValueToSQL(ref sqlBase, "TamanoOriginal", m.TamañoOriginal);
            SetValueToSQL(ref sqlBase, "TamanoModificado", m.TamañoModificado);
            SetValueToSQL(ref sqlBase, "ResolucionHorizontal", m.ResolucionHorizontal);
            SetValueToSQL(ref sqlBase, "ResolucionVertical", m.ResolucionVertical);
            SetValueToSQL(ref sqlBase, "PathOrigen", m.PathOrigen);
            SetValueToSQL(ref sqlBase, "PathDestino", m.PathDestino);
            SetValueToSQL(ref sqlBase, "FechaProceso", m.FechaProceso);
            SetValueToSQL(ref sqlBase, "UsuarioProceso", m.UsuarioProceso);
            SetValueToSQL(ref sqlBase, "Error", m.Error);
            SetValueToSQL(ref sqlBase, "CambioManual", m.CambioManual);

        }

        protected override void Validar(Archivo m)
        {
           
        }




        public override Archivo GetFromReader(IDataReader Reader)
        {
            Archivo result = new Archivo();

            result.ID = (int)Reader.GetValue(0);
            result.IdDocumento = (int)Reader.GetValue(1);
            result.NombreOriginal = (string)Reader.GetValue(2);
            result.NombreModificado = (string)Reader.GetValue(3);
            result.PathOrigen = (string)Reader.GetValue(4);
            result.PathDestino = (string)Reader.GetValue(5);
            result.FechaProceso = (DateTime)Reader.GetValue(6);
            result.UsuarioProceso = (string)Reader.GetValue(7);
            result.Error = (string)Reader.GetValue(8);
            result.TamañoOriginal = (long)Reader.GetValue(9);
            result.TamañoModificado = (long)Reader.GetValue(10);
            result.CambioManual = (bool)Reader.GetValue(11);
            if(Reader.GetValue(12)!= System.DBNull.Value )
                result.ResolucionHorizontal = (float)Reader.GetValue(12);
            if (!Reader.GetValue(13).Equals(System.DBNull.Value))
                result.ResolucionVertical = (float)Reader.GetValue(13);

            if (!LazyLoad)
            {
                BBValorArchivo BBV = new BBValorArchivo();
                result.MisValores = BBV.getByArchivo(result.ID);
            }
            return result;
        }


        public override void Eliminar(Archivo m)
        {
            BBValorArchivo BBVA = new BBValorArchivo();
            BBVA.EliminarPorArchivo(m);
            base.Eliminar(m);
        }

        public List<Archivo> getFiltered(int IdDoc, DateTime? Desde, DateTime? Hasta, bool ConError, bool CambioManual)
        {

            string SQLFiltro = "";

            if (CambioManual)
            {
                SQLFiltro = "CambioManual=1";
            }
            if (IdDoc>0)
            {
                if (SQLFiltro != "") SQLFiltro += " and ";
                SQLFiltro += "IdDocumento="+IdDoc.ToString() ;
            }
            if (ConError)
            {
                if (SQLFiltro != "") SQLFiltro += " and ";
                SQLFiltro += " Error<>'' and Error not like '%Archivo ya existente%'";
            }
            else {
                if (SQLFiltro != "") SQLFiltro += " and ";
                SQLFiltro += " (Error='' or Error like '%Archivo ya existente%') ";

            }
            if (Desde != null)
            {
                if (SQLFiltro.Length > 0) SQLFiltro += " and ";
                SQLFiltro += " FechaProceso >= '" + Desde.Value.ToString("MM/dd/yyyy 00:00:00") + " AM'";
            }
            if (Hasta != null)
            {
                if (SQLFiltro.Length > 0) SQLFiltro += " and ";
                SQLFiltro += " FechaProceso <= '" + Hasta.Value.ToString("MM/dd/yyyy 11:59:00") + " PM'";
            }

            return GetAll(SQLFiltro);
        }

        public List<Archivo> getByFileNameOriginal(string path, string NombreArchivo)
        {
            return GetAll("PathOrigen = '" + path + "' and NombreOriginal = '" + NombreArchivo + "'");
        }

        public List<Archivo> getByFileNameModificado(string path, string NombreArchivo)
        {
            return GetAll("PathDestino = '" + path + "' and NombreModificado = '" + NombreArchivo + "'");
        }

        public List<Archivo> getByDocumento(int IdDoc)
        {
            return GetAll("IdDocumento = " + IdDoc.ToString());
        }



        public List<Archivo> getByFileNameModificado(string NombreArchivo)
        {
            return GetAll("NombreModificado = '" + NombreArchivo + "'");
        }

        public void DeleteOldValues()
        {
            BBValorArchivo BBVA = new BBValorArchivo();
            BBVA.DeleteOldValues();
            this.EjecutarInsertUpdate("delete " + this.tableName + " Where NombreModificado <> '' or (FechaProceso <= '" + DateTime.Now.AddDays(-10).ToString("MM/dd/yyyy 11:59:00") + "' and (Error = '' or Error like '%Archivo ya existente%'))");

        }
    }
}
