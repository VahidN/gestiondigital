using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace LecturaCodigoBarra
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string sPlantillaSeleccionada;

        private OCRTools.OCR  OCR1 = new OCRTools.OCR (); 

        private void btnSeleccionarPlantilla_Click(object sender, EventArgs e)
        {
            //Te Declaras un Objeto de Tipo OpenFileDialog
            OpenFileDialog dialogo = new OpenFileDialog();

            dialogo.Filter = "Imagenes tif|*.tif|Imagenes jpg|*.jpg|Acrobat|*.pdf";

            //para mostrar el cuadro de seleccion de archivo hacemos asi:
            if (dialogo.ShowDialog() == DialogResult.OK)
            {
                sPlantillaSeleccionada = dialogo.FileName;
                lblPlantilla.Text = sPlantillaSeleccionada;
                OCR1.OCRType = OCRTools.OCRType.Barcode;
                OCR1.SetPictureBox(pictureBox1);
                OCR1.LoadPictureBox(sPlantillaSeleccionada);
                OCR1.PictureBox.Image.Save("a.jpg", System.Drawing.Imaging.ImageFormat.Jpeg);
                ResizeForm();

            }
        }
        
        #region Asignar Tama�o a la im�gen
        
        private void ResizeForm()
        {
            if (OCR1.PictureBox != null)
            {
                OCR1.PictureBox.ScaleImage(pnlDocument.Width, pnlDocument.Height);
            }
        }
        #endregion

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            txtZoneX.Text = OCR1.PictureBox.X.ToString();
            txtZoneY.Text = OCR1.PictureBox.Y.ToString();
            txtZoneWidth.Text = OCR1.PictureBox.Width.ToString();
            txtZoneHeight.Text = OCR1.PictureBox.Height.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Capture the user selected region of the PictureBox that the user selected with their mouse
            OCR1.BitmapImage = OCR1.PictureBox.Copy();
            OCR1.ErrorCorrection = OCRTools.ErrorCorrectionTypes.IterateBrightness;

            // Not requred, but you can check if the user did not select a region of the image to process with their mouse
            if (OCR1.BitmapImage.Size.Height <= 1)
            {
                System.Windows.Forms.MessageBox.Show(
                    "Debe realizar la selecci�n del c�digo de barra en la imagen antes de realizar el an�lisis.");
                return;
            }

            OCR1.Process();

            imgResultado.Image = OCR1.DemoResults;

            //System.Windows.Forms.MessageBox.Show(OCR1.Text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OCR1.BitmapImageFile = sPlantillaSeleccionada;
            OCR1.ErrorCorrection = OCRTools.ErrorCorrectionTypes.IterateBrightness;
            bool lResult = OCR1.SetRegion(Convert.ToInt32(txtZoneX.Text),
                                          Convert.ToInt32(txtZoneY.Text),
                                          Convert.ToInt32(txtZoneWidth.Text),
                                          Convert.ToInt32(txtZoneHeight.Text));

            // Not requred, but you can check if the user did not select a region of the image to process with their mouse
            if (lResult == false)
            {
                System.Windows.Forms.MessageBox.Show(
                    "Debe ingresar las coordenadas antes de realizar el an�lisis.");
                return;
            }

            OCR1.Process();

            imgResultado.Image = OCR1.DemoResults;

           // System.Windows.Forms.MessageBox.Show(OCR1.Text);
        }



    }
}